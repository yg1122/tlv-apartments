module.exports = function(grunt) {
  var jsOrderArr = [
    'local/resources/assets/js/site/before/modernizr.custom.46884.js', 
	'local/resources/assets/js/site/before/jquery/**/*.js', 
	'local/resources/assets/js/site/before/modules/**/*.js', 
	'local/resources/assets/js/site/custom/**/*.js'
  ];
  
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: jsOrderArr,
        dest: 'local/resources/assets/js/compressed/<%= pkg.name %>.js'
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          'local/resources/assets/js/compressed/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
        }
      }
    },
    qunit: {
      files: ['test/**/*.html']
    },
    jshint: {
      files: ['Gruntfile.js', 'local/resources/assets/js/site/custom/**/*.js'],
      options: {
        // options here to override JSHint defaults
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true
        }
      }
    },
    watch: {
      files: ['<%= jshint.files %>'],
      tasks: ['jshint', 'concat']
    }
  }); 

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  // grunt.loadNpmTasks('grunt-contrib-qunit');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');

  
  grunt.registerTask('w', ['jshint', 'concat']);
  grunt.registerTask('default', ['jshint', 'concat', 'uglify']);

};