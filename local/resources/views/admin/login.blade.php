<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	
	<title>Admin - Log in page</title>
	<link href="{{ asset('local/resources/assets/css/admin/style.css') }}" rel="stylesheet">
	<script src="{{ asset('local/resources/assets/js/admin/jquery.js') }}"></script>
</head>

<body>
	<div id="login-page">
		<div class="container">
			<form id="login-form" method="post" action="{{ URL::to('admin/admin/login/login')}}" >
				<div class="field">
					<label>User Name:</label>
					<input class="username" name="username" placeholder="User Name" />
				</div>
				<div class="field">
					<label>Password:</label>
					<input class="password" type="password" name="password" placeholder="Password" />
				</div>
				{!! csrf_field() !!}
				<button class="login-btn" type="button">Log In</button>
				@if(Session::has('message'))
					@foreach(Session::get('message') as $type => $messages)
						<div class="alert alert-{{$type}}">
							<ul>
								@foreach ($messages as $message)
									<li>{!! html_entity_decode(e($message)) !!}</li>
								@endforeach
							</ul>
						</div>
					@endforeach
				@endif
			</form>
		</div>
	</div>
	<script>
		$(function(){
			var username = $('.username');
			var loginBtn = $('.login-btn');
			loginBtn.click(function(){
				if(!username.val()){
					alert('User name is required.');
					return;
				}
				
				loginBtn.attr("disabled", "disabled");
				$('#login-form').submit();
			});
			
			$('.password, .username').keypress(function(e){
				if(e.which === 13){
					loginBtn.click();
				}
			});
			
		});
	</script>
</body>
</html>