<div id="manage-items">
	<div class="table col-5">
		<div class="table-header">
			<div class="th">Name</div>
			<div class="th">Url Key</div>
			<div class="th">Status</div>
			<div class="th">Edit</div>
			<div class="th">Remove</div>
		</div>
		<div class="table-body">
			@foreach ($data['items'] as $index => $item)
				<div class="tr {{($index%2 == 0) ? 'even' : 'odd'}}">
					<div class="td">{{ $item->name }}</div>
					<div class="td">{{ $item->url_key }}</div>	
					<div class="td">{{ $item->status ? 'enabled' : 'disabled' }}</div>
					<div class="td edit"><a href="{{ URL::to('admin/items/manage/editItem?id=' . $item->id)}}">Edit</a></div>
					<div class="td remove" data-id="{{$item->id}}" ><a>Remove</a></div>
				</div>
			@endforeach
		</div>
	</div>
	<a href="{{ URL::to('admin/items/manage/addItem')}}">
		<div class="add-btn">Add Item</div>
	</a>
	
	@include('admin.content.pagination')
</div>
<script>
$(function(){
	var baseUrl = "{{ URL::to('/')}}";
	
	$('.table-body').on('click', '.td.remove', function(){
		var itemId = $(this).attr("data-id");
		var remove = confirm("Are you sure you want to remove this item?");
		
		if(remove){
			window.location.href = baseUrl + "/admin/items/manage/removeItem?id=" + itemId;
		}
	});
});
</script>
