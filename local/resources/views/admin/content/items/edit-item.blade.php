<div id="edit-item">
	{{--*/ $item = $data['item'] /*--}}
	<form id="edit-item-form" method="post" action="{{ URL::to('admin/items/manage/saveItem')}}" enctype="multipart/form-data">
		<input type="hidden" name="update" value="{{$item->update ? 1 : 0}}" />
		<div class="field id">
			<label>Id</label>
			<input value="{{(isset($item->id)) ? $item->id : ''}}" name="id" readonly class="disabled" />
		</div>
		<div class="field status">
			<label>Status</label>
			<select name="status">
				<option value="1">Enable</option>
				<option value="0" {{((!isset($item->status)) || $item->status == 0) ? 'selected="selected"' : '' }}>Disable</option>
			</select>
		</div>
		<div class="section general">
			<div class="title">General</div>
			<div class="content">
				<div class="field wide name">
					<label>Name</label>
					<input value="{{(isset($item->name)) ? $item->name : ''}}" name="name" />
				</div>
				<div class="field wide url_key">
					<label>URL Key</label>
					<input value="{{(isset($item->url_key)) ? $item->url_key : ''}}" name="url_key" />
				</div>
				<div class="field wide about">
					<label>About the place</label>
					<textarea name="about">{{(isset($item->about)) ? $item->about : ''}}</textarea>
				</div>
				<div class="field wide short-description">
					<label>Short Description</label>
					<textarea name="short_description">{{(isset($item->short_description)) ? $item->short_description : ''}}</textarea>
				</div>
				<div class="field wide description">
					<label>Description</label>
					<textarea name="description">{{(isset($item->description)) ? $item->description : ''}}</textarea>
				</div>
				<div class="field wide price">
					<label>Price</label>
					<input value="{{(isset($item->price)) ? $item->price : ''}}" name="price" />
				</div>
				<div class="field wide max-guests">
					<label>Max Guests</label>
					<input value="{{(isset($item->max_guests)) ? $item->max_guests : ''}}" name="max_guests" />
				</div>
				<div class="field wide ical">
					<label>Ical URL</label>
					<input value="{{(isset($item->airbnb_ics)) ? $item->airbnb_ics : ''}}" name="ical_url" />
				</div>
				@if(isset($item->id) && $item->id)
				<div class="field wide local-ical">
					<label>Local Ical URL</label>
					<div class="text">{{isset($item->d_key) ?  Helper::getLocalIcalUrl($item->id, $item->d_key) : ''}}</div>
				</div>
				@endif
			</div>
		</div>
		<div class="section address">
			<div class="title">Address</div>
			<div class="content">
				<div class="field wide country">
					<label>Country</label>
					<input value="{{(isset($item->country)) ? $item->country : ''}}" name="country" />
				</div>
				<div class="field wide city">
					<label>City</label>
					<input value="{{(isset($item->city)) ? $item->city : ''}}" name="city" />
				</div>
				<div class="field wide street">
					<label>Street</label>
					<input value="{{(isset($item->street)) ? $item->street : ''}}" name="street" />
				</div>
				<div class="field wide house-number">
					<label>House Number</label>
					<input value="{{(isset($item->house_number)) ? $item->house_number : ''}}" name="house_number" />
				</div>
				<div class="field wide apartment-number">
					<label>Apartment Number</label>
					<input value="{{(isset($item->apartment_number)) ? $item->apartment_number : ''}}" name="apartment_number" />
				</div>
				<div class="field wide lat{{(isset($item->coordinates[0]) && $item->coordinates[0]) ? '' : ' disabled'}}">
					<label>Latitude</label>
					<input value="{{isset($item->coordinates[0]) ? $item->coordinates[0] : ''}}" name="lat" />
				</div>
				<div class="field wide lng{{(isset($item->coordinates[0]) && $item->coordinates[0]) ? '' : ' disabled'}}">
					<label>Longitude</label>
					<input value="{{isset($item->coordinates[1]) ? $item->coordinates[1] : ''}}" name="lng" />
				</div>
				@if(isset($item->coordinates) && (count($item->coordinates) == 2))
				<div class="field google-map">
					<label>Location on map</label>
					<div id="googleMap"></div>
				</div>
				@endif
			</div>
		</div>
		<div class="section specs">
			<div class="title">Specification</div>
			<div class="content">
				<div class="field wide property-type">
					<label>Property Type</label>
					<input value="{{(isset($item->property_type)) ? $item->property_type : ''}}" name="property_type" />
				</div>
				<div class="field wide bedrooms">
					<label>Bedrooms</label>
					<input value="{{(isset($item->bedrooms)) ? $item->bedrooms : ''}}" name="bedrooms" />
				</div>
				<div class="field wide bathrooms">
					<label>Bathrooms</label>
					<input value="{{(isset($item->bathrooms)) ? $item->bathrooms : ''}}" name="bathrooms" />
				</div>
				<div class="field wide beds">
					<label>Beds</label>
					<input value="{{(isset($item->beds)) ? $item->beds : ''}}" name="beds" />
				</div>
				<div class="field wide bed-type">
					<label>Bed Type</label>
					<input value="{{(isset($item->bed_type)) ? $item->bed_type : ''}}" name="bed_type" />
				</div>
				<div class="field wide property-size">
					<label>Property Size (meters)</label>
					<input value="{{(isset($item->size)) ? $item->size : ''}}" name="size" />
				</div>
			</div>
		</div>
		<div class="section amenities">
			<div class="title">Amenities</div>
			<div class="content">
				<?php $counter = 1; ?>
				@foreach($item->amenitiesArr as $key => $value)
					<?php if($counter%4==1) echo '<div class="row">'; ?>
						<div class="field {{$key}}">
							<label>{{$value}}</label>
							<input type="checkbox" name="amenities[{{$key}}]" {{(isset($item->amenities[$value]) && ($item->amenities[$value])) ? "checked" : "" }} />
						</div>
					<?php if($counter%4==0) echo '</div>'; ?>
					<?php $counter++; ?>
				@endforeach
				<?php if ($counter%4 != 1) echo "</div>"; ?>
			</div>
		</div>
		<div class="section conditions-and-rules">
			<div class="title">Conditions & Rules</div>
			<div class="content">
				<div class="field wide checkin-time">
					<label>Check In time</label>
					<input value="{{(isset($item->checkin_time)) ? $item->checkin_time : ''}}" name="checkin_time" />
				</div>
				<div class="field wide checkout-time">
					<label>Check Out time</label>
					<input value="{{(isset($item->checkout_time)) ? $item->checkout_time : ''}}" name="checkout_time" />
				</div>
				<div class="field wide min-nights">
					<label>Minimum stay (number of nights)</label>
					<input value="{{(isset($item->min_nights)) ? $item->min_nights : ''}}" name="min_nights" />
				</div>
				<div class="field wide cleaning-fee">
					<label>Cleaning Fee</label>
					<input value="{{(isset($item->cleaning_fee)) ? $item->cleaning_fee : ''}}" name="cleaning_fee" />
				</div>
				<div class="field wide house-rules">
					<label>House rules</label>
					<textarea name="rules">{{(isset($item->rules)) ? $item->rules : ''}}</textarea>
				</div>
				<div class="field wide cancellation">
					<label>Cancellation</label>
					<textarea name="cancellation">{{(isset($item->cancellation)) ? $item->cancellation : ''}}</textarea>
				</div>
			</div>
		</div>
		<div class="section images">
			<div class="title">Images</div>
			<div class="content">
				<div class="banners">
					@foreach($item->images as $key => $image)
						<div class="banner {{($key == 0) ? 'first' : ''}}{{($key == (count($item->images)-1)) ? ' last' : ''}}" data-key="{{$key}}">
							<div class="field-name">{{ isset($image['field_name']) ? $image['field_name'] : '' }}</div>
							<div class="image">
								<?php $inputName = (isset($image['input_name']) && $image['input_name']) ? $image['input_name'] : 'image'; ?>
								@if(isset($image['name']) && $image['name'])
									<?php $imageName = (isset($image['resized_image']) && $image['resized_image']) ? $image['resized_image'] : $image['name']; ?>
									<img src="{{ asset('local/resources/assets/img/product/' . $imageName) }}" />
								@endif
								<input type="hidden" name="current_{{$inputName}}[{{$key}}]" value="{{isset($image['name']) ? $image['name'] : ''}}">
								<input type="file" name="{{$inputName}}[{{$key}}]" /> 
							</div>
							<div class="fields">
								<div class="col left">
									<div class="field title">
										<label>Title</label>
										<input name="title[{{$key}}]" value="{{isset($image['title']) ? $image['title'] : ''}}" />
									</div>
									<div class="field alt">
										<label>Alt</label>
										<input name="alt[{{$key}}]" value="{{isset($image['alt']) ? $image['alt'] : ''}}" />
									</div>
									<div class="field url">
										<label>URL</label>
										<input name="url[{{$key}}]" value="{{isset($image['url']) ? $image['url'] : ''}}" />
									</div>
								</div>
								<div class="col middle">
									<div class="field resize-width">
										<label>Resize Width</label>
										<input name="resize_width[{{$key}}]" value="{{isset($image['resize_width']) ? $image['resize_width'] : ''}}" />
									</div>
									<div class="field resize-height">
										<label>Resize Height</label>
										<input name="resize_height[{{$key}}]" value="{{isset($image['resize_height']) ? $image['resize_height'] : ''}}" />
									</div>
									<div class="field position">
										<label>Position</label>
										<input name="position[{{$key}}]" value="{{isset($image['position']) ? $image['position'] : ''}}" />
									</div>
								</div>
								<div class="col right">
									<div class="field target">
										<label>Target</label>
										
										<div class="self">
											<input type="radio" name="target[{{$key}}]" value="0" {{((isset($image['target'])) && ($image['target'] == false)) ? 'checked' : ''}} />
											<div class="title">Same window</div>
										</div>
										<div class="blank">
											<input type="radio" name="target[{{$key}}]" value="1" {{(isset($image['target']) && $image['target']) ? 'checked' : ''}} />
											<div class="title">New window</div>
										</div>
									</div>
									<div class="field remove">
										@if($inputName == 'image')
										<button type="button">Remove</button>
										@endif
									</div>
								</div>
							</div>
						</div> 
					@endforeach
				</div>
				<button class="add" type="button">Add More</button>
			</div>
		</div>
		{!! csrf_field() !!}
		<button class="save" type="button">Save</button>
		@if(isset($item->id) && $item->id)
		<button class="remove-item" type="button">Remove Item</button>
		@endif
	</form>
</div>

<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
$(function(){
	@if(isset($item->coordinates) && (count($item->coordinates) == 2))
	var isInitialized = false;
	$('.section.address > .title').click(function(){
		if((isInitialized === false) && ($(this).next('.content').is(':hidden'))){
			initializeGoogleMap();
			isInitialized = true;
		}
	});
	
	function initializeGoogleMap() {
		var myLatLng = {lat: {{$item->coordinates[0]}}, lng: {{$item->coordinates[1]}}};

		var map = new google.maps.Map(document.getElementById('googleMap'), {
			zoom: 17,
			center: myLatLng
		});

		var marker = new google.maps.Marker({ 
			position: myLatLng,
			map: map,
			title: '{{$item->street . " " . $item->house_number . ", " . $item->city}}'
		});
	  
	  	// Resize stuff.
		google.maps.event.addDomListener(window, "resize", function() {
		   var center = map.getCenter();
		   google.maps.event.trigger(map, "resize");
		   map.setCenter(center); 
		});
	}
	@endif
	
	$(".save").click(function(){			
		showLoader();
		$(this).attr("disabled", "disabled");
		$("#edit-item-form").submit();
	});
	
	var bannersElem = $('.banners');
	var lastBanner = $('.banner').last().clone();
	$('.add').click(function(){
		var lastKey = parseInt($('.banner').last().attr("data-key")) + 1;
		bannersElem.append(lastBanner);
		
		lastBanner.attr("data-key", lastKey);
		lastBanner.find("input").each(function(){
			var updatedName = $(this).attr("name").replace(/\[(.+?)\]/g, "["+lastKey+"]");
			$(this).attr("name", updatedName);
		});
		
		var banners = $('.banner');
		lastBanner = banners.last().clone();
		banners.removeClass("last first");
		banners.first().addClass("first");
		banners.last().addClass("last");
		
	});
	
	$('.banners').on('click', '.remove button', function(){
		var remove = confirm("Are you sure you want to remove this item?");
		
		if(remove){
			$(this).closest(".banner").remove();
			
			var banners = $('.banner');
			banners.removeClass("last first");
			banners.first().addClass("first");
			banners.last().addClass("last");
		}
	});
	
	$('#edit-item-form').on('click', '.section > .title', function(){
		var content = $(this).next(".content");
		if(content.is(":visible")){
			content.slideUp();	
		}else{
			content.slideDown();	
		}
	});
	
	var itemId = '{{(isset($item->id)) ? $item->id : 0}}';
	var baseUrl = "{{ URL::to('/')}}";
	$('button.remove-item').click(function(){
		if(itemId){
			var remove = confirm("Are you sure you want to remove this item?");
			if(remove){
				window.location.href = baseUrl + "/admin/items/manage/removeItem?id=" + itemId;
			}
		}
	});
	
	$('.field.disabled').children('input').attr('readonly', true);
})
</script>