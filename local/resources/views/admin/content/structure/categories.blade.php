<div id="manage-categories">
	<div class="table col-6">
		<div class="table-header">
			<div class="th">Name</div>
			<div class="th">Url Key</div>
			<div class="th">Status</div>
			<div class="th">Position</div>
			<div class="th">Edit</div>
			<div class="th">Remove</div>
		</div>
		<div class="table-body">
			@foreach ($data['categories'] as $index => $ctg)
				<div class="tr {{($index%2 == 0) ? 'even' : 'odd'}}">
					<div class="td">{{ $ctg->name }}</div>
					<div class="td">{{ $ctg->url_key }}</div>	
					<div class="td">{{ $ctg->status ? 'enabled' : 'disabled' }}</div>
					<div class="td">{{ $ctg->position }}</div>	
					<div class="td edit"><a href="{{ URL::to('admin/structure/categories/editCategory?id=' . $ctg->id)}}">Edit</a></div>
					<div class="td remove" data-id="{{$ctg->id}}" ><a>Remove</a></div>
				</div>
			@endforeach
		</div>
	</div>
	<a href="{{ URL::to('admin/structure/categories/addCategory')}}">
		<div class="add-btn">Add Category</div>
	</a>
	
	@include('admin.content.pagination')
</div>
<script>
$(function(){
	var baseUrl = "{{ URL::to('/')}}";
	
	$('.table-body').on('click', '.td.remove', function(){
		var ctgId = $(this).attr("data-id");
		var remove = confirm("Are you sure you want to remove this item?");
		
		if(remove){
			window.location.href = baseUrl + "/admin/structure/categories/removeCategory?id=" + ctgId;
		}
	});
});
</script>
