<div id="edit-header">
	{{--*/ $header = $data['header'] /*--}}
	<form id="edit-header-form" method="post" action="{{ URL::to('admin/structure/header/saveHeader')}}">
		<div class="field">
			<label>Content: </label>
			<textarea name="content" id="content" rows="10" cols="80">{{isset($header['content']) ? $header['content'] : ''}}</textarea>
		</div>
		<div class="field">
			<label>Reset content to default: </label>
			<button type="button" class="reset-btn">Reset</button>
		</div>
		
		{!! csrf_field() !!}
		<button class="save" type="button">Save</button>
	</form>
</div>
<script>
	var baseUrl = "{{ URL::to('/')}}";
	$(".save").click(function(){
		showLoader();
		$(this).attr("disabled", "disabled");
		$("#edit-header-form").submit();
	});
	
	$('.reset-btn').click(function(){
		var reset = confirm("Are you sure you want to reset the header content to default?");
		
		if(reset){
			showLoader();
			$('button').attr("disabled", "disabled");
			window.location.href = baseUrl + "/admin/structure/header/resetContent?name=header";
		}
	});
</script>
