<div id="edit-category">
	{{--*/ $category = $data['category'] /*--}}
	<form id="edit-category-form" method="post" action="{{ URL::to('admin/structure/edit-category/saveCategory')}}" >
		<div class="field">
			<label>Status: </label>
			<select name="status" class="status">
				<option value="1">Enable</option>
				<option value="0" {{((!isset($category->status)) || $category->status == 0) ? 'selected="selected"' : '' }}>Disable</option>
			</select>
		</div>
		<div class="field">
			<label>URL Key: </label>
			<input class="url-key" name="url_key" value="{{isset($category->url_key) ? $category->url_key : ''}}" />
		</div>
		<div class="field">
			<label>Name: </label>
			<input class="name" name="name" value="{{isset($category->name) ? $category->name : ''}}" />
		</div>
		<div class="field">
			<label>Position: </label>
			<input class="position" name="position" value="{{isset($category->position) ? $category->position : ''}}" />
		</div>
		<input type="hidden" name="current_url_key" value="{{isset($category->url_key) ? $category->url_key : ''}}" />
		<input type="hidden" name="category_id" value="{{isset($category->id) ? $category->id : ''}}" class="category-id" />
		<input type="hidden" name="update" value="{{(isset($category->id) && $category->id) ? '1' : '0'}}" />
		
		{!! csrf_field() !!}
		<button class="save" type="button">Save</button>
		@if((isset($category->id)) && ($category->id))
		<button class="remove-btn" type="button">Remove Category</button>
		@endif
	</form>
</div>
<script>
$(function(){
	var baseUrl = "{{ URL::to('/')}}";
	var name = $(".name");
	$(".save").click(function(){
 		if(!name.val()){
			alert("Name value is required.");
			return;
		}
		
		showLoader();
		$(this).attr("disabled", "disabled");
		$("#edit-category-form").submit();
	});
	
	var categoryId = '{{isset($category->id) ? $category->id : ""}}';
	$('.remove-btn').click(function(){
		if(categoryId){
			var remove = confirm("Are you sure you want to remove this item?");
		
			if(remove){
				window.location.href = baseUrl + "/admin/structure/edit-category/removeCategory?id=" + categoryId;
			}
		}
	});
});
</script>