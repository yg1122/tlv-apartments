<div id="edit-footer">
	{{--*/ $footer = $data['footer'] /*--}}
	<form id="edit-footer-form" method="post" action="{{ URL::to('admin/structure/footer/saveFooter')}}">
		<div class="field wide credits">
			<label>Credits: </label>
			<textarea name="credits">{{isset($footer['credits']) ? $footer['credits'] : ''}}</textarea>
		</div>
		<div class="field">
			<label>Content: </label>
			<textarea name="content" id="content" rows="10" cols="80">{{isset($footer['content']) ? $footer['content'] : ''}}</textarea>
		</div>
		<div class="field">
			<label>Reset content to default: </label>
			<button type="button" class="reset-btn">Reset</button>
		</div>
		
		{!! csrf_field() !!}
		<button class="save" type="button">Save</button>
	</form>
</div>
<script>
	$(".save").click(function(){
		showLoader();
		$(this).attr("disabled", "disabled");
		$("#edit-footer-form").submit();
	});
	
	$('.reset-btn').click(function(){
		var reset = confirm("Are you sure you want to reset the footer content to default?");
		
		if(reset){
			showLoader();
			$('button').attr("disabled", "disabled");
			window.location.href = baseUrl + "/admin/structure/footer/resetContent?name=footer";
		}
	});
</script>