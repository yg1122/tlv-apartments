<div id="edit-order">
	{{--*/ $order = $data['order'] /*--}}
	<form id="edit-order-form" method="post" action="{{ URL::to('admin/orders/edit/saveOrder')}}">
		<div class="field send-email">
			<label>Send order email: </label>
			<button type="button">Send</button>
		</div>
		@if(isset($order->date) && $order->date)
		<div class="field date">
			<label>Order Date: </label>
			<span class="text">{{$order->date}}</span>
		</div>	
		@endif
		<div class="field first-name">
			<label>First Name: </label>
			<input name="first_name" value="{{isset($order->first_name) ? $order->first_name : ''}}" />
		</div>
		<div class="field last-name">
			<label>Last Name: </label>
			<input name="last_name" value="{{isset($order->last_name) ? $order->last_name : ''}}" />
		</div>
		<div class="field checkin">
			<label>Check In: </label>
			<input name="checkin" value="{{isset($order->checkin) ? $order->checkin : ''}}" id="datepicker-from" readonly />
		</div>
		<div class="field checkout">
			<label>Check Out: </label>
			<input name="checkout" value="{{isset($order->checkout) ? $order->checkout : ''}}" id="datepicker-till" readonly />
		</div>
		<div class="field guests-number">
			<label>Guests Number: </label>
			<input name="guests_number" value="{{isset($order->guests_number) ? $order->guests_number : ''}}" />
		</div>
		<div class="field property">
			<label>Property: </label>
			<select name="item_id">
				@foreach($order->items as $item)
					<option value="{{$item->id}}" {{($item->id == $order->item_id) ? 'selected="selected"' : ''}} >{{$item->name . (($item->status == 0) ? ' (disabled)' : '')}}</option>
				@endforeach
			</select>
		</div>
		<div class="field email">
			<label>Email Address: </label>
			<input name="email" value="{{isset($order->email) ? $order->email : ''}}" />
		</div>
		<div class="field phone">
			<label>Phone Number: </label>
			<input name="phone" value="{{isset($order->phone) ? $order->phone : ''}}" />
		</div>
		<div class="field requests">
			<label>Special Requests: </label>
			<textarea name="requests" rows="10" cols="80">{{isset($order->requests) ? $order->requests : ''}}</textarea>
		</div>

		<input type="hidden" name="current_checkout" value="{{isset($order->checkout) ? $order->checkout : ''}}" />
		<input type="hidden" name="current_checkin" value="{{isset($order->checkin) ? $order->checkin : ''}}" />
		<input type="hidden" name="current_item_id" value="{{isset($order->item_id) ? $order->item_id : ''}}" />
		<input type="hidden" name="order_id" value="{{isset($order->id) ? $order->id : ''}}" class="order-id" />
		<input type="hidden" name="update" value="{{(isset($order->id) && $order->id) ? '1' : '0'}}" />
		
		{!! csrf_field() !!}
		<button class="save" type="button">Save</button>
		@if((isset($order->id)) && ($order->id))
		<button class="remove-btn" type="button">Remove Order</button>
		@endif
	</form>
</div>
<script>
$(function(){
	var baseUrl = "{{ URL::to('/')}}";
	var minDate = '{{$order->minDate}}';
	var disabledDates = '{{$order->disabledDates}}';
	var datePickerFrom = $("#datepicker-from");
	var datePickerTill = $("#datepicker-till"); 
	var checkintDate = '{{isset($order->checkin) ? $order->checkin : ""}}';
	var minLeavingDate = checkintDate ? getNextDays(checkintDate, 1) : minDate;
	var dateFormat = 'dd/mm/yy';
	var firstName = $('.first-name input');
	var lastName  = $('.last-name input');
	var checkin = $('.checkin input');
	var checkout = $('.checkout input');
	var guestsNumber = $('.guests-number input');
	var email = $('.email input');
	var itemId = $('.property select');
	
	var errorsArr = [
		{'input':firstName, 'error':'First name is a required field.'},
		{'input':lastName, 'error':'Last name is a required field.'},
		{'input':checkin, 'error':'Check in is a required field.'},
		{'input':checkout, 'error':'Check out is a required field.'},
		{'input':guestsNumber, 'error':'Guests number is a required field.'},
		{'input':email, 'error':'Email address is a required field.'},
		{'input':itemId, 'error':'Property is a required field.'},
	];
	
	var formData = {
		'firstName':firstName,
		'lastName':lastName,
		'checkin':checkin,
		'checkout':checkout,
		'guestsNumber':guestsNumber,
		'email':email,
		'itemId':itemId,
		'disabledDates':disabledDates,
	};
	
	datePickerFrom.datepicker({
		minDate: minDate,
		dateFormat: dateFormat,
		beforeShowDay: function(date){
			var string = jQuery.datepicker.formatDate(dateFormat, date);
			return [ disabledDates.indexOf(string) == -1 ];
		},
		onSelect : function(selectedDate){			
			var tomorrow = getNextDays(selectedDate, 1);
			datePickerTill.datepicker('option', 'minDate', tomorrow).val('');
		}

	});

	datePickerTill.datepicker({
		minDate: minLeavingDate,
		dateFormat: dateFormat,
		beforeShowDay: function(date){
			var string = jQuery.datepicker.formatDate(dateFormat, date);
			return [ disabledDates.indexOf(string) == -1 ];
		}
	});

	
	$(".save").click(function()
	{	
		var isValid = $orderObj.validateForm(errorsArr, formData);
		
		if(isValid !== true){
			alert(isValid);
			return;
		}
		
		showLoader();
		$(this).attr("disabled", "disabled");
		$("#edit-order-form").submit();
	});
	
	var orderId = '{{isset($order->id) ? $order->id : ""}}';
	$('.remove-btn').click(function(){
		if(orderId){
			var remove = confirm("Are you sure you want to remove this item?");
		
			if(remove){
				showLoader();
				$(this).attr("disabled", "disabled");
				window.location.href = baseUrl + "/admin/orders/edit/removeOrder?order_id=" + orderId;
			}
		}
	});
	
	$('.send-email button').click(function(){
		showLoader();
		$(this).attr("disabled", "disabled");
		window.location.href = baseUrl + "/admin/orders/edit/sendOrderMail?id=" + orderId;
	});
});
</script>
