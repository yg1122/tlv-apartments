<div id="manage-requests">
	<div class="table col-9">
		<div class="table-header">
			<div class="th">Request Date</div>
			<div class="th">Request ID</div>
			<div class="th">Full Name</div>
			<div class="th">Check In</div>
			<div class="th">Check Out</div>
			<div class="th">Property</div>
			<div class="th">Status</div>
			<div class="th">Show</div>
			<div class="th">Remove</div>
		</div>
		<div class="table-body">
			@foreach ($data['requests'] as $index => $request)
				<div class="tr {{($index%2 == 0) ? 'even' : 'odd'}}">
					<div class="td">{{ $request->date }}</div>
					<div class="td">{{ $request->id }}</div>	
					<div class="td">{{ $request->first_name . ' ' . $request->last_name}}</div>
					<div class="td">{{ $request->checkin }}</div>
					<div class="td">{{ $request->checkout }}</div>
					<div class="td">{{ $request->name }}</div>
					<div class="td">{{ $request->status }}</div>
					<div class="td edit"><a href="{{ URL::to('admin/orders/show-request/showRequest?id=' . $request->id)}}">Show</a></div>
					<div class="td remove" data-id="{{$request->id}}" ><a>Remove</a></div>
				</div>
			@endforeach
		</div>
	</div>
	<a href="{{ URL::to('admin/orders/show-request/createRequest')}}">
		<div class="add-btn" title="Create Request">Create Request</div>
	</a>
	
	@include('admin.content.pagination')
</div>
<script>
$(function(){
	var baseUrl = "{{ URL::to('/')}}";
	
	$('.table-body').on('click', '.td.remove', function(){
		var requestId = $(this).attr("data-id");
		var remove = confirm("Are you sure you want to remove this request?");
		
		if(remove){
			window.location.href = baseUrl + "/admin/orders/manage-requests/removeRequest?request_id=" + requestId;
		}
	});
});
</script>
