<div id="manage-orders">
	<div class="table col-9">
		<div class="table-header">
			<div class="th">Order Date</div>
			<div class="th">Order ID</div>
			<div class="th">Full Name</div>
			<div class="th">Check In</div>
			<div class="th">Check Out</div>
			<div class="th">Guests Number</div>
			<div class="th">Property</div>
			<div class="th">Edit</div>
			<div class="th">Remove</div>
		</div>
		<div class="table-body">
			@foreach ($data['orders'] as $index => $order)
				<div class="tr {{($index%2 == 0) ? 'even' : 'odd'}}">
					<div class="td">{{ $order->date }}</div>
					<div class="td">{{ $order->id }}</div>	
					<div class="td">{{ $order->first_name . ' ' . $order->last_name}}</div>
					<div class="td">{{ $order->checkin }}</div>
					<div class="td">{{ $order->checkout }}</div>
					<div class="td">{{ $order->guests_number }}</div>
					<div class="td">{{ $order->name }}</div>
					<div class="td edit"><a href="{{ URL::to('admin/orders/manage/editOrder?id=' . $order->id)}}">Edit</a></div>
					<div class="td remove" data-id="{{$order->id}}" ><a>Remove</a></div>
				</div>
			@endforeach
		</div>
	</div>
	<a href="{{ URL::to('admin/orders/manage/createOrder')}}">
		<div class="add-btn" title="Create Order">Create Order</div>
	</a>
	
	@include('admin.content.pagination')
</div>
<script>
$(function(){
	var baseUrl = "{{ URL::to('/')}}";
	
	$('.table-body').on('click', '.td.remove', function(){
		var orderId = $(this).attr("data-id");
		var remove = confirm("Are you sure you want to remove this item?");
		
		if(remove){
			window.location.href = baseUrl + "/admin/orders/manage/removeOrder?order_id=" + orderId;
		}
	});
});
</script>
