<div id="request-page">
	{{--*/ $request = $data['request'] /*--}}
	<div class="field status">
		<label>Status: </label>
		<div class="text">{{ isset($request->status) ? $request->status : '' }}</div>
	</div>
	
	@if(isset($request->status) && ($request->status == 'Pending'))
	<form id="request-form" method="post" action="{{ URL::to('admin/orders/request/updateRequest')}}">	
		<div class="field update-status">
			<label>Update Status: </label>
			<select name="update_status">
				<option value="1">Approve & Send approval email</option>
				<option value="2">Approve & Don't send approval email</option>
				<option value="3">Reject & Send rejection email</option>
				<option value="4">Reject & Don't send rejection email</option>
			</select>
			<input type="hidden" name="id" value="{{ isset($request->id) ? $request->id : '' }}" />
			{!! csrf_field() !!}
			<button>Update</button>
		</div>

		@if(isset($request->status) && ($request->status == 'Pending'))
		<div class="field apply-auto-redirect">
			<label>Apply auto redirect: </label>
			<input type="checkbox" class="checkbox" name="paypal_auto_redirect" />
		</div>
		@endif
	</form>
	@endif
	
	<div class="field approval-url">
		<label>Approval Link: </label>
		<div class="text">{{ isset($request->approval_url) ? $request->approval_url : '' }}</div>
	</div>
	<div class="field date">
		<label>Date: </label>
		<div class="text">{{ isset($request->date) ? $request->date : '' }}</div>
	</div>
	<div class="field property">
		<label>Property: </label>
		<div class="text">{{ isset($request->property) ? $request->property . ' (' .$request->item_id . ')' : '' }}</div>
	</div>
	<div class="field first-name">
		<label>First Name: </label>
		<div class="text">{{ isset($request->first_name) ? $request->first_name : '' }}</div>
	</div>
	<div class="field last-name">
		<label>Last Name: </label>
		<div class="text">{{ isset($request->last_name) ? $request->last_name : '' }}</div>
	</div>
	<div class="field checkin">
		<label>Check In: </label>
		<div class="text">{{ isset($request->checkin) ? $request->checkin : '' }}</div>
	</div>
	<div class="field checkout">
		<label>Check Out: </label>
		<div class="text">{{ isset($request->checkout) ? $request->checkout : '' }}</div>
	</div>
	<div class="field guests-number">
		<label>Guests Number: </label>
		<div class="text">{{ isset($request->guests_number) ? $request->guests_number : '' }}</div>
	</div>
	<div class="field email">
		<label>Email Address: </label>
		<div class="text">{{ isset($request->email) ? $request->email : '' }}</div>
	</div>
	<div class="field message">
		<label>Special Requests: </label>
		<div class="message-container">
			<div class="content">{!! isset($request->requests) ? nl2br(html_entity_decode(e($request->requests))) : '' !!}</div>
		</div>
	</div>
	<button class="remove-btn" type="button">Remove Request</button>
</div>
<script>
$(function(){
	var baseUrl = "{{ URL::to('/')}}";
	var requestId = '{{isset($request->id) ? $request->id : ""}}';
	
	$('.remove-btn').click(function(){
		if(!requestId) return;
		
		var remove = confirm("Are you sure you want to remove this request?");
		
		if(remove){
			showLoader();
			window.location.href = baseUrl + "/admin/orders/manage-request/removeRequest?request_id=" + requestId;
		}
	});
	
	var approvalLink 	 = '{{$request->approval_url}}';
	var approvalLinkElem = $('.approval-url .text');
	$('.apply-auto-redirect input').change(function(){
		if($(this).is(':checked')){
			approvalLinkElem.html(approvalLink + '&auto_redirect=1');
		}else{
			approvalLinkElem.html(approvalLink);
		}
	});
});
</script>
