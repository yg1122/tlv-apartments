<div id="create-request">
	{{--*/ $request = $data['request'] /*--}}
	<form id="create-request-form" method="post" action="{{ URL::to('admin/orders/create-request/saveRequest')}}">
		<div class="field property">
			<label>Property: </label>
			<select name="item_id">
				@foreach($request->items as $item)
					<option value="{{$item->id}}">{{$item->name . (($item->status == 0) ? ' (disabled)' : '')}}</option>
				@endforeach
			</select>
		</div>
		<div class="field first-name">
			<label>First Name: </label>
			<input name="first_name" value="" />
		</div>
		<div class="field last-name">
			<label>Last Name: </label>
			<input name="last_name" value="" />
		</div>
		<div class="field checkin">
			<label>Check In: </label>
			<input name="checkin" value="" id="datepicker-from" readonly />
		</div>
		<div class="field checkout">
			<label>Check Out: </label>
			<input name="checkout" value="" id="datepicker-till" readonly />
		</div>
		<div class="field guests-number">
			<label>Guests Number: </label>
			<input name="guests_number" value="" />
		</div>
		<div class="field email">
			<label>Email Address: </label>
			<input name="email" value="" />
		</div>
		<div class="field requests">
			<label>Special Requests: </label>
			<textarea name="requests" rows="10" cols="80"></textarea>
		</div>
		
		{!! csrf_field() !!}
		<button class="save" type="button">Save</button>
	</form>
</div>
<script>
$(function(){
	var baseUrl = "{{ URL::to('/')}}";
	var minDate = '{{$request->minDate}}';
	var disabledDates = '{{$request->disabledDates}}';
	var datePickerFrom = $("#datepicker-from");
	var datePickerTill = $("#datepicker-till"); 
	var checkintDate = '';
	var minLeavingDate = minDate;
	var dateFormat = 'dd/mm/yy';
	var firstName = $('.first-name input');
	var lastName  = $('.last-name input');
	var checkin = $('.checkin input');
	var checkout = $('.checkout input');
	var guestsNumber = $('.guests-number input');
	var email = $('.email input');
	var itemId = $('.property select');
	
	var errorsArr = [
		{'input':firstName, 'error':'First name is a required field.'},
		{'input':lastName, 'error':'Last name is a required field.'},
		{'input':checkin, 'error':'Check in is a required field.'},
		{'input':checkout, 'error':'Check out is a required field.'},
		{'input':guestsNumber, 'error':'Guests number is a required field.'},
		{'input':email, 'error':'Email address is a required field.'},
		{'input':itemId, 'error':'Property is a required field.'},
	];
	
	var formData = {
		'firstName':firstName,
		'lastName':lastName,
		'checkin':checkin,
		'checkout':checkout,
		'guestsNumber':guestsNumber,
		'email':email,
		'itemId':itemId,
		'disabledDates':disabledDates,
	};
	
	datePickerFrom.datepicker({
		minDate: minDate,
		dateFormat: dateFormat,
		beforeShowDay: function(date){
			var string = jQuery.datepicker.formatDate(dateFormat, date);
			return [ disabledDates.indexOf(string) == -1 ];
		},
		onSelect : function(selectedDate){			
			var tomorrow = getNextDays(selectedDate, 1);
			datePickerTill.datepicker('option', 'minDate', tomorrow).val('');
		}

	});

	datePickerTill.datepicker({
		minDate: minLeavingDate,
		dateFormat: dateFormat,
		beforeShowDay: function(date){
			var string = jQuery.datepicker.formatDate(dateFormat, date);
			return [ disabledDates.indexOf(string) == -1 ];
		}
	});

	
	$(".save").click(function()
	{	
		var isValid = $orderObj.validateForm(errorsArr, formData);
		
		if(isValid !== true){
			alert(isValid);
			return;
		}
		
		showLoader();
		$(this).attr("disabled", "disabled");
		$("#create-request-form").submit();
	});
	
});
</script>
