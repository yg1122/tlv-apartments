{{--*/ $page = $data['page'] /*--}}
@if($page->pagesNumber > 1)
	<div class="pagination">
		<div class="cubes">
			@for ($i = 1; $i <= $page->pagesNumber; $i++)
				<a href="{{ $page->actionUrl . '?page=' . $i }}" class="page-number{{($page->number == $i) ? ' current' : ''}}" title="{{$i}}">{{$i}}</a>
			@endfor
		</div>
	</div>
@endif