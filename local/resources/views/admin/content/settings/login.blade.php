<div id="login-details">
	<form id="login-details-form" method="post" action="{{ URL::to('admin/settings/login/updateLoginDetails')}}">
		<div class="field">
			<label>User Name</label>
			<input name="username" class="username" value="{{$data['username']}}" />
		</div>
		<div class="field">
			<label>New Password</label>
			<input name="password" class="password" type="password" />
		</div>
		<div class="field">
			<label>Confirm Password</label>
			<input name="confirm_password" class="confirm-password" type="password" />
		</div>
		{!! csrf_field() !!}
		<button type="button" class="save">Save</button>
	</form>
</div>
<script>
$(function(){
	var username = $('.username');
	var password = $('.password');
	var confirmPassword = $('.confirm-password');
	
	$('.save').click(function(){
		if(!username.val()){
			alert('User name is required.');
			return;
		}

		if(password.val() !== confirmPassword.val()){
			alert("The passwords don't match.");
			return;
		}
		
		showLoader();
		$(this).attr("disabled", "disabled");
		$('#login-details-form').submit();
	});
});
</script>