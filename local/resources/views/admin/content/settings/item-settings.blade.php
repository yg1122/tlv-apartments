<div id="item-settings">
	<form id="item-settings-form" method="post" action="{{ URL::to('admin/settings/item-settings/saveItem')}}">
		<div class="field">
			<label>Performance Mode: </label>
			<select name="item_performance_mode">
				<option value="0">Disable</option>
				<option value="1" {{($data['item_performance_mode'] == 1) ? 'selected="selected"' : ''}}>Enable</option>
			</select>
		</div>
		<div class="field">
			<label>Request to book mode: </label>
			<select name="request_booking">
				<option value="0">Disable</option>
				<option value="1" {{($data['request_booking'] == 1) ? 'selected="selected"' : ''}}>Enable</option>
			</select>
			<div class="comment">If enabled, users won't be able to book a property without approval.</div>
		</div>
		{!! csrf_field() !!}
		<button type="button" class="save">Save</button>
	</form>
</div>
<script>
$(function(){
	$(".save").click(function(){
		showLoader();
		$(this).attr("disabled", "disabled");
		$("#item-settings-form").submit();
	});
});
</script>