<div id="seo">
{{--*/ $seo = $data['seo'] /*--}}
	<form id="seo-form" method="post" action="{{ URL::to('admin/settings/seo/saveSeo')}}">
		<div class="field meta-title">
			<label>Default Meta Title: </label>
			<input name="meta_title" value="{{$seo['meta_title']}}" />
		</div>
		<div class="field meta-keywords">
			<label>Default Meta Keywords: </label>
			<input name="meta_keywords" value="{{$seo['meta_keywords']}}" />
		</div>
		<div class="field meta-description">
			<label>Default Meta Description: </label>
			<textarea name="meta_description">{{$seo['meta_description']}}</textarea>
		</div>
		<div class="field robots">
			<label>Meta Robots: </label>
			<select name="robots">
				@foreach($seo['robots_values'] as $value)
					<option value="{{$value}}" {{($seo['robots'] == $value) ? 'selected="selected"' : ''}} >{{$value}}</option>
				@endforeach
			</select>
		</div>
		{!! csrf_field() !!}
		<button class="save" type="button">Save</button>
	</form>
</div>
<script>
	$(".save").click(function(){
		showLoader();
		$(this).attr("disabled", "disabled");
		$("#seo-form").submit();
	});
</script>