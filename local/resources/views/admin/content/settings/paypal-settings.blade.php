<div id="payapl-settings">
	<form id="payapl-settings-form" method="post" action="{{ URL::to('admin/settings/paypal/savePaypal')}}">
		<div class="field wide">
			<label>API Username: </label>
			<input name="username" value="{{$data['username']}}" />
		</div>
		<div class="field wide">
			<label>API Password: </label>
			<input name="password" value="{{$data['password']}}" />
		</div>
		<div class="field wide">
			<label>Api Signature: </label>
			<input name="signature" value="{{$data['signature']}}" />
		</div>
		<div class="field">
			<label>Mode: </label>
			<select name="mode">
				<option value="sandbox">sandbox</option>
				<option value="live" {{($data['mode'] == 'live') ? 'selected="selected"' : ''}} >live</option>
			</select>
		</div>
		<div class="field">
			<label>Paypal Auto Redirect: </label>
			<select name="auto_redirect">
				<option value="0">Disable</option>
				<option value="1" {{$data['auto_redirect'] ? 'selected="selected"' : ''}} >Enable</option>
			</select>
			<div class="comment">
				<div>If enabled, clicking the request approval link will auto redirect to Paypal.</div>
				<div class="sub">NOTICE: in order to apply this mode, the <b>Request to book mode</b> field has to be enabled in the <b>Item Settings</b> zone.</div>
			</div>
		</div>
		{!! csrf_field() !!}
		<button class="save" type="button">Save</button>
	</form>
</div>
<script>
	$(".save").click(function(){
		showLoader();
		$(this).attr("disabled", "disabled");
		$("#payapl-settings-form").submit();
	});
</script>