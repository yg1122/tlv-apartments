<div id="list-settings-container">
{{--*/ $listConf = $data['list_conf'] /*--}}
	<div class="list-settings-form-wrapper">
		<form class="list-settings-page-form" method="post" action="{{ URL::to('admin/settings/list-settings/saveList')}}">
			<div class="field">
				<label>List URL Key: </label>
				<input name="list_url_key" value="{{$listConf['list_url_key']}}" />
			</div>
			<div class="field">
				<label>Page Title: </label>
				<input name="list_title" value="{{$listConf['list_title']}}" />
			</div>
			<div class="field">
				<label>Number of items per page: </label>
				<select name="items_per_page" class="items-per-page">
					@foreach ($data['pagination'] as $value)
						<option value="{{$value}}" {{($listConf['items_per_page'] == $value) ? 'selected="selected"' : ''}}>{{$value}}</option>
					@endforeach
				</select>
			</div>
			<div class="field">
				<label>Check In latest hour: </label>
				<select name="max_checkin_hour">
					<option value="0:00" <?php if($listConf['max_checkin_hour'] == '0:00') echo 'selected="selected"'; ?>>No limit</option>
					<?php for($i=1;$i<24;$i++) : ?>
						<option value="<?php echo $i . ':00'; ?>" <?php if($listConf['max_checkin_hour'] == $i . ':00') echo 'selected="selected"'; ?>><?php echo $i . ':00'; ?></option>
						<option value="<?php echo $i . ':30'; ?>" <?php if($listConf['max_checkin_hour'] == $i . ':30') echo 'selected="selected"'; ?>><?php echo $i . ':30'; ?></option>
					<?php endfor; ?>
				</select>
				<div class="comment">The maximum hour that the customer will be allowed to book a property for the current day.</div>
			</div>
			<div class="field">
				<label>Hours in advance to check in:</label>
				<select name="hours_in_advance">
					<option value="0">Any hour in advance</option>
					<?php for($i=1;$i<49;$i++) : ?>
						<option value="<?php echo $i; ?>" <?php if($listConf['list_min_date_hours'] == $i) echo 'selected="selected"'; ?> ><?php echo $i; ?></option>
					<?php endfor; ?>
				</select>
				<div class="comment">
					<div>The number of hours before the check in time that the customer won't be allowed to book a property.</div>
					<div class="sub">NOTICE: if set, the <b>Check In latest hour</b> value will be overridden.</div>
				</div>
			</div>
			<div class="field">
				<label>Update iCal files</label>
				<button type="button" class="update-ical">Update</button>
				<div class="comment">Update all the iCal files.</div>
			</div>
			<input type="hidden" class="ical-input" name="update_ical" value="0" />
			<div class="field">
				<label>Disabled dates:</label>
				<div id="disabled-dates-calendar"></div>
				<input id="disabled-dates-input" name="disabled_dates" />
				<div class="comment">The chosen dates will be disabled for all the properties.</div>
			</div>
			
			{!! csrf_field() !!}
			<button type="button" class="save">Save</button>
		</form>
	</div>
</div>
<script>
$(document).ready(function(){

	var datesArr = '{{$listConf["list_disabled_dates"]}}';
	
	if(datesArr){
		datesArr = datesArr.split(",");			
		var disabledDatesArr = [];
		for(var i=0;i<datesArr.length;i++){
			disabledDatesArr.push(strToDate(datesArr[i]));
		}
	}
	
   $('#disabled-dates-calendar').multiDatesPicker({
		addDates: disabledDatesArr,
		altField: '#disabled-dates-input',
		dateFormat: "dd/mm/yy", 
   });
   
	function strToDate(str){
	
		var dateStrArr = str.split("/");
		var newDateStr = dateStrArr[1] + '/' + dateStrArr[0] + '/' + dateStrArr[2];
		
		return new Date(newDateStr);
	}
	
	$(".update-ical").click(function(){
		showLoader();
		$(".ical-input").val("1");
		$(this).attr("disabled", "disabled");
		$(".list-settings-page-form").submit();
	});
	
	$(".save").click(function(){
		showLoader();
		$(this).attr("disabled", "disabled");
		$(".list-settings-page-form").submit();
	});
   
});
</script>