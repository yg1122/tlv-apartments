<div id="general-settings">
	<form class="general-settings-form" method="post" action="{{ URL::to('admin/settings/general/saveGeneral')}}" enctype="multipart/form-data">
		<div class="field website-name">
			<label>Website Name: </label>
			<input name="website_name" value="{{$data['website_name']}}" />
		</div>
		<div class="field logo">
			<label>Logo: </label>
			<div class="image">
				@if(isset($data['logo']) && $data['logo'])
					<img src="{{ asset('local/resources/assets/img/icons/' . $data['logo']) }}" />
				@endif
				<input type="hidden" name="current_logo" value="{{isset($data['logo']) ? $data['logo'] : ''}}">
				<input type="file" name="image[0]" class="image-input" /> 
			</div>
		</div>
		<div class="field favicon">
			<label>Favicon: </label>
			<div class="image">
				@if(isset($data['favicon']) && $data['favicon'])
					<img src="{{ asset('local/resources/assets/img/icons/' . $data['favicon']) }}" />
				@endif
				<input type="hidden" name="current_favicon" value="{{isset($data['favicon']) ? $data['favicon'] : ''}}">
				<input type="file" name="image[1]" class="image-input" /> 
			</div>
		</div>
		<div class="field timezone">
			<label>Time Zone: </label>
			<select name="timezone">
				<?php foreach($data['timezones'] as $key => $value) :?>
					<option value="<?php echo $key; ?>" <?php if($data['timezone'] == $key) echo 'selected="selected"'; ?> ><?php echo $value; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="field vat">
			<label>VAT: </label>
			<input name="vat" value="{{$data['vat']}}" />
		</div>
		<div class="field site-phone">
			<label>Contact Phone Number: </label>
			<input name="contact_phone" value="{{$data['contact_phone']}}" />
		</div>
		<div class="field email-address">
			<label>General email "From" address: </label>
			<input name="email_address" value="{{$data['email_address']}}" />
		</div>
		<div class="field email-name">
			<label>General email "From" name: </label>
			<input name="email_name" value="{{$data['email_name']}}" />
		</div>
		<div class="field contact-email">
			<label>Contact email address: </label>
			<input name="contact_email" value="{{$data['contact_email']}}" />
			<div class="comment">An email address where the contact us inquiries will be sent to.</div>
		</div>
		<div class="field requests-email">
			<label>Booking requests email: </label>
			<input name="booking_requests_email" value="{{$data['booking_requests_email']}}" />
			<div class="comment">An email address where the booking requests will be sent to.</div>
		</div>
		<div class="field currency">
			<label>Currency: </label>
			<select name="currency">
				<?php foreach($data['currenciesArr']['currencies'] as $code => $currency) :?>
					<option value="<?php echo $code; ?>" <?php if($data['currency']['code'] == $code) echo 'selected="selected"'; ?> ><?php echo $currency['name']; ?></option>
				<?php endforeach; ?>
			</select>
			<div class="comment">
				<div>* {{$data['currenciesArr']['notices']['*']}}</div>
				<div>** {{$data['currenciesArr']['notices']['**']}}</div>
			</div>
		</div>
		<input type="hidden" name="current_timezone" value="{{$data['timezone']}}" />
		{!! csrf_field() !!}
		<button class="save" type="button">Save</button>
	</form>
</div>
<script>
	$(".save").click(function(){
		showLoader();
		$(this).attr("disabled", "disabled");
		$(".general-settings-form").submit();
	});
</script>