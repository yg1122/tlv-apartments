<div id="cms-blocks">
	<div class="table col-4">
		<div class="table-header">
			<div class="th">Identifier</div>
			<div class="th">Status</div>
			<div class="th">Edit</div>
			<div class="th">Remove</div>
		</div>
		<div class="table-body">
			@foreach ($data['content_data'] as $index => $block)
				<div class="tr {{($index%2 == 0) ? 'even' : 'odd'}}">
					<div class="td">{{ $block->identifier }}</div>
					<div class="td">{{ $block->status ? 'enabled' : 'disabled' }}</div>
					<div class="td edit"><a href="{{ URL::to('admin/cms/cms-blocks/editBlock?id=' . $block->id)}}">Edit</a></div>
					<div class="td remove" data-id="{{$block->id}}" ><a>Remove</a></div>
				</div>
			@endforeach
		</div>
	</div>
	<a href="{{ URL::to('admin/cms/cms-blocks/addBlock')}}">
		<div class="add-btn">Add Block</div>
	</a>
	
	@include('admin.content.pagination')
</div>
<script>
$(function(){
	var baseUrl = "{{ URL::to('/')}}";
	
	$('.table-body').on('click', '.td.remove', function(){
		var blockId = $(this).attr("data-id");
		var remove = confirm("Are you sure you want to remove this item?");
		
		if(remove){
			window.location.href = baseUrl + "/admin/cms/cms-blocks/removeBlock?id=" + blockId;
		}
	});
});
</script>
