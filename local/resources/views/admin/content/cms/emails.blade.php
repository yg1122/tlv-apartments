<div id="manage-emails">
	<div class="table col-4">
		<div class="table-header">
			<div class="th">Identifier</div>
			<div class="th">Status</div>
			<div class="th">Edit</div>
			<div class="th">Remove</div>
		</div>
		<div class="table-body">
			@foreach ($data['content_data'] as $index => $email)
				<div class="tr {{($index%2 == 0) ? 'even' : 'odd'}}">
					<div class="td">{{ $email->identifier }}</div>
					<div class="td">{{ $email->status ? 'enabled' : 'disabled' }}</div>
					<div class="td edit"><a href="{{ URL::to('admin/cms/emails/editEmail?id=' . $email->id)}}">Edit</a></div>
					<div class="td {{($email->permanent) ? 'disabled' : 'remove'}}" data-id="{{$email->id}}" ><a>Remove</a></div>
				</div>
			@endforeach
		</div>
	</div>
	<a href="{{ URL::to('admin/cms/emails/addEmail')}}">
		<div class="add-btn">Add Email</div>
	</a>
	
	@include('admin.content.pagination')
</div>
<script>
$(function(){
	var baseUrl = "{{ URL::to('/')}}";
	
	$('.table-body').on('click', '.td.remove', function(){
		var emailId = $(this).attr("data-id");
		var remove = confirm("Are you sure you want to remove this item?");
		
		if(remove){
			window.location.href = baseUrl + "/admin/cms/emails/removeEmail?id=" + emailId;
		}
	});
});
</script>
