<link href="{{ asset('local/resources/assets/css/admin/modules/editor.css') }}" rel="stylesheet">
<div class="upload-message">{{isset($data['message']) ? $data['message'] : ''}}</div>
<form action="{{ URL::to('admin/cms/editor/upload')}}" method="post" enctype="multipart/form-data" id="upload-form">
	{!! csrf_field() !!}
	<input type="file" name="upload" />
	</br></br>
	<input type="submit" value="Send it to the Server" class="upload-submit-btn" />
</form>