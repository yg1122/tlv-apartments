<div id="cms-pages-content">
	<div id="cms-pages-table">
		<div class="table-header">
			<div class="th">Url Key</div>
			<div class="th">Title</div>
			<div class="th">Status</div>
			<div class="th">Edit</div>
			<div class="th">Remove</div>
		</div>
		<div class="table-body">
			@foreach ($data['content_data'] as $index => $cms)
				<div class="tr {{($index%2 == 0) ? 'even' : 'odd'}}">
					<div class="td">{{ $cms->slug }}</div>
					<div class="td">{{ $cms->title }}</div>
					<div class="td">{{ $cms->status ? 'enabled' : 'disabled' }}</div>
					<div class="td edit"><a href="{{ URL::to('admin/cms/cms-pages/edit?id=' . $cms->id)}}">Edit</a></div>
					<div class="td {{($cms->slug == '404') ? 'disabled' : 'remove'}}" data-id="{{$cms->id}}" ><a>Remove</a></div>
				</div>
			@endforeach
		</div>
	</div>
	<a href="{{ URL::to('admin/cms/cms-pages/add/')}}">
		<div class="add-btn">Add Page</div>
	</a>
	
	@include('admin.content.pagination')
</div>
<script>
$(function(){
	var baseUrl = "{{ URL::to('/')}}";
	
	$('.table-body').on('click', '.td.remove', function(){
		var pageId = $(this).attr("data-id");
		var remove = confirm("Are you sure you want to remove this item?");
		
		if(remove){
			window.location.href = baseUrl + "/admin/cms/cms-pages/removePage?id=" + pageId;
		}
	});
});
</script>
