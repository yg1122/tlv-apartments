<div id="cms-page-edit">
	<div class="cms-form-wrapper">
		{{--*/ $page = $data['content_data'] /*--}}
		<form class="cms-page-form {{(isset($page->slug) && ($page->slug == '404')) ? 'disabled' : ''}}" method="post" action="{{ URL::to('admin/cms/cms-pages/savePage')}}">
			<div class="field">
				<label>Status: </label>
				<select name="status" class="status">
					@if(isset($page->slug) && ($page->slug == '404'))
						<option value="1">Enable</option>
					@else
						<option value="1">Enable</option>
						<option value="0" {{((!isset($page->status)) || $page->status == 0) ? 'selected="selected"' : '' }}>Disable</option>
					@endif
				</select>
			</div>
			<div class="field">
				<label>URL Key: </label>
				<input class="slug" name="slug" value="{{isset($page->slug) ? $page->slug : ''}}" {{(isset($page->slug) && ($page->slug == '404')) ? 'readonly' : ''}}/>
			</div>
			<div class="field">
				<label>Title: </label>
				<input name="title" value="{{isset($page->title) ? $page->title : ''}}" />
			</div>
			<div class="field">
				<label>Content: </label>
				<textarea name="page_content" id="content" rows="10" cols="80">{{isset($page->page_content) ? $page->page_content : ''}}</textarea>
			</div>
			<input type="hidden" name="current_slug" value="{{isset($page->slug) ? $page->slug : ''}}" class="current-slug" />
			<input type="hidden" name="page_id" value="{{isset($page->id) ? $page->id : ''}}" class="page-id" />
			<input type="hidden" name="update" value="{{(isset($page->id) && $page->id) ? '1' : '0'}}" />
			
			{!! csrf_field() !!}
			<button class="save" type="button">Save</button>
			@if((isset($page->slug)) && ($page->slug != '404'))
			<button class="remove-btn" type="button">Remove Page</button>
			@endif
		</form>
	</div>
</div>
<script>
$(function(){
	var baseUrl = "{{ URL::to('/')}}";
	var slugField = $("input.slug");
	$(".save").click(function(){
		if(!slugField.val()){
			alert("URL Key value must be set.");
			return;
		}
		
		showLoader();
		$(this).attr("disabled", "disabled");
		$(".cms-page-form").submit();
	});
	
	$('.cms-page-form.disabled .status').on('mousedown', function(e) {
        e.preventDefault();
        this.blur();
        window.focus();
    });
	
	$('.remove-btn').click(function(){
		var pageId = $('.page-id').val();
		var remove = confirm("Are you sure you want to remove this item?");
		
		if(remove){
			window.location.href = baseUrl + "/admin/cms/cms-pages/removePage?id=" + pageId;
		}
	});
});	
</script>