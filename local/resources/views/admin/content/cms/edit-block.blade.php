<div id="cms-block-edit">
	{{--*/ $block = $data['block'] /*--}}
	<form class="cms-block-form" method="post" action="{{ URL::to('admin/cms/cms-blocks/saveBlock/')}}">
		<div class="field">
			<label>Status: </label>
			<select name="status" class="status">
				<option value="1">Enable</option>
				<option value="0" {{((!isset($block->status)) || $block->status == 0) ? 'selected="selected"' : '' }}>Disable</option>
			</select>
		</div>
		<div class="field">
			<label>Identifier: </label>
			<input class="identifier" name="identifier" value="{{isset($block->identifier) ? $block->identifier : ''}}" />
		</div>
		<div class="field">
			<label>Content: </label>
			<textarea name="content" id="content" rows="10" cols="80">{{isset($block->content) ? $block->content : ''}}</textarea>
		</div>
		<input type="hidden" name="block_id" value="{{isset($block->id) ? $block->id : ''}}" class="block-id" />
		<input type="hidden" name="update" value="{{(isset($block->id) && $block->id) ? '1' : '0'}}" />
		<input type="hidden" name="current_identifier" value="{{isset($block->identifier) ? $block->identifier : ''}}" class="current-identifier" />
		
		{!! csrf_field() !!}
		<button class="save" type="button">Save</button>
		@if((isset($block->id)) && ($block->id))
		<button class="remove-btn" type="button">Remove Block</button>
		@endif
	</form>
</div>
<script>
	var baseUrl = "{{ URL::to('/')}}";
	var identifier = $(".identifier");
	$(".save").click(function(){
		if(!identifier.val()){
			alert("Identifier value must be set.");
			return;
		}
		
		showLoader();
		$(this).attr("disabled", "disabled");
		$(".cms-block-form").submit();
	});
	
	var blockId = '{{isset($block->id) ? $block->id : ""}}';
	$('.remove-btn').click(function(){
		if(blockId){
			var remove = confirm("Are you sure you want to remove this item?");
		
			if(remove){
				window.location.href = baseUrl + "/admin/cms/cms-blocks/removeBlock?id=" + blockId;
			}
		}
	});
</script>
