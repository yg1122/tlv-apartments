<div id="email-edit">
	{{--*/ $email = $data['email'] /*--}}
	<form id="email-edit-form" method="post" action="{{ URL::to('admin/cms/emails/saveEmail')}}">
		<div class="field">
			<label>Status: </label>
			@if(isset($email->permanent) && $email->permanent)
				<input class="disabled" value="Enable" readonly />
			@else
			<select name="status" class="status">
				<option value="1">Enable</option>
				<option value="0" {{((!isset($email->status)) || $email->status == 0) ? 'selected="selected"' : '' }}>Disable</option>
			</select>
			@endif
		</div>
		<div class="field">
			<label>Identifier: </label>
			@if(isset($email->permanent) && $email->permanent)
				<input class="identifier disabled" name="identifier" value="{{isset($email->identifier) ? $email->identifier : ''}}" readonly />
			@else
				<input class="identifier" name="identifier" value="{{isset($email->identifier) ? $email->identifier : ''}}" />
			@endif
		</div>
		<div class="field wide">
			<label>Subject: </label>
			<input class="subject" name="subject" value="{{isset($email->subject) ? $email->subject : ''}}" />
		</div>
		<div class="field">
			<label>Content: </label>
			<textarea name="content" id="content" rows="10" cols="80">{{isset($email->content) ? $email->content : ''}}</textarea>
		</div>
		@if(isset($email->tokens) && $email->tokens)
		<div class="field">
			<label>Available Tokens: </label>
			<code>{{$email->tokens}}</code>
		</div>
		@endif
		<input type="hidden" name="permanent" value="{{isset($email->permanent) ? $email->permanent : ''}}" />
		<input type="hidden" name="email_id" value="{{isset($email->id) ? $email->id : ''}}" class="email-id" />
		<input type="hidden" name="update" value="{{(isset($email->id) && $email->id) ? '1' : '0'}}" />
		<input type="hidden" name="current_identifier" value="{{isset($email->identifier) ? $email->identifier : ''}}" class="current-identifier" />
		
		{!! csrf_field() !!}
		<button class="save" type="button">Save</button>
		@if((isset($email->id)) && ($email->id) && ($email->permanent == false))
		<button class="remove-btn" type="button">Remove Email</button>
		@endif
	</form>
</div>
<script>
	var baseUrl = "{{ URL::to('/')}}";
	var identifier = $(".identifier");
	$(".save").click(function(){
		if(!identifier.val()){
			alert("Identifier value must be set.");
			return;
		}
		
		showLoader();
		$(this).attr("disabled", "disabled");
		$("#email-edit-form").submit();
	});
	
	var emailId = '{{isset($email->id) ? $email->id : ""}}';
	$('.remove-btn').click(function(){
		if(emailId){
			var remove = confirm("Are you sure you want to remove this item?");
		
			if(remove){
				window.location.href = baseUrl + "/admin/cms/emails/removeEmail?id=" + emailId;
			}
		}
	});
</script>
