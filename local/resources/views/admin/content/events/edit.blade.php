<div id="edit-event">
	{{--*/ $event = $data['event'] /*--}}
	<form id="edit-event-form" method="post" action="{{ URL::to('admin/events/edit/saveEvent')}}">	
		<div class="field checkin">
			<label>Check In: </label>
			<input name="checkin" value="{{isset($event->checkin) ? $event->checkin : ''}}" id="datepicker-from" readonly />
		</div>
		<div class="field checkout">
			<label>Check Out: </label>
			<input name="checkout" value="{{isset($event->checkout) ? $event->checkout : ''}}" id="datepicker-till" readonly />
		</div>
		<div class="field property">
			<label>Property: </label>
			<select name="item_id">
				@foreach($event->items as $item)
					<option value="{{$item->id}}" {{($item->id == $event->item_id) ? 'selected="selected"' : ''}} >{{$item->name . (($item->status == 0) ? ' (disabled)' : '')}}</option>
				@endforeach
			</select>
		</div>

		<input type="hidden" name="current_checkout" value="{{isset($event->checkout) ? $event->checkout : ''}}" />
		<input type="hidden" name="current_checkin" value="{{isset($event->checkin) ? $event->checkin : ''}}" />
		<input type="hidden" name="current_item_id" value="{{isset($event->item_id) ? $event->item_id : ''}}" />
		<input type="hidden" name="event_id" value="{{(isset($event->id) && $event->id) ? $event->id : ''}}" />
		<input type="hidden" name="update" value="{{(isset($event->id) && $event->id) ? '1' : '0'}}" />
		
		{!! csrf_field() !!}
		<button class="save" type="button">Save</button>
		@if((isset($event->id)) && ($event->id))
		<button class="remove-btn" type="button">Remove Event</button>
		@endif
	</form>
</div>
<script>
$(function(){
	var baseUrl = "{{ URL::to('/')}}";
	var minDate = '{{$event->minDate}}';
	var disabledDates = '{{$event->disabledDates}}';
	var datePickerFrom = $("#datepicker-from");
	var datePickerTill = $("#datepicker-till"); 
	var checkintDate = '{{isset($event->checkin) ? $event->checkin : ""}}';
	var minLeavingDate = checkintDate ? getNextDays(checkintDate, 1) : minDate;
	var dateFormat = 'dd/mm/yy';
	var checkin = $('.checkin input');
	var checkout = $('.checkout input');
	var itemId = $('.property select');
	var formElement = $('#edit-event-form');
	
	var errorsArr = [
		{'input':checkin, 'error':'Check in is a required field.'},
		{'input':checkout, 'error':'Check out is a required field.'},
		{'input':itemId, 'error':'Property is a required field.'},
	];
	
	var formData = {
		'checkin':checkin,
		'checkout':checkout,
		'itemId':itemId,
		'disabledDates':disabledDates,
	};
	
	datePickerFrom.datepicker({
		minDate: minDate,
		dateFormat: dateFormat,
		beforeShowDay: function(date){
			var string = jQuery.datepicker.formatDate(dateFormat, date);
			return [ disabledDates.indexOf(string) == -1 ];
		},
		onSelect : function(selectedDate){			
			var tomorrow = getNextDays(selectedDate, 1);
			datePickerTill.datepicker('option', 'minDate', tomorrow).val('');
		}

	});

	datePickerTill.datepicker({
		minDate: minLeavingDate,
		dateFormat: dateFormat,
		beforeShowDay: function(date){
			var string = jQuery.datepicker.formatDate(dateFormat, date);
			return [ disabledDates.indexOf(string) == -1 ];
		}
	});

	
	$(".save").click(function(){	
		var isValid = $eventsObj.validateForm(errorsArr, formData);
		
		if(isValid !== true){
			alert(isValid);
			return;
		}
		
		showLoader();
		$(this).attr("disabled", "disabled");
		formElement.submit();
	});
	
	var eventId = '{{isset($event->id) ? $event->id : ""}}';
	$('.remove-btn').click(function(){
		if(eventId){
			var remove = confirm("Are you sure you want to remove this event?");
		
			if(remove){
				showLoader();
				$(this).attr("disabled", "disabled");
				formElement.attr('action', baseUrl + '/admin/events/edit/removeEvent');
				formElement.submit();
			}
		}
	});
});
</script>
