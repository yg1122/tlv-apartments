<div id="manage-events">
	<div class="table col-6">
		<div class="table-header">
			<div class="th">Item ID</div>
			<div class="th">Item Name</div>
			<div class="th">Check In</div>
			<div class="th">Check Out</div>
			<div class="th">Edit</div>
			<div class="th">Remove</div>
		</div>
		<div class="table-body">
			@foreach ($data['events'] as $index => $event)
				<div class="tr {{($index%2 == 0) ? 'even' : 'odd'}}">
					<div class="td">{{ $event->item_id }}</div>
					<div class="td">{{ $event->name }}</div>	
					<div class="td">{{ $event->checkin }}</div>
					<div class="td">{{ $event->checkout }}</div>
					<div class="td edit"><a href="{{ URL::to('admin/events/manage/editEvent?id=' . $event->id)}}">Edit</a></div>
					<div class="td remove" data-id="{{$event->id}}" ><a>Remove</a></div>
				</div>
			@endforeach
		</div>
	</div>
	<a href="{{ URL::to('admin/events/manage/createEvent')}}">
		<div class="add-btn" title="Create Event">Create Event</div>
	</a>
	
	@include('admin.content.pagination')
</div>
<script>
$(function(){
	var baseUrl = "{{ URL::to('/')}}";
	
	$('.table-body').on('click', '.td.remove', function(){
		var eventId = $(this).attr("data-id");
		var remove = confirm("Are you sure you want to remove this event?");
		
		if(remove){
			showLoader();
			window.location.href = baseUrl + "/admin/events/manage/removeEvent?id=" + eventId;
		}
	});
});
</script>
