<div id="manage-inquiries">
	<div class="table col-6">
		<div class="table-header">
			<div class="th">Name</div>
			<div class="th">Email Address</div>
			<div class="th">Subject</div>
			<div class="th">Date</div>
			<div class="th">Show</div>
			<div class="th">Remove</div>
		</div>
		<div class="table-body">
			@foreach ($data['inquiries'] as $index => $inquiry)
				<div class="tr {{($index%2 == 0) ? 'even' : 'odd'}}">
					<div class="td">{{ $inquiry->name }}</div>
					<div class="td">{{ $inquiry->email }}</div>	
					<div class="td">{{ $inquiry->subject }}</div>
					<div class="td">{{ $inquiry->date }}</div>
					<div class="td edit"><a href="{{ URL::to('admin/contact/manage/loadInquiry?id=' . $inquiry->id)}}">Show</a></div>
					<div class="td remove" data-id="{{$inquiry->id}}" ><a>Remove</a></div>
				</div>
			@endforeach
		</div>
	</div>
</div>
<script>
$(function(){
	var baseUrl = "{{ URL::to('/')}}";
	
	$('.table-body').on('click', '.td.remove', function(){
		var inquiryId = $(this).attr("data-id");
		var remove = confirm("Are you sure you want to remove this inquiry?");
		
		if(remove){
			showLoader();
			window.location.href = baseUrl + "/admin/contact/manage/removeInquiry?id=" + inquiryId;
		}
	});
});
</script>
