<div id="inquiry-page">
	{{--*/ $inquiry = $data['inquiry'] /*--}}
	<div class="field date">
		<label>Date: </label>
		<div class="text">{{ isset($inquiry->date) ? $inquiry->date : '' }}</div>
	</div>
	<div class="field name">
		<label>Name: </label>
		<div class="text">{{ isset($inquiry->name) ? $inquiry->name : '' }}</div>
	</div>
	<div class="field email">
		<label>Email Address: </label>
		<div class="text">{{ isset($inquiry->email) ? $inquiry->email : '' }}</div>
	</div>
	<div class="field message">
		<label>Message: </label>
		<div class="message-container">
			<div class="subject">{{ isset($inquiry->subject) ? $inquiry->subject : '' }}</div>
			<div class="content">{!! isset($inquiry->message) ? nl2br(html_entity_decode(e($inquiry->message))) : '' !!}</div>
		</div>
	</div>
	<button class="remove-btn" type="button">Remove Inquiry</button>
</div>
<script>
$(function(){
	var baseUrl = "{{ URL::to('/')}}";
	var inquiryId = '{{isset($inquiry->id) ? $inquiry->id : ""}}';
	
	$('.remove-btn').click(function(){
		if(!inquiryId) return;
		
		var remove = confirm("Are you sure you want to remove this inquiry?");
		
		if(remove){
			showLoader();
			window.location.href = baseUrl + "/admin/contact/manage/removeInquiry?id=" + inquiryId;
		}
	});
});
</script>
