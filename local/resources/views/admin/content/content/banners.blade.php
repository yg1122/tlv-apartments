<div id="banners">
	<div class="table col-3">
		<div class="table-header">
			<div class="th">Identifier</div>
			<div class="th">Edit</div>
			<div class="th">Remove</div>
		</div>
		<div class="table-body">
			@foreach ($data['banners'] as $index => $banner)
				<div class="tr {{($index%2 == 0) ? 'even' : 'odd'}}">
					<div class="td">{{ $banner->identifier }}</div>
					<div class="td edit"><a href="{{ URL::to('admin/content/banners/editBanner?identifier=' . $banner->identifier)}}">Edit</a></div>
					<div class="td remove" data-id="{{$banner->identifier}}" ><a>Remove</a></div>
				</div>
			@endforeach
		</div>
	</div>
	<a href="{{ URL::to('admin/content/banners/addBanner/')}}">
		<div class="add-btn">Add Banner</div>
	</a>
	
	@include('admin.content.pagination')
</div>
<script>
$(function(){
 	var baseUrl = "{{ URL::to('/')}}"; 
	
	$('.table-body').on('click', '.td.remove', function(){
		var identifier = $(this).attr("data-id");
		var remove = confirm("Are you sure you want to remove this item?");
		
		if(remove){
			window.location.href = baseUrl + "/admin/content/banners/removeBanner?identifier=" + identifier;
		}
	});
})
</script>
