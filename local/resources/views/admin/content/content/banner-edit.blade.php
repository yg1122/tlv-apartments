<div id="edit-banner">
	<form id="edit-banner-form" method="post" action="{{ URL::to('admin/content/banners/saveBanner')}}" enctype="multipart/form-data">
		<div class="field identifier">
			<label>Identifier</label>
			<input value="{{(isset($data['banners'][0]->identifier)) ? $data['banners'][0]->identifier : ''}}" name="identifier" />
		</div>
		<input type="hidden" name="current_identifier" value="{{(isset($data['banners'][0]->identifier)) ? $data['banners'][0]->identifier : ''}}" />
		<div class="banners">
			@foreach($data['banners'] as $key => $banner)
				<div class="banner {{($key == 0) ? 'first' : ''}}{{($key == (count($data['banners'])-1)) ? ' last' : ''}}" data-key="{{$key}}">
					<div class="image">
						@if(isset($banner->image_name) && $banner->image_name)
							<?php $imageName = (isset($banner->resized_image) && $banner->resized_image) ? $banner->resized_image : $banner->image_name; ?>
							<img src="{{ asset('local/resources/assets/img/banners/' . $imageName) }}" />
						@endif
						<input type="hidden" name="current_image[{{$key}}]" value="{{isset($banner->image_name) ? $banner->image_name : ''}}">
						<input type="file" name="image[{{$key}}]" /> 
					</div>
					<div class="fields">
						<div class="col left">
							<div class="field title">
								<label>Title</label>
								<input name="title[{{$key}}]" value="{{isset($banner->title) ? $banner->title : ''}}" />
							</div>
							<div class="field alt">
								<label>Alt</label>
								<input name="alt[{{$key}}]" value="{{isset($banner->alt) ? $banner->alt : ''}}" />
							</div>
							<div class="field url">
								<label>URL</label>
								<input name="url[{{$key}}]" value="{{isset($banner->url) ? $banner->url : ''}}" />
							</div>
						</div>
						<div class="col middle">
							<div class="field resize-width">
								<label>Resize Width</label>
								<input name="resize_width[{{$key}}]" value="{{isset($banner->resize_width) ? $banner->resize_width : ''}}" />
							</div>
							<div class="field resize-height">
								<label>Resize Height</label>
								<input name="resize_height[{{$key}}]" value="{{isset($banner->resize_height) ? $banner->resize_height : ''}}" />
							</div>
							<div class="field position">
								<label>Position</label>
								<input name="position[{{$key}}]" value="{{isset($banner->position) ? $banner->position : ''}}" />
							</div>
						</div>
						<div class="col right">
							<div class="field target">
								<label>Target</label>
								
								<div class="self">
									<input type="radio" name="target[{{$key}}]" value="0" {{((isset($banner->target)) && ($banner->target == false)) ? 'checked' : ''}} />
									<div class="title">Same window</div>
								</div>
								<div class="blank">
									<input type="radio" name="target[{{$key}}]" value="1" {{(isset($banner->target) && $banner->target) ? 'checked' : ''}} />
									<div class="title">New window</div>
								</div>
							</div>
							<div class="field remove">
								<button type="button">Remove</button>
							</div>
						</div>
					</div>
				</div> 
			@endforeach
		</div>
		{!! csrf_field() !!}
		<button class="save" type="button">Save</button>
		<button class="add" type="button">Add More</button>
		@if(isset($data['banners'][0]->identifier) && $data['banners'][0]->identifier)
		<button class="remove-btn" type="button">Remove Banner</button>
		@endif
	</form>
</div>
<script>
	var baseUrl = "{{ URL::to('/')}}";
	var identifierField = $(".field.identifier input");
	$(".save").click(function(){
		if(!identifierField.val()){
			alert("Identifier value must be set.");
			return;
		}
		
		showLoader();
		$(this).attr("disabled", "disabled");
		$("#edit-banner-form").submit();
	});
	
	var bannersElem = $('.banners');
	var lastBanner = $('.banner').last().clone();
	$('.add').click(function(){
		var lastKey = parseInt($('.banner').last().attr("data-key")) + 1;
		bannersElem.append(lastBanner);
		
		lastBanner.attr("data-key", lastKey);
		lastBanner.find("input").each(function(){
			var updatedName = $(this).attr("name").replace(/\[(.+?)\]/g, "["+lastKey+"]");
			$(this).attr("name", updatedName);
		});
		
		var banners = $('.banner');
		lastBanner = banners.last().clone();
		banners.removeClass("last first");
		banners.first().addClass("first");
		banners.last().addClass("last");
		
	});
	
	$('.banners').on('click', '.remove button', function(){
		var remove = confirm("Are you sure you want to remove this item?");
		
		if(remove){
			$(this).closest(".banner").remove();
			
			var banners = $('.banner');
			banners.removeClass("last first");
			banners.first().addClass("first");
			banners.last().addClass("last");
		}
	});
	
	var bannerId = "{{(isset($data['banners'][0]->identifier)) ? $data['banners'][0]->identifier : ''}}";
	$('.remove-btn').click(function(){
		if(bannerId){
			var remove = confirm("Are you sure you want to remove this item?");
		
			if(remove){
				window.location.href = baseUrl + "/admin/content/banners/removeBanner?identifier=" + bannerId;
			}
		}
	});
</script>