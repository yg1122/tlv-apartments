<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="NOINDEX, NOFOLLOW" />

    <title>Admin | {{$data['seo']['meta_title']}}</title>

	<!-- admin's compiled CSS -->
	<link href="{{ asset('local/resources/assets/css/admin/style.css') }}" rel="stylesheet">
	<script src="{{ asset('local/resources/assets/js/admin/jquery.js') }}"></script>
	<script src="{{ asset('local/resources/assets/js/site/before/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('local/resources/assets/js/admin/ckeditor/ckeditor.js') }}"></script>
	<script src="{{ asset('local/resources/assets/js/site/before/jquery/jquery-ui.multidatespicker.js') }}"></script>
	<script src="{{ asset('local/resources/assets/js/admin/custom/custom.js') }}"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
			
            <a href="{{ URL::to('admin')}}" class="logo"><b>Admin Panel</b></a>
            <!--logo end-->
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                <ul class="nav top-menu">
				    <li class="dropdown contacts">
                        <a data-toggle="dropdown" class="dropdown-toggle" data-table="contact">
                            <i class="fa fa-users"></i>
							@if($notifications['contacts']['new'])
                            <span class="badge bg-theme">{{ $notifications['contacts']['new'] }}</span>
							@endif
                        </a>
                         <ul class="dropdown-menu extended inbox">
                            <div class="notify-arrow notify-arrow-green"></div>
                            <li>
								@if($notifications['requests']['new'])
                                <p class="green">You have {{ $notifications['contacts']['new'] }} new inquiries</p>
								@else
								<p class="green">No new inquiries</p>
								@endif
                            </li>
							@foreach($notifications['contacts']['values'] as $notification)
                            <li class="notification {{ $notification->notify ? 'new' : 'seen' }}" data-id="{{ $notification->id }}">
                                <a href="{{ URL::to('admin/contact/manage/loadInquiry?id=') . $notification->id}}">
								    <div class="photo">
										<i class="fa fa-comment" style="font-size:30px;"></i>									
									</div>
                                    <div class="notification-content">
										<span class="subject">
											<span class="from">{{ $notification->name }}</span>
											<span class="time">{{ $notification->time }}</span>
										</span>
										<span class="message">{{ $notification->subject }}</span>
									</div>
                                </a>
                            </li>
							@endforeach
                            <li>
                                <a href="{{ URL::to('admin/contact/manage') }}">See all inquiries</a>
                            </li>
                        </ul>
                    </li>
					
                    <!-- settings start -->
                    <li class="dropdown orders">
                        <a data-toggle="dropdown" class="dropdown-toggle" data-table="order">
                            <i class="fa fa-credit-card"></i>
							@if($notifications['orders']['new'])
                            <span class="badge bg-theme">{{ $notifications['orders']['new'] }}</span>
							@endif
                        </a>
                        <ul class="dropdown-menu extended inbox">
                            <div class="notify-arrow notify-arrow-green"></div>
                            <li>
								@if($notifications['requests']['new'])
                                <p class="green">You have {{ $notifications['orders']['new'] }} new orders</p>
								@else
								<p class="green">No new orders</p>
								@endif
                            </li>
							@foreach($notifications['orders']['values'] as $notification)
                            <li class="notification {{ $notification->notify ? 'new' : 'seen' }}" data-id="{{ $notification->id }}">
                                <a href="{{ URL::to('admin/orders/manage/editOrder?id=') . $notification->id}}">
								    <div class="photo">
										<?php $imageName = (isset($notification->list_image['resized_image']) && $notification->list_image['resized_image']) ? $notification->list_image['resized_image'] : $notification->list_image['name']; ?>
										<img src="{{ asset('local/resources/assets/img/product/' . $imageName) }}" />
									</div>
									<div class="notification-content">
										<span class="subject">
											<span class="from">{{ $notification->name }}</span>
											<span class="time">{{ $notification->time }}</span>
										</span>
										<span class="message">
											<div class="row first">has been reserved for:</div>
											<div class="row second">{{ $notification->checkin }} - {{ $notification->checkout }}</div>
										</span>
									</div>
								</a>
                            </li>
							@endforeach
                            <li>
                                <a href="{{ URL::to('admin/orders/manage') }}">See all orders</a>
                            </li>
                        </ul>
                    </li>
                    <!-- settings end -->
					
                    <!-- inbox dropdown start-->
                    <li id="header_inbox_bar" class="dropdown requests">
                        <a data-toggle="dropdown" class="dropdown-toggle" data-table="request">
                            <i class="fa fa-envelope-o"></i>
							@if($notifications['requests']['new'])
                            <span class="badge bg-theme">{{ $notifications['requests']['new'] }}</span>
							@endif
                        </a>
                        <ul class="dropdown-menu extended inbox">
                            <div class="notify-arrow notify-arrow-green"></div>
                            <li>
								@if($notifications['requests']['new'])
                                <p class="green">You have {{ $notifications['requests']['new'] }} new booking requests</p>
								@else
								<p class="green">No new booking requests</p>
								@endif
                            </li>
							@foreach($notifications['requests']['values'] as $notification)
                            <li class="notification {{ $notification->notify ? 'new' : 'seen' }}" data-id="{{ $notification->id }}">
                                <a href="{{ URL::to('admin/orders/show-request/showRequest?id=') . $notification->id}}">
								    <div class="photo">
										<?php $imageName = (isset($notification->list_image['resized_image']) && $notification->list_image['resized_image']) ? $notification->list_image['resized_image'] : $notification->list_image['name']; ?>
										<img src="{{ asset('local/resources/assets/img/product/' . $imageName) }}" />
									</div> 
									<div class="notification-content">
										<span class="subject">
											<span class="from">{{ $notification->first_name }} {{ $notification->last_name }}</span>
											<span class="time">{{ $notification->time }}</span>
										</span>
										<span class="message">
											<div class="row first">would like to book {{ $notification->name }} at:</div>
											<div class="row second">{{ $notification->checkin }} - {{ $notification->checkout }}</div>
										</span>
									</div>
                                </a>
                            </li>
							@endforeach
                            <li>
                                <a href="{{ URL::to('admin/orders/manage-requests') }}">See all requests</a>
                            </li>
                        </ul>
                    </li>
                    <!-- inbox dropdown end -->
                </ul>
                <!--  notification end -->
            </div>
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="{{ URL::to('admin/admin/logout/logout')}}">Logout</a></li>
            	</ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
				<p class="centered">
					<a href="{{ URL::to('admin') }}">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" width="50px" height="50px" viewBox="0 0 32 32" style="enable-background:new 0 0 32 32;" xml:space="preserve" >
							<g>
								<path d="M1.38,15.408l13.288-0.527l8.085-9.339l7.728,11.579L32,16.903l-9.09-12.91l-9.283,1.719v-1.65l-0.765-0.158l-2.304,0.487    V6.28L7.463,6.854L0,13.633v1.785C0,15.419,0.763,15.399,1.38,15.408z"></path>
								<path d="M22.702,6.342l-7.804,9.016L2.155,15.865v10.82l11.708,1.406v0.004l7.319-0.854v-8.43l3.368-0.005v8.041l5.396-0.661    v-8.99L22.702,6.342z M7.126,23.309L3.733,23.09v-4.33l3.393,0.002V23.309z M12.862,23.655l-3.945-0.253v-4.66l3.945,0.001V23.655    z M19.521,23.453l-4.165,0.273v-4.94l4.165-0.003V23.453z M28.971,22.861l-3.145,0.207v-4.275l3.145-0.004V22.861z"></path>
							</g>
						</svg>
						<!-- 	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" width="50px" height="50px" viewBox="0 0 58.137 58.137" style="enable-background:new 0 0 58.137 58.137;" xml:space="preserve" >
							<g>
								<path d="M47.815,49.001c-0.028-3.53-0.073-20.329,1.571-26.652l-6.257-5.151v30.587c-0.362-0.073-0.724-0.146-1.104-0.214V17.196   l-7.176,4.784c0.934,4.16,1.362,18.671,1.508,24.857c-0.402-0.033-0.816-0.062-1.231-0.091c0.011-1.795,0.04-3.6,0.104-5.409   c-0.082-2.564-0.191-5.439-0.33-8.197l-1.209-0.381v-2.841h1.024c-0.212-3.338-0.479-6.208-0.811-7.686l-0.174-0.774l1.687-1.125   l-1.307-0.418l0.464-2.508l1.758,0.555l-0.224,1.911L37,19.278c0.751-6.372,1.438-10.637,1.438-10.637l-8.556-6.808v44.702   c-0.283-0.002-0.549,0.002-0.827,0.002V1.19l-9.568,7.359c0,0,0.331,2.181,0.785,5.768l2.188,1.626v-2.036l1.982-0.749v2.372   l-1.465,0.797l2.155,1.604l-0.203,0.759c-1.045,3.893-1.873,12.321-2.422,19.414c0.166,2.862,0.267,5.763,0.326,8.643   c-0.555,0.042-1.098,0.087-1.626,0.14c0.384-6.485,1.465-22.537,3.063-28.497l-7.543-5.611v34.729   c-0.373,0.067-0.748,0.133-1.104,0.205V12.781L8.173,18.3c1.907,6.549,2.542,26.762,2.65,30.695C1.676,52.075,0,56.947,0,56.947   h58.137C58.137,56.947,57.156,52.083,47.815,49.001z M35.072,8.745l2.162,1.019l-0.302,2.643l-2.102-0.78L35.072,8.745z    M34.831,12.828l1.981,0.811l-0.421,2.973l-1.816-0.721L34.831,12.828z M22.46,12.408l-0.601-2.522l2.042-1.07l0.541,2.812   L22.46,12.408z M21.199,18.413l1.875,1.007l-0.389,1.875l-1.618-0.479L21.199,18.413z M20.929,21.837l1.562,0.39L22.312,24.3   l-1.381-0.543L20.929,21.837L20.929,21.837z M20.749,24.854l1.384,0.559l-0.152,2.338l-1.382-0.667L20.749,24.854z M20.456,28.24   l1.224,0.226l-0.15,2.063l-1.074-0.347V28.24z M20.208,31.384h1.322v2.321l-1.322-0.1V31.384z M19.578,43.09l-1.441-0.361v-2.402   l1.441,0.6V43.09z M19.578,39.78l-1.441-0.362v-2.402l1.441,0.602V39.78z M19.578,36.469l-1.441-0.36v-2.403l1.441,0.602V36.469z    M19.578,33.158l-1.441-0.36v-2.402l1.441,0.601V33.158z M19.578,29.847l-1.441-0.36v-2.403l1.441,0.601V29.847z M19.578,26.537   l-1.441-0.362v-2.403l1.441,0.601V26.537z M19.578,23.227l-1.441-0.361v-2.402l1.441,0.6V23.227z M19.578,19.916l-1.441-0.361   v-2.403l1.441,0.601V19.916z M20.929,43.09h-0.946v-2.068h0.946V43.09z M20.929,39.78h-0.946v-2.068h0.946V39.78z M20.929,36.469   h-0.946v-2.067h0.946V36.469z M23.902,26.221l1.217-0.692l0.225,2.948l-1.441,0.207V26.221z M25.658,41.021h-1.037v-2.746h1.037   V41.021z M25.658,36.924l-1.215,0.182v-3.02l1.215-0.226V36.924z M24.442,32.582v-2.504l0.9-0.27l0.315,2.688L24.442,32.582z    M27.866,40.633l-1.201,0.572v-3.064l1.201-0.689V40.633z M27.866,36.249l-1.201,0.674V33.86l1.201-0.794V36.249z M27.866,31.866   l-1.201,0.631v-3.063l1.201-0.749V31.866z M27.866,27.481l-1.472,0.66V25.08l1.472-0.78V27.481z M27.866,23.098l-1.622,0.66v-3.062   l1.622-0.779V23.098z M27.866,18.714l-1.622,0.57v-3.062l1.622-0.69V18.714z M27.866,14.33l-1.801,0.841v-3.063l1.801-0.96V14.33z    M27.866,9.946l-2.042,0.78V7.664l2.042-0.9V9.946z M31.242,6.763l2.118,1.2v2.906l-2.118-1.104V6.763z M32.28,41.205l-1.038-0.36   v-2.93l1.038,0.36V41.205z M32.28,36.924l-1.038-0.38V32.82l1.038,0.657V36.924z M32.598,32.496l-1.354-0.962v-3.057l1.354,1.016   V32.496z M32.598,28.142l-1.354-1.005v-3.002l1.354,1.103V28.142z M32.79,23.569l-1.548-0.775v-3.002l1.548,0.872V23.569z    M32.941,19.415l-1.699-0.964v-3.002l1.699,1.062V19.415z M32.941,14.885l-1.699-0.776v-3.002l1.699,0.873V14.885z M34.381,41.205   l-1.037-0.36v-2.93l1.037,0.36V41.205z M34.577,37.105l-1.096-0.182v-2.928l1.096,0.239V37.105z M46.421,22.32l1.577,0.754v1.779   l-1.577-0.901V22.32z M46.106,33.429l1.066,0.379v1.778l-1.066-0.523V33.429z M46.106,36.305l1.066,0.377v1.778l-1.066-0.522   V36.305z M46.106,39.181l1.066,0.377v1.779l-1.066-0.522V39.181z M45.205,45.071l-1.26-0.495v-2.004l1.26,0.61V45.071z    M45.205,42.383l-1.26-0.496v-2.002l1.26,0.61V42.383z M45.205,39.695l-1.26-0.496v-2.002l1.26,0.609V39.695z M45.205,37.008   l-1.26-0.497v-2.002l1.26,0.608V37.008z M45.205,34.321l-1.26-0.496v-2.004l1.26,0.608V34.321z M45.205,31.633l-1.26-0.495v-2.005   l1.26,0.61V31.633z M45.205,28.945l-1.26-0.496v-2.004l1.26,0.61V28.945z M45.205,26.258l-1.26-0.496v-2.004l1.26,0.611V26.258z    M45.205,23.569l-1.26-0.495V21.07l1.26,0.611V23.569z M47.322,44.024l-1.216-0.336v-1.634l1.216,0.192V44.024z M47.322,32.886   l-1.216-0.699v-1.633l1.216,0.553V32.886z M47.683,30.212l-1.577-0.689v-1.844l1.577,0.754V30.212z M47.683,27.524l-1.577-0.901   v-1.633l1.577,0.755V27.524z"></path>
							</g>
						</svg> -->
<!-- 						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" width="50px" height="50px" viewBox="0 0 44.922 44.922" style="enable-background:new 0 0 44.922 44.922;" xml:space="preserve" >
							<g>
								<path d="M44.662,30.428L27.518,18.295c-0.357-0.254-0.859-0.059-0.951,0.37l-1.47,6.829l-9.055,4.896    c-0.559,0.303-0.344,1.15,0.291,1.15h3.929v4.141h-3.579c-1.072,0-1.941,0.869-1.941,1.941v7.18h2.46v-6.236    c0-0.201,0.163-0.363,0.363-0.363h2.697v6.6h22.083V31.54h1.964C44.904,31.54,45.15,30.774,44.662,30.428z M37.38,38.201h-3.81    c-0.323,0-0.586-0.264-0.586-0.586v-3.029c0-0.323,0.263-0.586,0.586-0.586h3.81c0.323,0,0.585,0.263,0.585,0.586v3.029    C37.964,37.939,37.703,38.201,37.38,38.201z"></path>
								<path d="M3.596,5.948C5.481,5.554,7.564,5.936,9.532,7.5c0.912,0.725,1.582,1.53,2.043,2.378    c-0.591-0.671-1.412-1.146-2.363-1.288c-1.458-0.219-2.906,0.497-3.783,1.683c-0.352,0.475-0.553,0.949-0.651,1.416    c-0.067,0.318,0.307,0.561,0.563,0.36c0.879-0.684,2.05-1.046,3.483-0.844c1.177,0.166,2.08,0.648,2.729,1.314    C11.45,12.608,0.555,24.875,0,44.801h6.697c-0.585-21.974,7.052-30.522,7.058-30.554l0,0c0.733,0.485,1.353,1.228,1.754,2.27    c0.522,1.354,0.432,2.581-0.04,3.595c-0.136,0.291,0.182,0.597,0.473,0.462c0.436-0.202,0.856-0.507,1.243-0.96    c0.956-1.123,1.327-2.695,0.783-4.065c-0.354-0.895-1.001-1.587-1.789-2.011c0.93,0.258,1.866,0.729,2.778,1.453    c1.937,1.539,2.784,3.444,2.85,5.341c0.013,0.356,0.449,0.514,0.678,0.24c0.63-0.754,1.105-1.712,1.276-2.986    c0.298-2.212-0.466-4.505-2.225-5.879c-0.569-0.445-1.192-0.774-1.84-0.993c0.065,0.006,0.129,0.013,0.195,0.021    c2.366,0.276,4.052,1.374,5.117,2.862c0.248,0.347,0.812,0.198,0.854-0.226c0.08-0.827-0.018-1.719-0.401-2.687    c-0.892-2.24-2.855-3.913-5.257-4.116c-0.863-0.072-1.701,0.036-2.477,0.294c0.076-0.813-0.007-1.653-0.272-2.476    c-0.739-2.294-2.813-3.831-5.196-4.193C11.2,0.033,10.289,0.149,9.486,0.431C9.107,0.564,9.089,1.116,9.461,1.267    c1.716,0.691,3.192,2.093,4.005,4.361c0.022,0.063,0.043,0.124,0.063,0.186C13.17,5.232,12.708,4.7,12.146,4.246    C10.41,2.844,8.004,2.618,5.917,3.407c-1.24,0.469-2.08,1.176-2.676,1.991C3.051,5.66,3.277,6.015,3.596,5.948z"></path>
							</g>
						</svg> -->
					</a>
				</p>
              	  <h5 class="centered">{{ $data['website_name'] }}</h5>
              	  	
                  <li class="mt">
                      <a href="{{ URL::to('admin') }}">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>
				  <li class="sub-menu">
                      <a href="javascript:;" class="{{($data['section'] == 'settings') ? 'active' : ''}}">
                          <i class="fa fa-cogs"></i>
                          <span>Settings</span>
                      </a>
                      <ul class="sub">
							<li class="{{($data['content'] == 'general-settings') ? 'active' : ''}}"><a href="{{ URL::to('admin/settings/general-settings')}}">General</a></li>
							<li class="{{($data['content'] == 'seo') ? 'active' : ''}}"><a href="{{ URL::to('admin/settings/seo')}}">SEO</a></li>
							<li class="{{($data['content'] == 'paypal-settings') ? 'active' : ''}}"><a href="{{ URL::to('admin/settings/paypal-settings')}}">PayPal Settings</a></li>
							<li class="{{($data['content'] == 'item-settings') ? 'active' : ''}}"><a href="{{ URL::to('admin/settings/item-settings')}}">Item Settings</a></li>
							<li class="{{($data['content'] == 'list-settings') ? 'active' : ''}}"><a href="{{ URL::to('admin/settings/list-settings')}}">List Settings</a></li>
							<li class="{{($data['content'] == 'login') ? 'active' : ''}}"><a href="{{ URL::to('admin/settings/login')}}">Login Details</a></li>
					  </ul> 
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" class="{{($data['section'] == 'structure') ? 'active' : ''}}">
                          <i class="fa fa-puzzle-piece"></i>
                          <span>Structure</span>
                      </a>
                      <ul class="sub">
					  	  <li class="{{($data['content'] == 'categories') ? 'active' : ''}}"><a href="{{ URL::to('admin/structure/categories')}}">Categories</a></li>
					  	  <li class="{{($data['content'] == 'header') ? 'active' : ''}}"><a href="{{ URL::to('admin/structure/header')}}">Header</a></li>
					  	  <li class="{{($data['content'] == 'footer') ? 'active' : ''}}"><a href="{{ URL::to('admin/structure/footer')}}">Footer</a></li>
                      </ul>
                  </li>
				  <li class="sub-menu">
                      <a href="javascript:;" class="{{($data['section'] == 'cms') ? 'active' : ''}}">
                          <i class="fa fa-pencil-square-o"></i>
                          <span>CMS</span>
                      </a>
                      <ul class="sub">
							<li class="{{($data['content'] == 'cms-pages') ? 'active' : ''}}"><a href="{{ URL::to('admin/cms/cms-pages')}}">CMS Pages</a></li>
							<li class="{{($data['content'] == 'cms-blocks') ? 'active' : ''}}"><a href="{{ URL::to('admin/cms/cms-blocks')}}">CMS Blocks</a></li>
					  		<li class="{{($data['content'] == 'emails') ? 'active' : ''}}"><a href="{{ URL::to('admin/cms/emails')}}">Emails</a></li>
					  </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" class="{{($data['section'] == 'content') ? 'active' : ''}}">
                          <i class="fa fa-book"></i>
                          <span>Content</span> 
                      </a> 
                      <ul class="sub">
                          <li class="{{($data['content'] == 'banners') ? 'active' : ''}}"><a href="{{ URL::to('admin/content/banners')}}">Banners</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" class="{{($data['section'] == 'items') ? 'active' : ''}}">
                          <i class="fa fa-cubes"></i>
                          <span>Items</span>
                      </a>
                      <ul class="sub">
                          <li class="{{(($data['content'] == 'manage') && ($data['section'] == 'items')) ? 'active' : ''}}"><a  href="{{ URL::to('admin/items/manage')}}">Manage Items</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" class="{{($data['section'] == 'orders') ? 'active' : ''}}">
                          <i class="fa fa-credit-card"></i>
                          <span>Orders</span>
                      </a>
                      <ul class="sub">
                          <li class="{{(($data['content'] == 'manage') && ($data['section'] == 'orders')) ? 'active' : ''}}"><a  href="{{ URL::to('admin/orders/manage')}}">Manage Orders</a></li>
                          <li class="{{(($data['content'] == 'manage-requests') && ($data['section'] == 'orders')) ? 'active' : ''}}"><a  href="{{ URL::to('admin/orders/manage-requests')}}">Manage Requests</a></li>
					  </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" class="{{($data['section'] == 'events') ? 'active' : ''}}">
                          <i class="fa fa-calendar"></i>
                          <span>Events</span>
                      </a>
                      <ul class="sub">
                          <li class="{{(($data['content'] == 'manage') && ($data['section'] == 'events')) ? 'active' : ''}}"><a  href="{{ URL::to('admin/events/manage')}}">Manage Events</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" class="{{($data['section'] == 'contact') ? 'active' : ''}}">
                          <i class="fa fa-users"></i>
                          <span>Contacts</span>
                      </a>
                      <ul class="sub">
							<li class="{{(($data['content'] == 'manage') && ($data['section'] == 'contact')) ? 'active' : ''}}"><a  href="{{ URL::to('admin/contact/manage')}}">Manage Inquiries</a></li>
                      </ul>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> {{$data['content_title']}}</h3>
			@if(Session::has('message'))
				@foreach(Session::get('message') as $type => $messages)
					<div class="alert alert-{{$type}}">
						<ul>
							@foreach ($messages as $message)
								<li>{!! html_entity_decode(e($message)) !!}</li>
							@endforeach
						</ul>
					</div>
				@endforeach
			@endif
          	<div class="row mt">
          		<div class="col-lg-12">
					<div id="admin-content">
						@include($data['content_template'])
					</div>
          		</div>
          	</div>
			
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
			{{$data['website_name']}}
              <a class="go-top" title="Back to Top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>
	
	<div id="overlay"></div>
	
	<div class="loaders-container">
 		<div class="loader spinner"></div>
		
  		<div class="loader bubble">
		  <div class="double-bounce1"></div>
		  <div class="double-bounce2"></div>
		</div>
		
 		<div class="loader cubes">
		  <div class="cube1"></div>
		  <div class="cube2"></div>
		</div>

 		<div class="loader square"></div>
		
  		<div class="loader sk-cube-grid">
		  <div class="sk-cube sk-cube1"></div>
		  <div class="sk-cube sk-cube2"></div>
		  <div class="sk-cube sk-cube3"></div>
		  <div class="sk-cube sk-cube4"></div>
		  <div class="sk-cube sk-cube5"></div>
		  <div class="sk-cube sk-cube6"></div>
		  <div class="sk-cube sk-cube7"></div>
		  <div class="sk-cube sk-cube8"></div>
		  <div class="sk-cube sk-cube9"></div>
		</div>
		
		<div class="loader sk-folding-cube">
		  <div class="sk-cube1 sk-cube"></div>
		  <div class="sk-cube2 sk-cube"></div>
		  <div class="sk-cube4 sk-cube"></div>
		  <div class="sk-cube3 sk-cube"></div>
		</div>
	</div>
	
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{ asset('local/resources/assets/js/admin/bootstrap.min.js') }}"></script>
    <script src="{{ asset('local/resources/assets/js/admin/jquery-ui-1.9.2.custom.min.js') }}"></script>
    <script src="{{ asset('local/resources/assets/js/admin/jquery.ui.touch-punch.min.js') }}"></script>
    <script class="include" type="text/javascript" src="{{ asset('local/resources/assets/js/admin/jquery.dcjqaccordion.2.7.js') }}"></script>
    <script src="{{ asset('local/resources/assets/js/admin/jquery.scrollTo.min.js') }}"></script>
    <script src="{{ asset('local/resources/assets/js/admin/jquery.nicescroll.js') }}" type="text/javascript"></script>

    <!--common script for all pages-->
    <script src="{{ asset('local/resources/assets/js/admin/common-scripts.js') }}"></script>

  </body>
</html>