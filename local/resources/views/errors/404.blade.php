@extends('layout.default')
@section('content')
	<div class="container">
		@if ($page->title)
			<h1 class="page-title">{{$page->title}}</h1>
		@endif
		
		<div class="page-content">
		{!! html_entity_decode(e($page->page_content)) !!}
		</div>
	</div>
@stop