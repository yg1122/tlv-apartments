@extends('layout.default')
@section('content')
{{--*/ $item = $page->item /*--}}
<div id="checkout">
	<div class="container">
		@if (isset($page->title) && $page->title)
			<h1 class="page-title">{{$page->title}}</h1>
		@endif
		
		<div class="page-content">
			<div class="order-summary">
				<div class="sub-title">Order Summary</div>
				<div class="content">
					<div class="image">
						<?php $imageName = (isset($item->image['resized_image']) && $item->image['resized_image']) ? $item->image['resized_image'] : $item->image['name']; ?>
						<img src="{{ asset('local/resources/assets/img/product/' . $imageName) }}" alt="{{$item->image['alt']}}" title="{{$item->image['title']}}" /> 
					</div>
					<div class="order-details">
						<div class="name">{{$item->name}}</div>
						<div class="boxes-container">
							<div class="top">
								<div class="box checkin">
									<div class="value">{{$page->checkin}}</div>
									<svg version="1.1" style="enable-background:new 0 0 414.287 414.287;" width="60px" height="60px" x="0px" y="0px" viewBox="0 0 31.934 31.934" xml:space="preserve" >
										<g>
											<path d="M12.527,0L8.736,1.176H0.436v28.245h1.54V2.716h1.793L2.593,3.081v26.34l9.935,2.465V2.716h4.395v26.705h1.541V1.176    h-5.935L12.527,0L12.527,0z M10.2,19.229c-0.5,0-0.905-0.492-0.905-1.098S9.7,17.033,10.2,17.033s0.905,0.49,0.905,1.098    C11.104,18.736,10.7,19.229,10.2,19.229z" style="" fill=""></path>
											<circle cx="27.955" cy="5.503" r="3.542" style="" fill=""></circle>
											<polygon points="23.045,30.057 23.019,30.29 22.814,30.268 21.611,30.294 21.484,31.386 22.481,31.594 23.681,31.539     23.67,31.732 24.972,31.886 25.115,30.668 25.129,30.538 25.154,30.329 25.172,30.331 26.713,25.271 24.512,25.115   " style="" fill=""></polygon>
											<polygon points="31.248,30.036 31.227,29.889 31.24,29.886 30.072,25.658 29.842,20.417 30.231,20.361 30.231,9.568 26.75,9.568     25.172,9.568 25.278,15.493 26.679,9.932 27.025,10.047 29.219,10.6 29.461,10.66 29.399,10.903 27.08,20.1 27.019,20.342     26.775,20.28 26.717,20.254 26.711,20.484 28.805,20.583 28.509,25.355 27.836,25.328 29.151,30.372 28.949,30.404 27.812,30.848     27.986,31.934 28.986,31.771 30.127,31.393 30.168,31.583 31.465,31.376 31.271,30.168   " style="" fill=""></polygon>
											<path d="M28.416,20.842l-2.015-0.119l0.041-0.82l0.313,0.082l2.279-9.055L27.017,10.4l-1.647,6.403l0.002,0.096l-0.635,2.556    l0.534,0.14l-0.489,0.182l-0.054,0.857l-2.263-0.16l-0.225,4.203l5.938,0.397L28.416,20.842z M25.904,20.592l-0.662-0.04    l0.022-0.394l0.662,0.039L25.904,20.592z" style="" fill=""></path>
										</g>
									</svg>
					<!-- 			<svg version="1.1" x="0px" y="0px" width="60px" height="60px" viewBox="0 0 470.5 470.5" style="enable-background:new 0 0 470.5 470.5;" xml:space="preserve" >
										<g>
											<path d="M53.771,0v469.931h34.134V34.134h232.192L185.262,57.91v366.811l185.67,45.779v-67.14h11.664v67.14h34.133V0H53.771z    M382.596,34.134v42.678h-11.664V34.134H382.596z M217.994,258.876c-6.768,0-12.255-7.14-12.255-15.946   c0-8.806,5.487-15.946,12.255-15.946s12.254,7.14,12.254,15.946C230.248,251.736,224.762,258.876,217.994,258.876z    M370.934,350.262V129.909h11.662v220.353H370.934z"></path>
										</g>
									</svg> -->
									<div class="label">Check In</div>
								</div>
								<div class="box arrow">
									<svg style="enable-background:new 0 0 414.287 414.287;" width="60px" height="60px" version="1.1" x="0px" y="0px" viewBox="0 0 414.287 414.287" xml:space="preserve" >
										<g>
											<path d="M412.398,202.993L288.535,95.289c-1.502-1.305-3.591-1.699-5.463-1.027c-1.873,0.67-3.237,2.3-3.568,4.261l-8.938,52.887   H40.554c-2.684,0-4.976,1.938-5.423,4.583l-3.054,18.067c-0.27,1.597,0.177,3.23,1.223,4.467c1.045,1.237,2.581,1.95,4.2,1.95   h103.982l-2.208,15H8.134c-2.684,0-4.976,1.938-5.423,4.583l-2.634,15.584c-0.27,1.597,0.177,3.229,1.223,4.467   c1.045,1.236,2.581,1.949,4.2,1.949h159.196l-2.206,15H63.505c-2.684,0-4.976,1.938-5.423,4.583l-2.505,14.817   c-0.271,1.597,0.177,3.229,1.222,4.467c1.046,1.237,2.582,1.95,4.201,1.95h190.721l-8.628,51.053   c-0.366,2.168,0.593,4.346,2.439,5.538c0.908,0.586,1.945,0.879,2.982,0.879c1.07,0,2.141-0.312,3.067-0.935l160.272-107.704   c1.421-0.955,2.316-2.518,2.422-4.225C414.385,205.776,413.689,204.116,412.398,202.993z"></path>
										</g>
									</svg>
								</div>
								<div class="box checkout">
									<div class="value">{{$page->checkout}}</div>
										<svg style="enable-background:new 0 0 414.287 414.287;" width="60px" height="60px" version="1.1" x="0px" y="0px" viewBox="0 0 492.5 492.5" xml:space="preserve" >
											<g>
												<path d="M285.048,21.72L164.234,0v21.72H79.292v433.358h31.403V53.123h53.539V492.5l208.15-37.422v-61.235h9.421v61.235h31.403   V21.72H285.048z M202.526,263.129c-6.997,0-12.67-7.381-12.67-16.486c0-9.104,5.673-16.485,12.67-16.485s12.67,7.381,12.67,16.485   C215.196,255.748,209.523,263.129,202.526,263.129z M381.805,344.646h-9.421V142.621h9.421V344.646z M381.805,93.423h-9.421v-40.3   h9.421V93.423z"></path>
											</g>
										</svg>
									<div class="label">Check Out</div>
								</div>
							</div>
							<div class="bottom">
								<div class="box guests">
									<div class="value">{{$page->guestsNumber}}</div>
										<svg version="1.1" x="0px" y="0px" width="60px" height="60px" viewBox="0 0 80.13 80.13" style="enable-background:new 0 0 80.13 80.13;" xml:space="preserve" >
											<g>
												<path d="M48.355,17.922c3.705,2.323,6.303,6.254,6.776,10.817c1.511,0.706,3.188,1.112,4.966,1.112   c6.491,0,11.752-5.261,11.752-11.751c0-6.491-5.261-11.752-11.752-11.752C53.668,6.35,48.453,11.517,48.355,17.922z M40.656,41.984   c6.491,0,11.752-5.262,11.752-11.752s-5.262-11.751-11.752-11.751c-6.49,0-11.754,5.262-11.754,11.752S34.166,41.984,40.656,41.984   z M45.641,42.785h-9.972c-8.297,0-15.047,6.751-15.047,15.048v12.195l0.031,0.191l0.84,0.263   c7.918,2.474,14.797,3.299,20.459,3.299c11.059,0,17.469-3.153,17.864-3.354l0.785-0.397h0.084V57.833   C60.688,49.536,53.938,42.785,45.641,42.785z M65.084,30.653h-9.895c-0.107,3.959-1.797,7.524-4.47,10.088   c7.375,2.193,12.771,9.032,12.771,17.11v3.758c9.77-0.358,15.4-3.127,15.771-3.313l0.785-0.398h0.084V45.699   C80.13,37.403,73.38,30.653,65.084,30.653z M20.035,29.853c2.299,0,4.438-0.671,6.25-1.814c0.576-3.757,2.59-7.04,5.467-9.276   c0.012-0.22,0.033-0.438,0.033-0.66c0-6.491-5.262-11.752-11.75-11.752c-6.492,0-11.752,5.261-11.752,11.752   C8.283,24.591,13.543,29.853,20.035,29.853z M30.589,40.741c-2.66-2.551-4.344-6.097-4.467-10.032   c-0.367-0.027-0.73-0.056-1.104-0.056h-9.971C6.75,30.653,0,37.403,0,45.699v12.197l0.031,0.188l0.84,0.265   c6.352,1.983,12.021,2.897,16.945,3.185v-3.683C17.818,49.773,23.212,42.936,30.589,40.741z"></path>
											</g>
										</svg>
									<div class="label">Guests</div>
								</div>
							</div>
						</div>
					</div>
					<div class="price-details">
						<div class="field subtotal">
							<div class="label">Subtotal: </div>
							<div class="value">${{$page->priceBeforeTax}}</div>
						</div>
						<div class="field tax">
							<div class="label">Tax ({{$page->vat}}%): </div>
							<div class="value">${{$page->vatValue}}</div> 
						</div>
						<div class="field total">
							<div class="label">Total: </div>
							<div class="value">${{$page->totalPrice}}</div>
						</div>
					</div>
				</div>
			</div>
			<div class="guest-details">
				<div class="inner">
					<div class="sub-title">Guest Details</div>
					<div class="content">
						<form id="checkout-form" method="post" action="{{ URL::to('payment')}}">
							<div class="field-container">
								<div class="left">
									<div class="field first-name">
										<div class="label">First Name</div>
										<input name="first_name" value="{{ $page->customer_details['first_name'] }}" />
									</div>
								</div>
								<div class="right">
									<div class="field last-name">
										<div class="label">Last Name</div>
										<input name="last_name" value="{{ $page->customer_details['last_name'] }}" />
									</div>
								</div>
							</div>
							<div class="field-container">
								<div class="left">
									<div class="field email">
										<div class="label">Email Address</div>
										<input name="email" value="{{ $page->customer_details['email'] }}" />
									</div>
								</div>
								<div class="right">
									<div class="field phone">
										<div class="label">Phone Number</div>
										<input name="phone" value="{{ $page->customer_details['phone'] }}" />
									</div>
								</div>
							</div>
							<div class="field requests">
								<div class="label">Special Requests</div>
								<textarea name="requests">{{ $page->customer_details['requests'] }}</textarea>
							</div>
							<input type="hidden" name="total_price" value="{{$page->totalPrice}}" />
							{!! csrf_field() !!}
							
							<div class="error"></div>
							<div class="book-btn" title="Continue to Payment">Continue to Payment</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var page = 'checkout';
</script>
@stop