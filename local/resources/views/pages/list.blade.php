@extends('layout.default')
@section('content')
{{--*/ $userSearchData = $page->userSearchData /*--}}
<div class="container list-page">
	<h1 class="page-title">{{$page->title}}</h1>
	
	<div class="page-content">
		<div class="left-col">
			<div class="top-line">
				<div class="inner">Choose Dates</div>
			</div>
			<div class="search-panel">
				<form id="search-panel-form" method="get" action="{{ URL::to('/' . $page->listUrlKey)}}">
					<div class="location-field">
						<div class="label">Location</div>
						<select name="location" class="location">
							@foreach($page->cities as $city)
								<option value="{{$city}}" {{($city == $userSearchData['location']) ? 'selected="selected"' : ''}}>{{$city}}</option>
							@endforeach
						</select>
					</div>					
					<div class="label">Check In</div>
					<input value="{{isset($userSearchData['checkin_date']) ? $userSearchData['checkin_date'] : ''}}" id="datepicker-from" name="checkin_date" readonly />
					
					<div class="label">Check Out</div>
					<input value="{{isset($userSearchData['checkout_date']) ? $userSearchData['checkout_date'] : ''}}" id="datepicker-till" name="checkout_date" readonly />
					
					<div class="guest-fields">
						<div class="guests-field">
							<div class="label">Guest(s)</div>
							<input class="selection guests" value="{{isset($userSearchData['guests_number']) ? $userSearchData['guests_number'] : '2'}}" name="guests_number" />
							<div class="selectors">
								<div class="arrow top" title="Increase">&#9651;</div>
								<div class="arrow bottom" title="Decrease">&#9661;</div>
							</div>
						</div>
					</div>	
					<div class="error"></div>
					<div class="search-btn" title="Search">Search</div>
				</form>
			</div>
		</div>
		
		<div class="right-col">
			<div class="top-line">
				@if ($page->searchDatesString)
					<span class="results-for">Results for:</span>
					<span class="search-result-text">{{$page->searchDatesString}}</span> 
					<span class="clear-btn" title="Clear search results">
						<a href="{{URL::to($page->listUrlKey)}}">Clear</a>
					</span>
				@else
					All existing apartments
				@endif
			</div>
			<div class="items-container">
				@if ($page->items)
					@foreach ($page->items as $item)
					<div class="item">
						<div class="image-container">
							<a href="{{ URL::to('/' . $page->listUrlKey . '/' . $item->url_key)}}">
								<?php $imageName = (isset($item->list_image['resized_image']) && $item->list_image['resized_image']) ? $item->list_image['resized_image'] : $item->list_image['name']; ?>
								<img src="{{ asset('local/resources/assets/img/product/' . $imageName) }}" class="item-image" alt="{{ isset($item->list_image['alt']) ? $item->list_image['alt'] : '' }}" title="{{ isset($item->list_image['title']) ? $item->list_image['title'] : '' }}" />
							</a>
						</div>
						<div class="right-col">
							<div class="details">
								<div class="name">
									<a href="{{ URL::to('/' . $page->listUrlKey . '/' . $item->url_key)}}">{{$item->name}}</a>
								</div>
								<div class="short-description">{{$item->short_description}}</div>
								<ul class="specs">
									<li>Size: {{$item->size}}m<sup>2</sup></li>
									<li>Beds: {{$item->beds}}</li>
								</ul>
								<div class="amenities">
								<?php foreach($item->listAmenities as $amenity) :?>
									<div class="amenity" title="<?php echo $amenity['name']; ?>" style="background-position:<?php echo $amenity['background_pos']; ?>;"></div>
								<?php endforeach; ?>
								</div>
							</div>
							<div class="price-box">
								<div class="price">${{number_format($item->price + $item->cleaning_fee,2)}}</div>
								<a href="{{ URL::to('/' . $page->listUrlKey . '/' . $item->url_key)}}" title="Book Now">
									<div class="order-btn">Book Now</div>
								</a>
							</div>
						</div>						
					</div>
					@endforeach
				@else
					<div class="no-results-text">No available apartments were found for the required dates.</div>
				@endif
			</div>
			<div class="pagination">
				<div class="cubes">
				@if($page->availablePages > 1)
					@for ($i = 1; $i <= $page->availablePages; $i++)
						<a href="{{ $page->actionUrl . 'page=' . $i }}" class="page-number{{($page->pageNumber == $i) ? ' current' : ''}}" title="{{$i}}">{{$i}}</a>
					@endfor
				@endif
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var minDate = '{{$page->minDate}}';
	var userCheckInDate = '{{isset($userSearchData["checkin_date"]) ? $userSearchData["checkin_date"] : ""}}';
	var disabledDates = '{{$page->disabledDates}}';
	var page = 'list';
</script>
@stop