<form id="checkout-form" method="post" action="{{ URL::to('payment')}}">
	<input type="hidden" name="first_name" value="{{ $page->customer_details['first_name'] }}" />
	<input type="hidden" name="last_name" value="{{ $page->customer_details['last_name'] }}" />
	<input type="hidden" name="email" value="{{ $page->customer_details['email'] }}" />
	<input type="hidden" name="phone" value="{{ $page->customer_details['phone'] }}" />
	<input type="hidden" name="total_price" value="{{ $page->totalPrice }}" />
	<textarea name="requests" style="display:none !important;">{{ $page->customer_details['requests'] }}</textarea>
	{!! csrf_field() !!}
</form>
<script>
document.getElementById("checkout-form").submit();
</script>