@extends('layout.default')
@section('content')
{{--*/ $userSearchData = $page->user_search_data /*--}}

<div id="homepage">
	<div class="carousel">
		<div class="container search-bar">
			<div id="search-bar">
				<form id="search-panel-form" method="get" action="{{ URL::to('/' . $page->listUrlKey)}}">
					<div class="field location">
						<div class="label">Location</div>
						<select name="location" class="location">
							@foreach($page->cities as $city)
								<option value="{{$city}}" {{($city == $userSearchData['location']) ? 'selected="selected"' : ''}}>{{$city}}</option>
							@endforeach
						</select>
					</div>
					<div class="field checkin">
						<div class="label">Check In</div>
						<input value="{{isset($userSearchData['checkin_date']) ? $userSearchData['checkin_date'] : ''}}" id="datepicker-from" name="checkin_date" readonly />
					</div>
					<div class="field checkout">
						<div class="label">Check Out</div>
						<input value="{{isset($userSearchData['checkout_date']) ? $userSearchData['checkout_date'] : ''}}" id="datepicker-till" name="checkout_date" readonly />
					</div>
					<div class="field guests-field">
						<div class="label">Guests</div>
						<input class="selection guests" value="{{isset($userSearchData['guests_number']) ? $userSearchData['guests_number'] : '2'}}" name="guests_number" />
						<div class="selectors">
							<div class="arrow top" title="Increase">&#9651;</div>
							<div class="arrow bottom" title="Decrease">&#9661;</div>
						</div>
					</div>
					<div class="search-btn" title="Search">Search</div>
				</form>
			</div>
		</div>
	
		<div class="arrow arrow-carousel arrow-left"></div>
		<div class="arrow arrow-carousel arrow-right"></div>
		<div class="carousel-inner">
			@foreach($page->banners['homepage'] as $banner)
				<?php $imageName = (isset($banner->resized_image) && $banner->resized_image) ? $banner->resized_image : $banner->image_name; ?>
				<div class="img-wrapper">
					<a href="{{($banner->url) ? $banner->url : '#'}}" target="{{($banner->target) ? '_blank' : '_self'}}" >
						<img src="{{ asset('local/resources/assets/img/banners/' . $imageName) }}" alt="{{$banner->alt}}" title="{{$banner->title}}" />
					</a>
				</div>
			@endforeach
		</div>
	</div>
	<div class="container">
		<div class="homepage-specs">
			<?php 
			$banner = $page->banners['homepage-secondary'][0];
			$imageName = (isset($banner->resized_image) && $banner->resized_image) ? $banner->resized_image : $banner->image_name; 
			?>
			<a href="{{($banner->url) ? $banner->url : '#'}}" target="{{($banner->target) ? '_blank' : '_self'}}" >
				<img src="{{ asset('local/resources/assets/img/banners/' . $imageName) }}" alt="{{$banner->alt}}" title="{{$banner->title}}" />
			</a>
		</div>
	</div>
	<div class="main-content">
		<div class="container">{!! html_entity_decode(e($page->blocks['homepage-content'])) !!}</div>
	</div>
</div>
<script>
	var minDate = '{{$page->minDate}}';
	var userCheckInDate = '{{isset($userSearchData["checkin_date"]) ? $userSearchData["checkin_date"] : ""}}';
	var disabledDates = '{{$page->disabledDates}}';
	var page = 'homepage';
</script>
@stop