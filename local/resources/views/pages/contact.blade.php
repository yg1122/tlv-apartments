@extends('layout.default')
@section('content')
{{--*/ $banner = $page->banner /*--}}
	<div id="contact-us">
		<div class="container">
		@if (isset($page->title) && $page->title)
			<h1 class="page-title">{{$page->title}}</h1>
		@endif
		</div>
		
		<div class="page-content">
			<div class="top-section">
				<div class="container">
					<div class="content">{!! html_entity_decode(e($page->blocks['contact-us-content'])) !!}</div>
					<div class="banner">
						@if($banner)
						<?php $imageName = (isset($banner->resized_image) && $banner->resized_image) ? $banner->resized_image : $banner->image_name; ?>
						<a href="{{($banner->url) ? $banner->url : '#'}}" target="{{($banner->target) ? '_blank' : '_self'}}" >
							<img src="{{ asset('local/resources/assets/img/banners/' . $imageName) }}" alt="{{$banner->alt}}" title="{{$banner->title}}" />
						</a>
						@endif
					</div>
				</div>
			</div>
			<div class="bottom-section container">
				<div class="contact-form-container">
					<form id="contact-form" method="post" action="{{ URL::to('action/contact/handleInquiry')}}">
						<div class="top-fields">
							<div class="left">
								<div class="field name">
									<div class="label">Name</div>
									<input name="name" />
								</div>
							</div>
							<div class="right">
								<div class="field email">
									<div class="label">Email Address</div>
									<input name="email" />
								</div>
							</div>
						</div>
						<div class="field subject">
							<div class="label">Subject</div>
							<input name="subject" />
						</div>
						<div class="field message">
							<div class="label">Message</div>
							<textarea name="message"></textarea>
						</div>
						<div class="error"></div>
						<div class="send-btn" title="Send">Send</div>
						
						{!! csrf_field() !!}
					</form>
				</div>
				<div class="contact-details">{!! html_entity_decode(e($page->blocks['contact-us-details'])) !!}</div>
			</div>
		</div>
	</div>
<script>
	var page = 'contact';
</script>
@stop