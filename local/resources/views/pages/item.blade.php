@extends('layout.default')
@section('content')
{{--*/ $item = $page->item /*--}}
<div class="item-page">
{{-- 
	<div class="wrapper">
		<div class="placeholder">
			<img src="{{ asset('local/resources/assets/img/homepage/home33.jpg') }}" alt="image1" />
			<div class="shadow"></div>
		</div>
		<ul id="sb-slider" class="sb-slider">
			<li>
				<a href="#" target="_blank">
					<img src="{{ asset('local/resources/assets/img/homepage/home35.jpg') }}" alt="image1"/>
				</a>
			</li>
			<li>
				<a href="#" target="_blank">
					<img src="{{ asset('local/resources/assets/img/homepage/home34.jpg') }}" alt="image2"/>
				</a>
			</li>
			<div id="nav-arrows" class="nav-arrows">
				<a href="#">Next</a>
				<a href="#">Previous</a>
			</div>
		</ul>

		<div id="shadow" class="shadow"></div>

	</div>
--}}

	<div id="flipbook">
		<div class="arrow arrow-left"></div>
		<div class="arrow arrow-right"></div>
		
		@foreach($item->images as $image)
			<?php $imageName = (isset($image['resized_image']) && $image['resized_image']) ? $image['resized_image'] : $image['name']; ?>
			<div class="slide">
				<img src="{{ asset('local/resources/assets/img/product/' . $imageName) }}" alt="$image['alt']" title="$image['title']" /> 
			</div>
		@endforeach
	</div>

	<div class="top-stripe">
		<div class="main-title">
			<div class="container">
				<div class="left-section">
					<h1>{{$item->name}}</h1>
					<div class="location">{{ $item->city . ', ' . $item->country }}</div>
				</div>
				<div class="right-section">
					<img class="map-icon" src="{{ asset('local/resources/assets/img/icons/map-icon.png') }}" title="Show map" alt="Show map" />
				</div>
			</div>
		</div>
		<div class="container amenities">
			<div class="amenity room-type">
				<div class="icon">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 32 32" xml:space="preserve">
						<g>
							<path d="M1.38,15.408l13.288-0.527l8.085-9.339l7.728,11.579L32,16.903l-9.09-12.91l-9.283,1.719v-1.65l-0.765-0.158l-2.304,0.487    V6.28L7.463,6.854L0,13.633v1.785C0,15.419,0.763,15.399,1.38,15.408z"></path>
							<path d="M22.702,6.342l-7.804,9.016L2.155,15.865v10.82l11.708,1.406v0.004l7.319-0.854v-8.43l3.368-0.005v8.041l5.396-0.661    v-8.99L22.702,6.342z M7.126,23.309L3.733,23.09v-4.33l3.393,0.002V23.309z M12.862,23.655l-3.945-0.253v-4.66l3.945,0.001V23.655    z M19.521,23.453l-4.165,0.273v-4.94l4.165-0.003V23.453z M28.971,22.861l-3.145,0.207v-4.275l3.145-0.004V22.861z"></path>
						</g>
					</svg>
					{{--<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 57.204 57.203" xml:space="preserve" style="width: 50px;height: 50px;fill: #bcc9cd;">
						<g>
							<path d="M7.745,33.974l2.497,21.231h36.597l2.873-21.231l-21.109-16.55L7.745,33.974z M27.854,50.022h-7.618l-0.48-7.993h8.098    V50.022z M27.854,40.529h-8.188l-0.554-9.241h8.743L27.854,40.529L27.854,40.529z M29.353,31.287h8.743l-0.617,9.241h-8.125    V31.287z M29.353,42.029h8.026l-0.533,7.993h-7.493V42.029z"></path>
							<polygon points="57.204,37.844 49.585,19.61 28.838,1.998 24.867,5.298 24.505,9.805 24.48,13.854 13.134,22.742 12.4,15.659     7.495,19.735 0,37.844 28.838,15.113   "></polygon>
							<polygon points="13.74,21.12 23.733,13.292 23.733,9.879 12.574,9.879   "></polygon>
							<path d="M24.106,4.674h-9.908l-1.625,4.683h11.158L24.106,4.674z M22.171,8.358H14.81l0.875-2.685h6.799L22.171,8.358z"></path>
						</g>
					</svg>--}}
				</div>
				<div class="text">{{$item->property_type}}</div>
			</div>
			<div class="amenity size">
				<div class="icon">{{$item->size}}m<sup>2</sup></div>
				<div class="text">size</div>
			</div>
			<div class="amenity guests-number">
				<div class="icon">{{$item->max_guests}}</div>
				<div class="text">{{($item->max_guests > 1) ? 'guests' : 'guest'}}</div>
			</div>
			<div class="amenity bedrooms-number">
				<div class="icon">
					<div class="icon-text">{{$item->bedrooms}}</div>
						{{--<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="imgView" x="0px" y="0px" width="256px" height="256px" viewBox="0 0 494.239 494.238" style="display: block;" xml:space="preserve" class="detail convertSvgInline replaced-svg" data-id="59801" data-kw="open203" fill="#000000">
						<g>
							<path d="M199.725,0v36.025H85.211v421.66l114.514,0.094v36.459l209.085-37.555l0.216-418.867L199.725,0z M234.404,230.574   c7.022,0,12.715,7.408,12.715,16.545c0,9.139-5.692,16.545-12.715,16.545s-12.715-7.406-12.715-16.545   C221.688,237.982,227.382,230.574,234.404,230.574z M119.211,423.713V70.025h80.514v353.753L119.211,423.713z" style="" fill=""></path>
						</g>
						</svg>--}}
				</div>
				<div class="text">{{($item->bedrooms > 1) ? 'bedrooms' : 'bedroom'}}</div>
			</div>
			<div class="amenity beds-number">
				<div class="icon">
					<div class="icon-text">{{$item->beds}}</div>
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 589.05 589.05" xml:space="preserve">
						<g>
							<path d="M557.175,196.987c-17.85,0-31.875,14.025-31.875,31.875v80.325h-459V165.112c0-17.85-14.025-31.875-33.15-31.875S0,147.263,0,165.112v258.825c0,17.85,14.025,31.875,33.15,31.875s33.15-14.025,33.15-31.875v-36.975h459v36.975c0,17.85,14.025,31.875,31.875,31.875c17.851,0,31.875-14.025,31.875-31.875V228.862C589.05,211.013,575.025,196.987,557.175,196.987z" style="" fill=""></path>
							<circle cx="136.425" cy="239.063" r="45.9" style="" fill=""></circle>
							<path d="M238.425,284.963h219.3c22.949,0,40.8-19.125,40.8-40.8c0-22.95-17.851-40.8-40.8-40.8h-219.3c-22.95,0-40.8,19.125-40.8,40.8C197.625,265.837,216.75,284.963,238.425,284.963z" style="" fill=""></path>
						</g>
					</svg>
				</div>
				<div class="text">{{($item->beds > 1) ? 'beds' : 'bed'}}</div>
			</div>
		</div>
	</div>
	<div id="details">
		<div class="container details">
			<div class="left">
				<div class="row about">
					<div class="price-box">
						<span>$</span><span>{{$item->price}}</span>
						<div class="text">Price per night</div>
					</div>
					<div class="label">About the place</div>
					<div class="content">{!! html_entity_decode(nl2br(e($item->about))) !!}</div>
				</div>
				<div class="row place-details">
					<div class="label">The Space</div>
					<div class="content">
						<div class="left-section">
							<div>Bed type: <span class="bold">{{$item->bed_type}}</span></div>
							<div>Property type: <span class="bold">{{$item->property_type}}</span></div>
							<div>Accommodates: <span class="bold">{{$item->max_guests}}</span></div>
						</div>
						<div class="right-section">
							<div>Bedrooms: <span class="bold">{{$item->bedrooms}}</span></div>
							<div>Beds: <span class="bold">{{$item->beds}}</span></div>
							<div>Bathrooms: <span class="bold">{{$item->bathrooms}}</span></div>
						</div>
					</div>
				</div>
				<div class="row amenities">
					<div class="label">Amenities</div>
					<div class="content">
						<?php $counter = 1; ?>
						@foreach($item->amenitiesArr as $key => $value)
							<?php echo ($counter%2 != 0) ? '<div class="row">' : ''; ?>
								<div class="{{($counter%2 == 0) ? 'right' : 'left'}} {{ ($item->amenities[$value]) ? 'exist' : 'line-through'}}">
									<div class="icon"></div>
									<div class="name">{{$value}}</div>
								</div>
							<?php echo ($counter%2 == 0) ? '</div>' : ''; $counter++; ?>
						@endforeach
					</div>
				</div>
				<div class="row price">
					<div class="label">Prices</div>
					<div class="content">
						<div>Cleaning Fee: <span class="bold">{{$item->cleaning_fee}}</span></div>
						<div>Cancellation: <span class="bold">{!! nl2br(html_entity_decode(e($item->cancellation))) !!}</span></div>
					</div>
				</div>
				<div class="row description">
					<div class="label">Description</div>
					<div class="content">{!! html_entity_decode(nl2br(e($item->description))) !!}</div>
				</div>
				@if(isset($item->rules) && $item->rules)
				<div class="row house-rules">
					<div class="label">House Rules</div>
					<div class="content">{!! html_entity_decode(nl2br(e($item->rules))) !!}</div>
				</div>
				@endif
				@if(isset($item->coordinates[0]) && $item->coordinates[0])
				<div id="map">
					<div id="googleMap"></div>
				</div>
				@endif
			</div>
			<div class="right">
				<form class="booking-form" method="post" action="{{ URL::to('action/checkout/prepareReservation')}}">
					<div class="date-inputs">
						<div class="checkin-field">
							<div class="label">Check In</div>
							<input value="{{isset($page->userSearchData['checkin_date']) ? $page->userSearchData['checkin_date'] : ''}}" id="datepicker-from" name="checkin_date" readonly />
						</div>
						<div class="checkout-field">
							<div class="label">Check Out</div>
							<input value="{{isset($page->userSearchData['checkout_date']) ? $page->userSearchData['checkout_date'] : ''}}" id="datepicker-till" name="checkout_date" readonly />
						</div>
					</div>
					<div class="guests-field">
						<div class="label">Guest(s)</div>
						<input class="selection guests" value="{{isset($page->userSearchData['guests_number']) ? $page->userSearchData['guests_number'] : '2'}}" name="guests_number" />
						<div class="selectors">
							<div class="arrow top" title="Increase">&#9651;</div>
							<div class="arrow bottom" title="Decrease">&#9661;</div>
						</div>
					</div>
					<div class="error"></div>
					<div class="book-btn-wrapper">
						<div class="book-btn" title="Book Now">{{$page->requestBooking ? 'Request to Book' : 'Book Now'}}</div>
					</div>
					<input type="hidden" value="{{$item->id}}" name="item_id" />
					<input type="hidden" value="{{$item->city}}" name="city" />
					{!! csrf_field() !!}
				</form>
			</div>
		</div>
	</div>
	<div id="booking-request-popup">
		<form id="booking-request-form" method="post" action="{{ URL::to('payment')}}">
			<div class="field-container">
				<div class="left">
					<div class="field first-name">
						<div class="label">First Name</div>
						<input name="first_name" />
					</div>
				</div>
				<div class="right">
					<div class="field last-name">
						<div class="label">Last Name</div>
						<input name="last_name" />
					</div>
				</div>
			</div>
			<div class="field-container">
				<div class="field email">
					<div class="label">Email Address</div>
					<input name="email" />
				</div>
			</div>
			<div class="field requests">
				<div class="label">Special Requests</div>
				<textarea name="requests"></textarea>
			</div>
			{!! csrf_field() !!}
			
			<div class="error"></div>
			<div class="request-booking-btn" title="Send Request">Send Request</div>
		</form>
		<div class="close-btn-wrapper" title="Close">
			<svg class="close-btn" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" width="17px" height="17px" viewBox="0 0 31.816 31.816" style="enable-background:new 0 0 31.816 31.816;" xml:space="preserve" >
				<g>
					<path d="M30.922,9.343c0.562-0.525,0.885-1.259,0.895-2.028c0.008-0.77-0.301-1.51-0.852-2.048L27.143,1.53   c-0.535-0.523-1.256-0.812-2.006-0.803c-0.748,0.009-1.461,0.314-1.982,0.85l-7.197,7.377L8.862,1.562   C8.303,0.979,7.537,0.643,6.73,0.627C5.923,0.61,5.142,0.916,4.561,1.475l-3.695,3.55C0.31,5.558-0.002,6.294,0,7.065   c0.002,0.77,0.318,1.505,0.876,2.036l7.746,7.368l-6.858,7.028c-1.046,1.071-1.071,2.774-0.058,3.879l2.67,2.901   c0.509,0.556,1.218,0.881,1.97,0.909c0.751,0.028,1.483-0.244,2.032-0.759l7.522-7.035l7.339,6.98   c0.545,0.519,1.272,0.797,2.024,0.772c0.752-0.021,1.463-0.344,1.979-0.895l2.691-2.877c1.022-1.097,1.014-2.8-0.023-3.88   l-6.68-6.964L30.922,9.343z"></path>
				</g>
			</svg>
		</div>
	</div>
</div>

<script>
	var minDate = '{{$item->minDate}}';
	var userCheckInDate = '{{isset($page->userSearchData["checkin_date"]) ? $page->userSearchData["checkin_date"] : ""}}';
	var disabledDates = '{{$item->disabledDates}}';
	var requestBooking = '{{$page->requestBooking}}';
	var requestBookingAction = '{{ URL::to("action/checkout/requestBooking")}}';
	var page = 'item';

	@if(isset($item->coordinates[0]) && $item->coordinates[0])
		var coordinates = {lat: {{$item->coordinates[0]}}, lng: {{$item->coordinates[1]}}};
	@else
		var coordinates = false;
	@endif
	
	// Init Google map
	function initializeGoogleMap() {
		if(!coordinates) return;
		
		var map = new google.maps.Map(document.getElementById('googleMap'), {
			zoom: 17,
			center: coordinates
		});

		var marker = new google.maps.Marker({
			position: coordinates,
			map: map
		});
		
		// Resize stuff.
		google.maps.event.addDomListener(window, "resize", function() {
		   var center = map.getCenter();
		   google.maps.event.trigger(map, "resize");
		   map.setCenter(center); 
		});
		
		setTimeout(function(){ window.dispatchEvent(new Event('resize')); }, 500);
	}
	
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?callback=initializeGoogleMap"></script>
@stop