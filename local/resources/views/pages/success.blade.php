@extends('layout.default')
@section('content')
{{--*/ $order = $page->order /*--}}
<div id="success-page">
	<div class="container">
		@if (isset($page->title) && $page->title)
			<h1 class="page-title">{{$page->title}}</h1>
		@endif
	</div>
	<div class="page-content">
		<div class="container">
			<div class="success-message">{{$order['customer_details']['first_name']}} {{$order['customer_details']['last_name']}}, your reservation is complete.</div>
			<div class="order-details">
				<div class="left-section">
					<div class="order-number-box">
						<div>Your reservation number: <span class="order-id">{{$order['order_id']}}</span></div>
						<div>A confirmation email will be sent to you.</div>
					</div>
					<div class="hosting-dates">
						<div class="content">
							<div class="checkin">
								<div class="field">
									<div class="label">Check In Time</div>
									<div class="value">{{$order['checkin_time']}}</div>
								</div>
							</div>
							<div class="checkout">
								<div class="field">
									<div class="label">Check Out Time</div>
									<div class="value">{{$order['checkout_time']}}</div>
								</div>
							</div>
						</div>
					</div>
					<div class="property-details">
						<div class="title">Property Details</div>
						<div class="field phone">
							<div class="label">Phone Number:</div>
							<div class="value">{{$page->contact_phone}}</div>
						</div>
						<div class="field email">
							<div class="label">Email Address:</div>
							<div class="value">{{$page->general_email}}</div>
						</div>
						<div class="field address">
							<div class="label">Address:</div>
							<div class="value">{{$order['location']['street'] . ' ' . $order['location']['house_number'] . ', ' . $order['location']['city'] . ', ' . $order['location']['country']}}</div>
						</div>
					</div>
				</div>
				<div class="right-section">
					<div class="box">
						<div class="image">
							@if(isset($order['image']) && $order['image'])
								<?php $imageName = (isset($order['image']['resized_image']) && $order['image']['resized_image']) ? $order['image']['resized_image'] : $order['image']['name']; ?>
								<img src="{{ asset('local/resources/assets/img/product/' . $imageName) }}" alt="{{$order['image']['alt']}}" title="{{$order['image']['title']}}" /> 
							@endif	
						</div>
						<div class="bottom">
							<div class="name">{{$order['property_name']}}</div>
							<div class="inner">
								<div class="details">
									<div class="field property-type">
										<div class="label">Property Type</div>
										<div class="value">{{$order['property_type']}}</div>
									</div>
									<div class="field checkin">
										<div class="label">Check In</div>
										<div class="value">{{$order['checkin']}}</div>
									</div>
									<div class="field checkout">
										<div class="label">Check Out</div>
										<div class="value">{{$order['checkout']}}</div>
									</div>
									<div class="field guests">
										<div class="label">Guest(s)</div>
										<div class="value">{{$order['guests_number']}}</div>
									</div>
								</div>
								<div class="price-box">
									<div class="field subtotal">
										<div class="label">Subtotal</div>
										<div class="value">${{$order['subtotal']}}</div>
									</div>
									<div class="field vat">
										<div class="label">Tax ({{$order['vat']}}%)</div>
										<div class="value">${{$order['vat_value']}}</div>
									</div>
									<div class="field total">
										<div class="label">Total</div>
										<div class="value">${{$order['total_price']}}</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop