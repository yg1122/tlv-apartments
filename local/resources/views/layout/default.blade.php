<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0" /> 
	<meta name="description" content="{{ $data['seo']['meta_description'] }}">
	<meta name="keywords" content="{{ $data['seo']['meta_keywords'] }}">
	<meta name="robots" content="{{ $data['seo']['robots'] }}" />
	<link rel="shortcut icon" href="{{ asset('local/resources/assets/img/icons/' . $data['favicon']) }}" />
	
	<title>{!! isset($page->title) ? $page->title : $data['seo']['meta_title'] !!}</title>
	<link href="{{ asset('local/resources/assets/css/style.css') }}" rel="stylesheet" />
</head>
<body>
	<div class="main-container">
	  <header> @include('layout.header') </header>
		@if(Session::has('message'))
			@foreach(Session::get('message') as $type => $messages)
			<div class="message-container {{$type}}">
				<div class="container">
					<ul>
					@foreach ($messages as $message)
						<li>{!! html_entity_decode(e($message)) !!}</li>
					@endforeach
					</ul>
				</div>
			</div>
			@endforeach
		@endif
	  <div class="contents"> @yield('content') </div>
	  <footer> @include('layout.footer') </footer>
	</div>
	<div id="overlay"></div>
	<script type="text/javascript" src="{{ asset('local/resources/assets/js/compressed/compressed.js') }}"></script>
</body>
</html>