<div class="nav-row">
<?php $location = Request::segment(1); ?>
	<div class="categories">
		@foreach ($data['categories'] as $ctg)
			<?php $url = ($ctg->url_key == $location) ? URL::to('/' . $ctg->url_key . '#') : URL::to($ctg->url_key); ?>
			<a href="{!! $url !!}" class="ctg-name {!! $ctg->id !!}{!!($ctg->url_key == $location) ? ' current' : ''!!}">{!! $ctg->name !!}</a>
		@endforeach
	</div>
</div>