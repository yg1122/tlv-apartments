<div id="footer">
	{{--*/ $footer = $data['footer'] /*--}}
	<div class="top">
		<div class="container">{!! html_entity_decode(e($footer['content'])) !!}</div>
	</div>
	
	<div class="bottom">
		@include('layout.navigation')
		<div class="credits">{!! html_entity_decode(e($footer['credits'])) !!}</div>
	</div>
</div>