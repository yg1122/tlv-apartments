<div id="header">
	{{--*/ $header = $data['header'] /*--}}
	
	<div class="top-row">
		<div class="container">{!! html_entity_decode(e($header['content'])) !!}</div>
	</div>
	@include('layout.navigation')
</div>