$(function(){
	// General
	page = (typeof page !== 'undefined') ? page : false;
	
	function validateEmail(email) {
		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		return re.test(email);
	}

	// List Page
	var datePickerFrom = $("#datepicker-from");
	var datePickerTill = $("#datepicker-till"); 
	disabledDates = (typeof disabledDates !== 'undefined') ? disabledDates : false;
	var errorElem = $(".error");
	errorElem = (errorElem.length > 0) ? errorElem : false;
	
	$('.selectors').on('mouseover', '.arrow', function(){ 
		if($(this).hasClass("top")){
			$(this).html("&#9650;");
		}else{
			$(this).html("&#9660;");
		}
	}).on('mouseout', '.arrow', function(){
		if($(this).hasClass("top")){
			$(this).html("&#9651;");
		}else{
			$(this).html("&#9661;");
		}
	}).on('click', '.arrow', function(){
		var increaseBy = $(this).hasClass("top") ? 1 : -1;
		var currentInput = $(this).parent().prev();
		var currentInputVal = currentInput.val();

		if((currentInputVal <= 0) && increaseBy == -1) return;
		
		currentInput.val(parseInt(currentInputVal) + increaseBy);

	});
	
	if((page == 'list') ||(page == 'item') || (page == 'homepage')){
		$('.search-btn, .book-btn').click(function(){
			
			if(errorElem) errorElem.html("");
			var response = validateSearchForm();
			
			if(response === true){
				if((page == 'item') && (requestBooking === '1'))
				{
					// If request booking mode is enabled, show the request booking popup.
					toggleRequestBookingPopup(true);
					return;
				}
				$("#search-panel-form, .booking-form").submit();
			}else{
				if(errorElem){
					errorElem.html(response);
				}else{
					alert(strip(response));
				}	
			}
			
		});
	}
	
	var overlay = $('#overlay');
	var requestBookingPopup = $('#booking-request-popup');
	function toggleRequestBookingPopup(show)
	{
		if(show){
			requestBookingPopup.fadeIn();
			overlay.fadeIn();
		}else{
			requestBookingPopup.fadeOut();
			overlay.fadeOut();
		}
	}
	
	function strip(html)
	{
	   var tmp = document.createElement("DIV");
	   tmp.innerHTML = html;
	   return tmp.textContent || tmp.innerText || "";
	}
	
	function validateSearchForm(){
		
		var checkInDate = datePickerFrom.val();
		var checkOutDate = datePickerTill.val();
		
		// Check if empty.
		if(!checkInDate || !checkOutDate){
			return "Please choose hosting dates.";
		}
		
		// Check if one of the values is disabled.
		if(disabledDates.indexOf(checkInDate) != -1){
			return "Invalid check in date.";
		}
		if(disabledDates.indexOf(checkOutDate) != -1){
			return "Invalid check out date.";		
		}
		
		// Check if one of the dates between the two values is disabled.
		var tomorrow = false;
		var daysAfter = 1;
		var disabledDaysToInform = [];
		var isFreeWeek = true;
		
		while(checkOutDate != tomorrow){
			tomorrow = getNextDays(checkInDate, daysAfter);
			if(disabledDates.indexOf(tomorrow) != -1){
				isFreeWeek = false;
				disabledDaysToInform.push(tomorrow);
			}
			
			daysAfter++;
		}
		
		// If one of the days in the range is disabled, throw an error.
		if(!isFreeWeek){
			var disabledDaysToInformText = '<span class="error-dates">';
			for(var i=0;i<disabledDaysToInform.length;i++){
				disabledDaysToInformText += disabledDaysToInform[i] + ", ";
			}
			disabledDaysToInformText = disabledDaysToInformText.substring(0, disabledDaysToInformText.length - 2);
			disabledDaysToInformText += '</span>';
			
			return "The following chosen dates are not available: " + disabledDaysToInformText;		
		}
		
		if((!parseInt($('.selection.guests').val()))){
			return "Please choose number of guests.";
		}
		
		// Check if the check out date is previous to the check in date.
		if(strToDate(checkInDate) > strToDate(checkOutDate)){
			return "The check out date is previous to the check in date.";
		}
		
		return true;
	}
	
	function strToDate(str){
	
		var dateStrArr = str.split("/");
		var newDateStr = dateStrArr[1] + '/' + dateStrArr[0] + '/' + dateStrArr[2];
		
		return new Date(newDateStr);
	}
	 

	userCheckInDate = (typeof userCheckInDate !== 'undefined') ? userCheckInDate : false;
	minDate = (typeof minDate !== 'undefined') ? minDate : false;
	var minLeavingDate = userCheckInDate ? getNextDays(userCheckInDate, 1) : minDate;
	var dateFormat = 'dd/mm/yy';
	var daysToAdd = 7;
	
	function getlength(number) {
		return number.toString().length;
	}
	
	function getNextDays(selectedDate, daysForward){

		var nextWeekDate = strToDate(selectedDate);
		nextWeekDate.setDate(nextWeekDate.getDate() + daysForward);
		
		var day = nextWeekDate.getDate();
		if(getlength(day) == 1) day = '0' + day;
		
		var month = nextWeekDate.getMonth() + 1;
		if(getlength(month) == 1) month = '0' + month;
		
		var year = nextWeekDate.getFullYear();
		
		return day + '/' + month + '/' + year;
	}
	
	function setLeavingDay(selectedDate){
		var nextWeekDateStr;
		var isFreeWeek = true;
		var tomorrow;
		for(var i=1;i<daysToAdd+1;i++){
			tomorrow = getNextDays(selectedDate, i);
			if(disabledDates.indexOf(tomorrow) != -1){
				isFreeWeek = false;
				nextWeekDateStr = selectedDate;
				break;
			}
			
		}
		
		if(isFreeWeek){
			nextWeekDateStr = tomorrow;
		}
		
		datePickerTill.datepicker("setDate", nextWeekDateStr);
	}
	
	datePickerFrom.datepicker({
		minDate: minDate,
		dateFormat: dateFormat,
		beforeShowDay: function(date){
			var string = jQuery.datepicker.formatDate(dateFormat, date);
			return [ disabledDates.indexOf(string) == -1 ];
		},
		onSelect : function(selectedDate){ 
			// setLeavingDay(selectedDate); 
			
			var tomorrow = getNextDays(selectedDate, 1);
			datePickerTill.datepicker('option', 'minDate', tomorrow).val('');
		}

	});
	
	datePickerTill.datepicker({
		minDate: minLeavingDate,
		dateFormat: dateFormat,
		beforeShowDay: function(date){
			var string = jQuery.datepicker.formatDate(dateFormat, date);
			return [ disabledDates.indexOf(string) == -1 ];
		}
	});
	
	
	// End of - List Page
	
	// Homepage
	
	if(page == 'homepage'){
		$('#homepage .carousel-inner').slick();

		var arrows = $('#homepage .arrow');
		var prevButton = $('#homepage .slick-prev');
		var nextButton = $('#homepage .slick-next');
		var carouselInterval = setInterval(function(){ nextButton.click(); }, 6000);

		arrows.click(function(){
			clearInterval(carouselInterval);
			carouselInterval = setInterval(function(){ nextButton.click(); }, 6000);

			if($(this).hasClass('arrow-left')){
				prevButton.click();
			}else{
				nextButton.click();
			}
		}); 
	}
	
	// End of - Homepage
	
	
	// Item Page
	
	// sb-slider script (cube).
/* 	var Page = (function() {

		var $navArrows = $( '#nav-arrows' ).hide(),
			$shadow = $( '#shadow' ).hide(),
			slicebox = $( '#sb-slider' ).slicebox( {
				onReady : function() {

					$navArrows.show();
					$shadow.show();

				},
				// perspective : 800,
				orientation : 'v',
				cuboidsRandom : false,
				cuboidsCount : 1,
				colorHiddenSides : 'transparent', 
				speed : 1100,
			} ),
			
			init = function() {

				initEvents();
				
			},
			initEvents = function() {

				// add navigation events
				$navArrows.children( ':first' ).on( 'click', function() {

					slicebox.next();
					return false;

				} );

				$navArrows.children( ':last' ).on( 'click', function() {
					
					slicebox.previous();
					return false;

				} );

			};

			return { init : init };

	})();

	Page.init(); */
	 
	 
/* 	function getImgSize(imgSrc) {
		var newImg = new Image();

		newImg.onload = function() {
		  setTimeout(function(){ placeholder.hide(); }, 600);
		};
 
		newImg.src = imgSrc;
	}
	
	var placeholder = $(".item-page .placeholder");	
	var firstImage = $("#sb-slider li img").eq("0");
	getImgSize(firstImage.attr("src")); */
	
	function checkImgSize(imgSrc) {
		var newImg = new Image();

		newImg.onload = function() {
			imageDimensions = newImg.height/newImg.width;
			imageWidth = newImg.width;
			setSliderHeight(imageWidth);
		  
			// detailsElemTop = detailsElem.offset().top -40;
			$(window).resize();
			$(window).scroll();
			setTimeout(function(){ 
				$(window).resize(); 
				$(window).scroll();
			}, 400); 
		};
 
		newImg.src = imgSrc;
	}
	
	function setSliderHeight(imageWidth){
		var imageHeight = Math.round(imageWidth*imageDimensions);
		$("#flipbook").height(imageHeight);
		$("#flipbook .slide").height(imageHeight);
		$("#flipbook .slide .img").height(imageHeight);
	}
	
	if(page == 'item'){
		// Flipbook
		var detailsElem = $('#details');
		var detailsElemTop;
		var imageHeight;
		var imageDimensions;

		checkImgSize($("#flipbook img").eq(0).attr("src"));
		
		$(window).resize(function(){
			var windowsWidth = $(window).width();
			setSliderHeight(windowsWidth);
			
			// Update details element top position value.
			detailsElemTop = detailsElem.offset().top;
		});
		
		$('#flipbook').pageFlip({}); 
		
		var flipbookPrevBtn = $("#flipbook .prev");
		var flipbookNextBtn = $("#flipbook .next");
		$("#flipbook .arrow").click(function(){
			var arrow = $(this).hasClass("arrow-right") ? flipbookNextBtn : flipbookPrevBtn;
			arrow.click();
		});
	
	
		// Fixed booking form.
		var bookingForm = $('.booking-form');
		$(window).scroll(function() {
			var scrollTop = $(window).scrollTop();
			if((bookingForm.hasClass("fixed") === false) && (scrollTop > detailsElemTop)){ 
				bookingForm.addClass("fixed");
			}else if((bookingForm.hasClass("fixed")) && (scrollTop < detailsElemTop)){ 
				bookingForm.removeClass("fixed");
			}
		});
		$(window).scroll();
		
		var googleMap = $("#googleMap");
		if(googleMap.length > 0){
			$(".map-icon").click(function() {
				$('html, body').animate({
					scrollTop: googleMap.offset().top -40
				}, 1000);
			});
		}
		
		// If request booking mode is enabled, show the request booking popup.
		if(requestBooking === '1'){
			$('.request-booking-btn').click(function(){
				submitRequestBookingForm();
			});
			
			$.merge(overlay, $('.item-page .close-btn')).click(function(){
				toggleRequestBookingPopup(false);
			});
		}
	}

	function validateRequestBookingForm(inputsObj)
	{
		if(!inputsObj.first_name){
			return "First name is a required field.";
		}
		
		if(!inputsObj.last_name){
			return "Last name is a required field.";
		}
		
		if(!inputsObj.email){
			return "Email address is a required field.";
		}
		
		// Email validation.
		if(!validateEmail(inputsObj.email)){
			return "Please insert a valid email address.";
		}
		
		return true;
	}
	
	function submitRequestBookingForm()
	{
		var requestBookingForm = $('#booking-request-form');
		var firstName 	= requestBookingForm.find(".first-name input").val();
		var lastName 	= requestBookingForm.find(".last-name input").val();
		var email		= requestBookingForm.find(".email input").val();
		var requests	= requestBookingForm.find(".requests textarea").val();
		
		var inputsObj = {'first_name' : firstName, 'last_name' : lastName, 'email' : email};
		var response  = validateRequestBookingForm(inputsObj);
		var errorElem = requestBookingForm.find('.error');
		errorElem.html("");
		
		// If the form isn't validate, throw an error message.
		if(response !== true){
			errorElem.html(response);
			return;
		}
		
		var inputsHTML = '';	
		for(var input in inputsObj)
		{
			inputsHTML += '<input type="hidden" name="' + input + '" value="' + inputsObj[input] + '" />';
		}
		inputsHTML += '<textarea name="requests" class="hidden">' + requests + '</textarea>';
		
		// Append the request booking fields to the booking form.
		bookingForm.append(inputsHTML);
		
		// Update the booking form action.
		bookingForm.attr('action', requestBookingAction);
		
		// Submit the booking form.
		bookingForm.submit();
	}
	// End of - Item Page 
	
	
	// Checkout page
	
	function validateCheckoutForm(){
		if(!firstName.val()){
			return "First name is a required field.";
		}
		
		if(!lastName.val()){
			return "Last name is a required field.";
		}
		
		if(!email.val()){
			return "Email address is a required field.";
		}
		
		// Email validation.
		if(!validateEmail(email.val())){
			return "Please insert a valid email address.";
		}
		
		return true;
	}
	
	if(page == 'checkout'){
		var firstName = $("#checkout-form .first-name input");
		var lastName = $("#checkout-form .last-name input");
		var email = $("#checkout-form .email input");

		$('.book-btn').click(function(){
			if(errorElem) errorElem.html("");
			var response = validateCheckoutForm();
			
			if(response === true){
				$("#checkout-form").submit();
			}else{
				errorElem.html(response);
			}	
		});
	}
	
	// End of - Checkout page
	
	
	// Contact page
	
	function validateContactForm(){
		if(!name.val()){
			return "Name is a required field.";
		}
		
		if(!subject.val()){
			return "Subject is a required field.";
		}
		
		if(!contactEmail.val()){
			return "Email address is a required field.";
		}
		
		// Email validation.
		if(!validateEmail(contactEmail.val())){
			return "Please insert a valid email address.";
		}
		
		if(!message.val()){
			return "Message is a required field.";
		}
		return true;
	}
	
	if(page == 'contact'){
		var name  		 = $("#contact-form .name input");
		var contactEmail = $("#contact-form .email input");
		var subject 	 = $("#contact-form .subject input");
		var message 	 = $("#contact-form .message textarea");
		
		$('.send-btn').click(function(){
			if(errorElem) errorElem.html("");
			var response = validateContactForm();
			
			if(response === true){
				$("#contact-form").submit();
			}else{
				errorElem.html(response);
			}	
		});
	}
	
	// End of - Contact page
});