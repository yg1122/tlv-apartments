// General
function showLoader(){
	
	var loaders = [
		'spinner',
		'sk-cube-grid',
		'square',
		'cubes',
		'sk-cube-grid',
		'bubble',
		'spinner',
		'sk-folding-cube',
		'sk-folding-cube',
	];
	
	var loadersNumber = loaders.length;
	
	var max = loadersNumber - 1;
	var min = 0;
	var randomNumber = Math.floor(Math.random() * (max - min + 1)) + min;

	$("#overlay").show();
	$(".loader." + loaders[randomNumber]).show();
}

function setTableCellsHeight()
{
	$('#admin-content .table .tr').each(function(){
		var maxHeight = 0;
		$(this).children('.td').each(function(){
			if($(this).outerHeight() > maxHeight){
				maxHeight = $(this).outerHeight();
			}
		});
		$(this).children('.td').css("height", maxHeight);
	});
}

// Orders - edit page
function isEmpty(fieldsArr)
{
	var fieldsArrLen = fieldsArr.length;
	for(var i=0; i<fieldsArrLen; i++){
		if(!fieldsArr[i]['input'].val()){
			return fieldsArr[i]['error'];
		}
	}
	return false;
}

function validateEmail(email) {
	var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	return re.test(email);
}

function isPositiveInteger(str) {
	var n = ~~Number(str);
	return String(n) === str && n > 0;
}

function checkIfDatesDisabled(checkInDate, checkOutDate, disabledDates)
{
	// Check if one of the values is disabled.
	if(disabledDates.indexOf(checkInDate) != -1){
		return "The check in date is not available.";
	}
	if(disabledDates.indexOf(checkOutDate) != -1){
		return "The check out date is not available.";		
	}
	
	// Check if one of the dates between the two values is disabled.
	var tomorrow = false;
	var daysAfter = 1;
	var disabledDaysToInform = [];
	var isFreeWeek = true;
	
	while(checkOutDate != tomorrow){
		tomorrow = getNextDays(checkInDate, daysAfter);
		if(disabledDates.indexOf(tomorrow) != -1){
			isFreeWeek = false;
			disabledDaysToInform.push(tomorrow);
		}
		
		daysAfter++;
	}
	
	// If one of the days in the range is disabled, throw an error.
	if(!isFreeWeek){
		var disabledDaysToInformText = '';
		for(var i=0;i<disabledDaysToInform.length;i++){
			disabledDaysToInformText += disabledDaysToInform[i] + ", ";
		}
		disabledDaysToInformText = disabledDaysToInformText.substring(0, disabledDaysToInformText.length - 2);
		
		return "The following chosen dates are not available: " + disabledDaysToInformText;		
	}
	
	return false;
}

function getlength(number) {
	return number.toString().length;
}

function strToDate(str){

	var dateStrArr = str.split("/");
	var newDateStr = dateStrArr[1] + '/' + dateStrArr[0] + '/' + dateStrArr[2];
	
	return new Date(newDateStr);
}

function getNextDays(selectedDate, daysForward){

	var nextWeekDate = strToDate(selectedDate);
	nextWeekDate.setDate(nextWeekDate.getDate() + daysForward);
	
	var day = nextWeekDate.getDate();
	if(getlength(day) == 1) day = '0' + day;
	
	var month = nextWeekDate.getMonth() + 1;
	if(getlength(month) == 1) month = '0' + month;
	
	var year = nextWeekDate.getFullYear();
	
	return day + '/' + month + '/' + year;
}

$orderObj = {};
$orderObj.validateForm = function(errorsArr, formData){
	
	var error = isEmpty(errorsArr);
	
	if(error !== false){
		return error;
	}

	if(!validateEmail(formData.email.val())){
		return 'Please insert a valid email address.';
	}
	
	if(!isPositiveInteger(formData.guestsNumber.val())){
		return 'Guests number should be a positive integer.';
	}
	
	error = checkIfDatesDisabled(formData.checkin.val(), formData.checkout.val(), formData.disabledDates);
	if(error !== false){
		return error;
	}
	
	return true;
};

// End of - Orders edit page

// Events edit page

$eventsObj = {};
$eventsObj.validateForm = function(errorsArr, formData){
	
	var error = isEmpty(errorsArr);
	
	if(error !== false){
		return error;
	}
	
	error = checkIfDatesDisabled(formData.checkin.val(), formData.checkout.val(), formData.disabledDates);
	if(error !== false){
		return error;
	}
	
	return true;
};

// End of - Events edit page

$(function(){
	var baseUrl = window.location.origin;
	
	$('footer .go-top').click(function() {
	  $("html, body").animate({ scrollTop: 0 }, "slow");
	  return false;
	});
	
	setTableCellsHeight();
	$(window).resize(function(){
		setTableCellsHeight();
	});	
	
	// CKeditor init
	if($('textarea#content').length > 0){
		CKEDITOR.replace( 'content', {
			filebrowserBrowseUrl: '/admin/cms/editor/browse',
			filebrowserUploadUrl: '/admin/cms/editor/upload',
			filebrowserWindowWidth: '300', 
			filebrowserWindowHeight: '400'
		});
	}
	
	$('.nav.notify-row li.dropdown > .dropdown-toggle').click(function(){
		$(this).children('.badge.bg-theme').hide();
		
		var notifications = [];
		var newNotifications = $(this).parent().find('.notification.new');
		newNotifications.each(function(){
			notifications.push($(this).attr("data-id"));
		});

		// If there are no new notifications, return.
		if(!notifications) return;
		
		var table = $(this).attr("data-table");
		$.post(baseUrl + "/admin/admin/main/updateNotifications", {'table':table, 'notifications':notifications}, function(data){
			console.log(data);
		});

		
 		setTimeout(function(){ 
			newNotifications.stop().animate({backgroundColor:'white'}, 800);
				   
		}, 2000);
	});
});