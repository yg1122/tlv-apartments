<?php
return array(
    // set your paypal credential
    'client_id' => 'AcF9_nIzhuvcEdhzieu68SPcB8BqKCjvXZUjWOOkRs2cKFE3pgd3rc58elBa9yzF_7m823_f9jSkf8XQ',
    'secret' 	=> 'ENVK_0OtebqT4l_nlamW9hdBfrul1S47Utu4ca5SFIvQlpgQGEBv4cy-94oZRTPg3I1ZQU0RHw1AEePd',

    /**
     * SDK configuration 
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'live',

        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,

        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,

        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);