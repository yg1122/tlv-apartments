<?php

namespace App\Providers;

use \RecursiveDirectoryIterator;
use \RecursiveIteratorIterator;
use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		
    }

    /**
     * Register any application services.
     *
     * @return void
     */
	public function register()
	{	
		$appPath = app_path();
		$priorClasses = [
			// GuzzleHttp
			$appPath . '/Helpers/GuzzleHttp/Post/PostFileInterface.php',
			$appPath . '/Helpers/GuzzleHttp/Stream/StreamInterface.php',
			$appPath . '/Helpers/GuzzleHttp/Stream/StreamDecoratorTrait.php',
			$appPath . '/Helpers/GuzzleHttp/Exception/TransferException.php',
			$appPath . '/Helpers/GuzzleHttp/Exception/RequestException.php',
			$appPath . '/Helpers/GuzzleHttp/Exception/BadResponseException.php',
			$appPath . '/Helpers/GuzzleHttp/Stream/MetadataStreamInterface.php',
			$appPath . '/Helpers/GuzzleHttp/Cookie/CookieJarInterface.php',
			$appPath . '/Helpers/GuzzleHttp/ToArrayInterface.php',
			$appPath . '/Helpers/GuzzleHttp/Cookie/CookieJar.php',
			$appPath . '/Helpers/GuzzleHttp/Event/SubscriberInterface.php',
			$appPath . '/Helpers/GuzzleHttp/Event/HasEmitterInterface.php',
			$appPath . '/Helpers/GuzzleHttp/Message/MessageInterface.php',
			$appPath . '/Helpers/GuzzleHttp/Message/AbstractMessage.php',
			$appPath . '/Helpers/GuzzleHttp/Event/HasEmitterTrait.php',
			$appPath . '/Helpers/GuzzleHttp/Event/ListenerAttacherTrait.php',
			$appPath . '/Helpers/GuzzleHttp/ClientInterface.php',
			$appPath . '/Helpers/GuzzleHttp/Adapter/AdapterInterface.php',
			$appPath . '/Helpers/GuzzleHttp/Adapter/ParallelAdapterInterface.php',
			$appPath . '/Helpers/GuzzleHttp/Adapter/TransactionInterface.php',
			$appPath . '/Helpers/GuzzleHttp/HasDataTrait.php',
			$appPath . '/Helpers/GuzzleHttp/Event/EventInterface.php',
			$appPath . '/Helpers/GuzzleHttp/Event/AbstractEvent.php',
			$appPath . '/Helpers/GuzzleHttp/Event/AbstractRequestEvent.php',
			$appPath . '/Helpers/GuzzleHttp/Event/AbstractTransferEvent.php',
			$appPath . '/Helpers/GuzzleHttp/Event/EmitterInterface.php',
			$appPath . '/Helpers/GuzzleHttp/Message/MessageFactoryInterface.php',
			$appPath . '/Helpers/GuzzleHttp/Message/RequestInterface.php',
			$appPath . '/Helpers/GuzzleHttp/Message/ResponseInterface.php',
			$appPath . '/Helpers/GuzzleHttp/Post/PostBodyInterface.php',
		];
		
		foreach($priorClasses as $class){
			require_once($class);
		}
	
		$helpersDir = $appPath . '/Helpers';
		$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($helpersDir), RecursiveIteratorIterator::SELF_FIRST);
		foreach($objects as $name => $object)
		{
			$fileParts = pathinfo($name);
			if((isset($fileParts['extension'])) && ($fileParts['extension'] == 'php') && (in_array($name, $priorClasses) == false)){
				require_once($name);
			}
		}
	}
	
}
