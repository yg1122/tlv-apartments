<?php

namespace App\Providers;

use DB;
use View;
use Helper;
use Session;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		// 404 view.
		View::composer('errors/404', function($view){
			$page = DB::table('page')
						->where('slug', '404')
						->first();
			
			$view->with('page', $page);
		});
		
		// Admin view
		View::composer('admin/admin', function($view){
		
			// Get notifications.
			$notifications = $this->getNotifications();
			$view->with('notifications', $notifications);
		});
		
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
	
	private function getNotifications()
	{

		$select = ['request.id','request.first_name','request.last_name','request.checkin',
					'request.checkout','request.date','request.notify','item.name','item.list_image'];
		
		$requests = DB::table('request')
						->join('item', 'item.id', '=', 'request.item_id')
						->select($select)
						->orderBy('date', 'desc')
						->take(5)
						->get();
							
		$contacts = DB::table('contact')
						->select('id','name', 'subject', 'date', 'notify')
						->orderBy('date', 'desc')
						->take(5)
						->get();
		
		
		$select = ['order.id','order.checkin','order.checkout','order.date','order.notify','item.name', 'item.list_image'];			  
		$orders	  = DB::table('order')
						->join('item', 'item.id', '=', 'order.item_id')
						->select($select)
						->orderBy('date', 'desc')
						->take(5)
						->get();
		
		$notificationsArr = [$requests, $contacts, $orders];
		$newNotificationsArr = [];
		foreach($notificationsArr as $notificationValues)
		{
			$new = 0;
			$newNotifications = [];
			foreach($notificationValues as $notification)
			{		
				if(!$notification->notify) continue;
				
				// Calculate elapsed time.
				$notification->time = Helper::calculateElapsedTime($notification->date);
				
				// Unserialize list_image property if exists.
				if(isset($notification->list_image))
				{
					$notification->list_image = unserialize($notification->list_image);
				}
				
				// Add notification.
				$newNotifications[] = $notification;
				$new++;
			}
			$newNotificationsArr[] = ['number' => $new, 'notifications' => $newNotifications];
		}
		
		return [
			'requests' 	=> [
				'values' => $newNotificationsArr[0]['notifications'],
				'new' 	 => $newNotificationsArr[0]['number'],
			],
			'contacts' 	=> [
				'values' => $newNotificationsArr[1]['notifications'],
				'new' 	 => $newNotificationsArr[1]['number'],
			],
			'orders'	=> [
				'values' => $newNotificationsArr[2]['notifications'],
				'new' 	 => $newNotificationsArr[2]['number'],
			],
		];
	}	
}
