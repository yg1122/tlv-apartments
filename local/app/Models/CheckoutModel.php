<?php

namespace App\Models;

use DB;
use Helper;
use Session;
use Illuminate\Database\Eloquent\Model;

class CheckoutModel extends Model
{
	
	public function createOrder($transactionDetails)
	{
		// Create the local event.
		$reservation = Session::get('reservation');

		$checkInDateFromUser = Helper::dateToTimestamp($reservation['checkin']);
		$checkOutDateFromUser = Helper::dateToTimestamp($reservation['checkout']);

		$event = [
			'item_id' 			 => $reservation['item_id'],
			'checkin_timestamp'  => $checkInDateFromUser,
			'checkout_timestamp' => $checkOutDateFromUser,
		];
		
		$order = [
			'first_name'		=> $reservation['customer_details']['first_name'],
			'last_name'			=> $reservation['customer_details']['last_name'],
			'phone'				=> $reservation['customer_details']['phone'],
			'email'				=> $reservation['customer_details']['email'],
			'requests'			=> $reservation['customer_details']['requests'],
			'checkin'			=> $reservation['checkin'],
			'checkout'			=> $reservation['checkout'],
			'guests_number' 	=> $reservation['guests_number'],
			'item_id'			=> $reservation['item_id'],
			'price_per_night'	=> $reservation['price_per_night'],
			'total_price'		=> $reservation['total_price'],
			'subtotal'			=> $reservation['subtotal'],
			'payment_id'		=> $transactionDetails['payment_id'],
			'payer_id'			=> $transactionDetails['payer_id'],
			'token'				=> $transactionDetails['token'],
			'notify'			=> 1,
			'date'				=> date('Y-m-d H:i:s'),
		];
		
		
		$data = ['event' => $event, 'order' => $order, 'reservation' => $reservation];
		
		// Bundle all the DB operation in one transaction.
		DB::transaction(function () use ($data)
		{
			// Insert to events tables.
			$tablesToInsert = ['local_events', 'future_events', 'events'];
			foreach($tablesToInsert as $table)
			{
				DB::table($table)->insert($data['event']);	
			}
			
			// Create the order.
			$orderId = DB::table('order')->insertGetId($data['order']);
			
			// Add the order id to the reservation session.
			$data['reservation']['order_id'] = $orderId;
			Session::put('reservation', $data['reservation']);
		});
		
	}
	
	public function sendOrderMail()
	{
 		$reservation = Session::get('reservation');

		$tokens = [
			'order_id'			=> $reservation['order_id'],
			'first_name' 		=> $reservation['customer_details']['first_name'],
			'last_name' 		=> $reservation['customer_details']['last_name'],
			'phone'				=> $reservation['customer_details']['phone'],
			'email'				=> $reservation['customer_details']['email'],
			'requests' 			=> $reservation['customer_details']['requests'],
			'checkin' 			=> $reservation['checkin'],
			'checkout' 			=> $reservation['checkout'],
			'nights_number' 	=> $reservation['nights_number'],
			'guests_number' 	=> $reservation['guests_number'],
			'total_price'		=> $reservation['total_price'],
			'subtotal'			=> $reservation['subtotal'],
			'cleaning_fee'		=> $reservation['cleaning_fee'],
			'vat'				=> $reservation['vat'],
			'vat_value'			=> $reservation['vat_value'],
			'price_per_night' 	=> $reservation['price_per_night'],
			'property_name' 	=> $reservation['property_name'],
			'country'			=> $reservation['location']['country'],
			'city'				=> $reservation['location']['city'],
			'street'			=> $reservation['location']['street'],
			'house_number'		=> $reservation['location']['house_number'],
			'apartment_number'	=> $reservation['location']['apartment_number'],
		];
			
		$email 	   = Helper::email('order');
		$emailData = [
			'from_address' 	=> Session::get('configs')['email_address'],
			'from_name'		=> Session::get('configs')['email_name'],
			'to_address'	=> $reservation['customer_details']['email'],
			'subject'		=> $email->subject,
		];
		
		$data = [
			'content' => $email->content,
			'tokens'  => $tokens,
		];
		
		// Send the order email to the user.
		Helper::sendCmsMail($data, $emailData);
		
		
		$orderAdminMail 		 = Helper::email('order-admin');
		$data['content'] 		 = $orderAdminMail->content;
		$emailData['subject'] 	 = $orderAdminMail->subject;
		$emailData['to_address'] = Session::get('configs')['email_address'];
		
		// Send the order email to the user.
		Helper::sendCmsMail($data, $emailData);	
	}
}
