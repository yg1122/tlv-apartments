<?php

namespace App\Models;

use DB;
use Helper;
use Cookie;
use Request;
use \iCalUtilityFunctions;
use Illuminate\Database\Eloquent\Model;

class ListModel extends Model
{
	public $listConf;
	
	public function loadListPage($listConf, $searchParams = false)
	{
		// $this->testIcalCreate();
		// $this->testIcalLibMerge();
		
		$this->listConf = $listConf;
		$params = Request::all();
		$searchParams = isset($params['checkin_date']) ? $params : false;

		// Check if page param exists.
		$pageNumber = isset($params["page"]) ? $params["page"] : 1;
		$itemsPerPage = $listConf['items_per_page'];
		$startFrom = ($pageNumber-1) * $itemsPerPage;

		$page = new \stdClass;
		$page->title = $listConf['list_title'];
		$page->disabledDates = $listConf['list_disabled_dates'];
		$page->listUrlKey = $listConf['list_url_key'];
		$page->pageNumber = $pageNumber;
		
		// Fetch cities array for search field.
		$page->cities = Helper::getCities();
		
		$page->minDate = $this->getMinDate($listConf);
		$page->actionUrl = $this->getActionUrl();

		if($searchParams)
		{
			$userSearchData = [
				'checkin_date'  => $searchParams['checkin_date'],
				'checkout_date' => $searchParams['checkout_date'],
				'guests_number' => $searchParams['guests_number'],
				'location'  	=> isset($searchParams['location']) ? $searchParams['location'] : false,
			];
			
			// Set user search cookie for 16 weeks (about 3.5 months).
			Cookie::queue('user_search_data', $userSearchData, 60*24*7*4*4);
			
			$checkInDate = str_replace('/', '-', $searchParams['checkin_date']);
			$checkOutDate = str_replace('/', '-', $searchParams['checkout_date']);
			
			$bookedApartments = $this->getBookedApartments($checkInDate, $checkOutDate);
			$searchDatesString = $this->getSearchDatesString($checkInDate, $checkOutDate);
			$location = isset($searchParams['location']) ? strtolower($searchParams['location']) : false;
			
			$whereConditions = $location ? ['status' => 1, 'city' => $location] : ['status' => 1];
			$items = DB::table('item')
					->select('name','amenities','list_image','url_key','short_description','size','beds','price','cleaning_fee')
                    ->whereNotIn('id', $bookedApartments) 
					->where($whereConditions)
					->where('max_guests', '>=', $searchParams['guests_number'])
					->skip($startFrom)
					->take($itemsPerPage)
					->get();
			
			// Count number of matching rows.
			$itemsNumber = DB::table('item')
						->whereNotIn('id', $bookedApartments) 
						->where($whereConditions)
						->where('max_guests', '>=', $searchParams['guests_number'])
						->count();

		}else{
			$searchDatesString = false;
			
			// Get user search cookie if exists.
			$userSearchData = Request::Cookie('user_search_data');
			$userSearchData = $userSearchData ? $userSearchData : false;
			
			// Fetch all items.
			$items = DB::table('item')
					->select('name','amenities','list_image','url_key','short_description','size','beds','price','cleaning_fee')
					->where('status', 1)
					->skip($startFrom)
					->take($itemsPerPage)
					->get();
			
			// Count number of matching rows.
			$countResult = DB::select( DB::raw("SELECT COUNT(*) FROM item WHERE status=1") );
			$itemsNumber = reset($countResult[0]);
		}
		
		$page->availablePages = ceil($itemsNumber / $itemsPerPage);
		$page->userSearchData = $userSearchData;
		$page->searchDatesString = $searchDatesString;
		$page->items = $this->prepareItems($items);

		return $page;
	}
	
	private function prepareItems($items)
	{
		$listAmenitiesArr = [
			'Air Conditioning' => [
				'name' 			 => "A/C",
				'background_pos' => 'top 0px left 0px',
			],
			'WiFi' => [
				'name' 			 => 'WiFi',
				'background_pos' => 'top 0px left -41px',
			],
			'Telephone' => [
				'name' 			 => "Telephone",
				'background_pos' => 'top 0px left -82px',
			],
			'TV' => [
				'name' => "TV",
				'background_pos' => 'top 0px left -123px',
			],
			'Kitchen' => [
				'name' => "Kitchen",
				'background_pos' => 'top 0px left -205px',
			],
		];
		
		foreach($items as $item)
		{
			// Arrange item amenities.
			$amenities = unserialize($item->amenities);
			$listAmenities = [];
			
			foreach($listAmenitiesArr as $amenity => $values)
			{
				if(isset($amenities[$amenity]) && $amenities[$amenity]){
					$listAmenities[] = $values;
				}
			}
			
			$item->listAmenities = $listAmenities;
			
			// Unserialize item's list image.
			$item->list_image = unserialize($item->list_image);
		}
		
		return $items;
	}
	
	private function getActionUrl()
	{
		$protocol = (Helper::isSecure()) ? 'https' : 'http';
		$actualLink = $protocol . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$parsedUrl = parse_url($actualLink);
		$newUrl = Request::url() . '?';
		
		if(!isset($parsedUrl['query'])){
			return $newUrl;
		}
		
		parse_str($parsedUrl['query'], $queryArr);

		if(isset($queryArr['page']))   unset($queryArr['page']);
		if(isset($queryArr['_token'])) unset($queryArr['_token']);
		
		$queryString = http_build_query($queryArr);
		if($queryString){
			$newUrl = $newUrl . $queryString . '&';
		}
		
		return $newUrl;
	}
	
	public function getMinDate($listConf)
	{

		if($listConf["list_min_date_hours"]){
			return date("d/m/Y", strtotime('+' . $listConf["list_min_date_hours"] . ' hours'));
		}else{
			
			$currentDate = date("d/m/Y");
			$currentDateArr = explode("/", $currentDate);
			$day = $currentDateArr[0];
			$month = $currentDateArr[1];
			$year = $currentDateArr[2];
			
			$maxCheckInHour = explode(':', $listConf["max_checkin_hour"]);
			$hour = $maxCheckInHour[0];
			$minute = $maxCheckInHour[1];
			
			//The variables should be arranged according to the date format and so the separators.
			$checkInTimestamp = mktime($hour, $minute, 0, $month, $day, $year);

			// If check in time has passed, block check in at the current day.
			if($checkInTimestamp < time()){
				return date("d/m/Y", strtotime('tomorrow'));
			}else{
				return date("d/m/Y");
			}
		}
	}
	
	private function getSearchDatesString($checkInDate, $checkOutDate){
		
		$checkInDateFormat = explode(':', date_format(new \DateTime($checkInDate), 'M:j:Y'));
		$checkOutDateFormat = explode(':', date_format(new \DateTime($checkOutDate), 'M:j:Y'));
		
		if(($checkInDateFormat[0] == $checkOutDateFormat[0]) && ($checkInDateFormat[2] == $checkOutDateFormat[2])){
			// Same month & year.
			$searchDatesString = $checkInDateFormat[0] . ' ' . $checkInDateFormat[1] . 
			'-' . $checkOutDateFormat[1] . ', ' . $checkInDateFormat[2];
		}else{
			// Different month/year.
			$searchDatesString = $checkInDateFormat[0] . ' ' . $checkInDateFormat[1] . ', ' . $checkInDateFormat[2] .
			' - ' . $checkOutDateFormat[0] . ' ' . $checkOutDateFormat[1] . ', ' . $checkOutDateFormat[2];
		}
		
		$checkInTimeStamp = strtotime($checkInDate);
		$checkOutTimeStamp = strtotime($checkOutDate);
		
		
		// Calculate number of staying nights.
		$datediff = $checkOutTimeStamp - $checkInTimeStamp;
		$nightsNumber = floor($datediff/(60*60*24));
		
		return $searchDatesString . ' | ' . $nightsNumber . ' night(s)';
		
	}
	
	public function getBookedApartments($checkInDate, $checkOutDate)
	{
		// Should be a timestamp. Pay attention for the date format, should be: dd-mm-Y  (full year value).
		$checkInDateFromUser = strtotime($checkInDate);
		$checkOutDateFromUser = strtotime($checkOutDate);
		
		// Fetch only events with checkout date which is older than the wanted check in date (user check in date).
		$events = DB::table('future_events')
				->where('checkout_timestamp', '>', $checkInDateFromUser)
				->get();
		
		// Arrange the events array.
		$apartmentsEvents = [];
		foreach($events as $event)
		{
			$apartmentsEvents[$event->item_id][] = [
					'checkin_timestamp' => $event->checkin_timestamp,
					'checkout_timestamp' => $event->checkout_timestamp,
				];
		}
		
		$bookedApartments = [];
 		foreach($apartmentsEvents as $apartmentId => $events)
		{	
			$isBooked = false;
			foreach($events as $event)
			{
				// If booked at the wanted dates, break the loop & continue to the next apartment.
				$isBooked = $this->checkIfApartmentIsBooked($checkInDateFromUser, $checkOutDateFromUser, $event['checkin_timestamp'], $event['checkout_timestamp']);
				if($isBooked){
					$bookedApartments[] = $apartmentId;
					break;
				}
			}
		}
		return $bookedApartments;
	}

	public function checkIfApartmentIsBooked($checkInDateFromUser, $checkOutDateFromUser, $eventCheckInStamp, $eventCheckOutStamp)
	{
		
		$checkList = [
			[
				'startDate' 	=> $eventCheckInStamp,
				'endDate' 		=> $eventCheckOutStamp,
				'dateFromUser'	=> $checkInDateFromUser,
			],
			[
				'startDate' 	=> $eventCheckInStamp,
				'endDate' 		=> $eventCheckOutStamp,
				'dateFromUser'	=> $checkOutDateFromUser,
			],
			
			// The third scenario is for cases that the wanted check in date is prior
			// to the event's check in date & the wanted check out date is after the event's check out date.
			[
				'startDate' 	=> $checkInDateFromUser,
				'endDate' 		=> $checkOutDateFromUser,
				'dateFromUser'	=> $eventCheckInStamp,
			],
			
		];
		
		foreach($checkList as $scenario){
			$isBooked = $this->checkInRange($scenario['startDate'], $scenario['endDate'], $scenario['dateFromUser'], true);
			if($isBooked) break;
		}
		
		// Another check for scenarios which the check in date & check out date are equal to
		// the events check in & check out values.
		if(!$isBooked){
			if(($checkInDateFromUser == $eventCheckInStamp) && ($checkOutDateFromUser == $eventCheckOutStamp)){
				$isBooked = true;
			}
		}
		
		return $isBooked;
	}
	
	private function checkInRange($startDate, $endDate, $dateFromUser, $isTimestamp = false)
	{
		// Convert to timestamp.
		if(!$isTimestamp){
			$startDate = strtotime($startDate);
			$endDate = strtotime($endDate);
			$dateFromUser = strtotime($dateFromUser);
		}
		
		// Check if the user date is between the start & end dates.
		return (($dateFromUser > $startDate) && ($dateFromUser < $endDate));
	}
	
	public function testIcalLibMerge(){
/* 			'7807461' 	=> 'https://www.airbnb.fr/calendar/ical/7807461.ics?s=14f93962bcf8ede18e1224c88028cc40',
			'69886' 	=> 'https://www.airbnb.fr/calendar/ical/69886.ics?s=0a7b8021796995cadd8b4b1ddd712e11',
			'3001663'	=> 'https://www.airbnb.fr/calendar/ical/3001663.ics?s=3e84c5fb929bd3c6135ed82264fe7cf4',
			'191205'	=> 'https://www.airbnb.com/calendar/ical/191205.ics?s=b7244d9149fa3422dd5e00ebccd51abf', */
		
		$icsDir = base_path() . '/app/Libraries/ics-parser-master/ics/';
		$icalDir = base_path() . '/app/Libraries/iCalcreator-2.22.1/';
		require_once( $icalDir . 'iCalcreator.php' );

		$filename1 = 'airbnb_69886.ics';
		$filename2 = 'airbnb_191205.ics';
		$filename3 = 'airbnb_7807461.ics';
		$filename4 = 'airbnb_3001663.ics';
		
		$config = array( "unique_id" => "kigkonsult.se" );
		$v = new \vcalendar( $config );
  
		// First file
 		$config = array( "directory" => $icsDir, "filename" => $filename1 );
		$v->setConfig( $config );
		$v->parse();

		// Second file.
		$config = array( "directory" => $icsDir, "filename" => $filename2 );
		$v->setConfig( $config );
		$v->parse();
		
		// Third file.
		$config = array( "directory" => $icsDir, "filename" => $filename3 );
		$v->setConfig( $config );
		$v->parse();
		
		// Fourth file.
		$config = array( "directory" => $icsDir, "filename" => $filename4 );
		$v->setConfig( $config );
		$v->parse();
		
		// Create one merged file.
 		$v->setConfig("directory", $icalDir . "export");
		$v->setConfig("filename", "icalmerged.ics");
		$v->saveCalendar();


		echo "done";
		die();

	}
	
	public function testIcalCreate(){
		/* 			'7807461' 	=> 'https://www.airbnb.fr/calendar/ical/7807461.ics?s=14f93962bcf8ede18e1224c88028cc40',
			'69886' 	=> 'https://www.airbnb.fr/calendar/ical/69886.ics?s=0a7b8021796995cadd8b4b1ddd712e11',
			'3001663'	=> 'https://www.airbnb.fr/calendar/ical/3001663.ics?s=3e84c5fb929bd3c6135ed82264fe7cf4',
			'191205'	=> 'https://www.airbnb.com/calendar/ical/191205.ics?s=b7244d9149fa3422dd5e00ebccd51abf', */
		
		$icsDir = base_path() . '/app/Libraries/ics-parser-master/ics/';
		$icalDir = base_path() . '/app/Libraries/iCalcreator-2.22.1/';
		require_once( $icalDir . 'iCalcreator.php' );
		
		// 22/11/2016
		// 28/09/2015
		// 06/06/2016
		// 28/09/2016
		// 22/09/2016
		$checkInDateStampTest = 1474502400;
		$checkInDateTest = date('Ymd\THis', $checkInDateStampTest);
		
		// 25/11/2016
		// 30/09/2015
		// 11/06/2016
		// 30/09/2016
		// 24/09/2016
		$checkOutDateStampTest = 1474675200;
		$checkOutDateTest = date('Ymd\THis', $checkOutDateStampTest);


		$tz     = "Asia/Jerusalem"; // define time zone
		$config = array( "unique_id" => "kigkonsult.se" ,"TZID" => $tz );
		  // opt. "calendar" timezone
		$v      = new \vcalendar( $config );
		  // create a new calendar instance

		$v->setProperty( "method", "PUBLISH" );
		  // required of some calendar software
		$v->setProperty( "x-wr-calname", "Calendar Sample" );
		  // required of some calendar software
		$v->setProperty( "X-WR-CALDESC", "Calendar Description" );
		  // required of some calendar software
		$v->setProperty( "X-WR-TIMEZONE", $tz );
		  // required of some calendar software

		$xprops = array( "X-LIC-LOCATION" => $tz );
		  // required of some calendar software
		iCalUtilityFunctions::createTimezone( $v, $tz, $xprops );
		  // create timezone component(-s) opt. 1
		  // based on present date

		  
		// START VEVENT.  
		// Create an event calendar component
		$vevent = & $v->newComponent( "vevent" );
		  

		$vevent->setProperty( "dtstart", $checkInDateTest, array("VALUE" => "DATE"));
		
		$vevent->setProperty( "dtend", $checkOutDateTest, array("VALUE" => "DATE"));
		
		$vevent->setProperty( "LOCATION", "LARGE 2BR DESIGN apart next toBeach" );
		  // property name - case independent
		$vevent->setProperty( "summary", "Anna Goldy (S29TYD)" );
		
		
		$description = "CHECKIN:  27/09/2014\nCHECKOUT: 05/10/2014\nNIGHTS:   8\nPHONE:+7 916 922 01 93\nEMAIL:    (aucun alias d'e-mail disponible)\nPROPERTY: LARGE 2BR DESIGN apart next toBeach";
 
		$vevent->setProperty( "description", $description );
		// END VEVENT
		
		
		
		// One more event:
		$vevent = & $v->newComponent( "vevent" );
		
		
		// 02/09/2016
		$checkInDateStampTest = 1472774400;
		$checkInDateTest = date('Ymd\THis', $checkInDateStampTest);

		// 14/09/2016
		$checkOutDateStampTest = 1473811200;
		$checkOutDateTest = date('Ymd\THis', $checkOutDateStampTest);
		
		
		
		$vevent->setProperty( "dtstart", $checkInDateTest, array("VALUE" => "DATE"));
		
		$vevent->setProperty( "dtend", $checkOutDateTest, array("VALUE" => "DATE"));
		
		$vevent->setProperty( "LOCATION", "LARGE 2BR DESIGN apart next toBeach" );
		  // property name - case independent
		$vevent->setProperty( "summary", "Anna Goldy (S29TYD)" );
		
		
		$description = "CHECKIN:  27/09/2014\nCHECKOUT: 05/10/2014\nNIGHTS:   8\nPHONE:+7 916 922 01 93\nEMAIL:    (aucun alias d'e-mail disponible)\nPROPERTY: LARGE 2BR DESIGN apart next toBeach";
 
		$vevent->setProperty( "description", $description );
		//end
		
		// Create the file.
		$v->setConfig("directory", $icsDir . "local");
		$v->setConfig("filename", "export_22_09_2016.ics");
		$v->saveCalendar();


		echo "done";
		die();

	}
}
