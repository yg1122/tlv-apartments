<?php

namespace App\Models;

use DB;
use Helper;
use Cookie;
use Request;
use Session;
use App\Models\ListModel;
use Illuminate\Database\Eloquent\Model;

class ItemModel extends Model
{
	private $item;
	private $icsTempFile;
	private $listModel;
	private $listConf;
	
	public function loadItem()
	{
		$urlKey = Request::segment(2);
		
		$item = DB::table('item')
				->select($this->getItemSelectFields())
				->where('url_key', $urlKey)
				->where('status', 1)
				->first();
		
		if(!$item) abort(404);
		
		$this->listModel = new ListModel();
		$this->listConf = unserialize(Session::get('configs')['list_config']);
		$this->item = $item;
		$page = new \stdClass;
		$page->item = $item;
		$page->item->images = $this->getItemImages($item);
		$page->item->amenities = unserialize($page->item->amenities);
		$page->item->amenitiesArr = $this->getAmenities();
		$page->item->minDate = $this->listModel->getMinDate($this->listConf);
		$page->item->disabledDates = $this->listConf['list_disabled_dates'];
		
		// Get Google map coordinates.
		$page->item->coordinates = explode(",", $page->item->coordinates);
		
		// Get request booking mode.
		$page->requestBooking = (int)Session::get('configs')['request_booking'];
		
		// Get user search cookie if exists.
		$userSearchData = Request::Cookie('user_search_data');
		$page->userSearchData = $userSearchData ? $userSearchData : false;
		
		return $page;
		
	}
	
	public function getItemImages($item)
	{
		$images = unserialize($item->images);
		
		// Sort the array by the position value.
		if(is_array($images)){
			usort($images, function($a, $b) {
				return $a['position'] - $b['position'];
			});
		}
		
		return $images;
	}
	
	public function isAvailable($checkInDate, $checkOutDate, $performanceMode, $itemId = false, $airbnbIcs = false)
	{
		$listModel = new ListModel();
		
		if($itemId){
			$this->item = new \stdClass();
			$this->item->id = $itemId;
			if($airbnbIcs) $this->item->airbnb_ics = $airbnbIcs;
		}
		
		$checkInDate = str_replace('/', '-', $checkInDate);
		$checkOutDate = str_replace('/', '-', $checkOutDate);
		$checkInDateFromUser = strtotime($checkInDate);
		$checkOutDateFromUser = strtotime($checkOutDate);
		
		if(!$performanceMode){
			$itemEvents = $this->getEventsFromIcal();
			$localEventsObj = DB::table('local_events')
							->where('checkout_timestamp', '>', $checkInDateFromUser)
							->where('item_id', $this->item->id)
							->get();
			
			$localEvents = [];
			foreach($localEventsObj as $localEvent){
				$localEvents[] = [
					'DTSTART' 	=> $localEvent->checkin_timestamp,
					'DTEND' 	=> $localEvent->checkout_timestamp,
					'TIMESTAMP'	=> true,
				];
			}

			// Merge the remote events with the local ones.
			$itemEvents = array_merge($itemEvents, $localEvents);

		}else{
		
			// Fetch only events with checkout date which is older than the wanted check in date (user check in date).
			$events = DB::table('future_events')
				->where('checkout_timestamp', '>', $checkInDateFromUser)
				->where('item_id', $this->item->id)
				->get();
			
			// Arrange the events array.
 			$itemEvents = [];
			foreach($events as $event)
			{
				$itemEvents[] = [
						'DTSTART' => $event->checkin_timestamp,
						'DTEND' => $event->checkout_timestamp,
					];
			}
		}
		
		$isAvailable = true;
		$isBooked = false;
		foreach($itemEvents as $event)
		{
			$eventCheckOutStamp = $this->getEventTimestamp('DTEND', $performanceMode, $event);
			if($eventCheckOutStamp > $checkInDateFromUser){
				$eventCheckInStamp = $this->getEventTimestamp('DTSTART', $performanceMode, $event);
				$isBooked = $listModel->checkIfApartmentIsBooked($checkInDateFromUser, $checkOutDateFromUser, $eventCheckInStamp, $eventCheckOutStamp);
				if($isBooked){
					$isAvailable = false;
					break;
				}
			}
		}
		
		// Remove the temporary file.
		if(!$performanceMode){
			unlink($this->icsTempFile);
		}
		
		return $isAvailable;
	}
	
	private function getEventTimestamp($dateType, $performanceMode, $event)
	{
		if($performanceMode || (isset($event['TIMESTAMP']) && $event['TIMESTAMP'])){
			return $event[$dateType];
		}

		return strtotime($event[$dateType]);
	}
	
	public function getEventsFromIcal()
	{
		$icsParserPath = base_path() . '/app/Libraries/ics-parser-master/class.iCalReader.php';
		require $icsParserPath;
		
		$icsFile = Helper::curlGetContents($this->item->airbnb_ics);
		
		// Generate unique id for the temporary file.
		$icsId = uniqid(rand(), true);
		
		$icsTempFileName = $icsId . '_' . $this->item->id . '.ics';
		$icsTempDir = base_path() . '/app/Libraries/ics-parser-master/ics/tmp/';
		$this->icsTempFile = $icsTempDir . $icsTempFileName;
		
		// Create a temporary file.
		file_put_contents($this->icsTempFile, $icsFile);
		
		
		$ical = new \ical($this->icsTempFile);
		$itemEvents = $ical->events();
		
		return $itemEvents;
	}
	
	public function getAmenities($valuesToKeys = false)
	{
		$amenities = [
			'kitchen' 					=> 'Kitchen',
			'wifi' 						=> 'WiFi',
			'tv' 						=> 'TV',
			'heating' 					=> 'Heating',
			'air_conditioning' 			=> 'Air Conditioning',
			'washer' 					=> 'Washer',
			'dryer' 					=> 'Dryer',
			'free_parking'				=> 'Free Parking',
			'cable_tv' 					=> 'Cable TV',
			'breakfast'					=> 'Breakfast',
			'pets_allowed'				=> 'Pets Allowed',
			'family'					=> 'Family/Kid Friendly',
			'smoking' 					=> 'Smoking Allowed',
			'wheelchair' 				=> 'Wheelchair Accessible',
			'elevator' 					=> 'Elevator in Building',
			'pool' 						=> 'Pool',
			'hot_tub' 					=> 'Hot Tub',
			'gym' 						=> 'Gym',
			'smoke_detector'	   		=> 'Smoke Detector',
			'telephone'					=> 'Telephone',
			'aid_kit' 					=> 'First Aid Kit',
			'fire_extinguisher' 		=> 'Fire Extinguisher',
		];
		
		if($valuesToKeys){
			$amenities = Helper::arraySwitch($amenities);
		}
		
		return $amenities;
		
	}
	
	private function getItemSelectFields()
	{
		return [
			'images',
			'amenities',
			'coordinates',
			'name',
			'city',
			'country',
			'property_type',
			'size',
			'max_guests',
			'bedrooms',
			'beds',
			'price',
			'about',
			'bed_type',
			'bathrooms',
			'cleaning_fee',
			'cancellation',
			'description',
			'rules',
			'id',
		];
	}
}
