<?php

/* use DB;
use URL;
use Helper;
use Session; */

class Helper{

	public static function getAdminSection($section, $content)
	{
		$basePath = base_path();
		$configs = Session::get('configs');
		
		if($content){
			$currentContentPath = $basePath . '/resources/views/admin/content/' . $section . '/' . $content . '.blade.php';
			$contentTitle = ucwords(str_replace('-', ' ', $content));
			$contentTemplate = 'admin.content.' . $section . '.' . $content;
			
			if(!file_exists($currentContentPath)){
				$contentTemplate = 'admin.content.404';
				$contentTitle = 'The content is not available';
			}
		}else{
			$section = 'main';
			$contentTemplate = 'admin.content.main';
			$contentTitle = 'Main Title';
		}
			
		$data = [
			'section' 			=> $section,
			'content' 			=> $content,
			'content_template' 	=> $contentTemplate,
			'content_title' 	=> $contentTitle,
			'seo'				=> unserialize($configs['seo']),
			'website_name'		=> $configs['website_name'],
			'page'				=> Helper::getCurrentPage(),
		];

		return $data;
	}
	
	public static function getCities()
	{
		$data = DB::table('item')->select('city')->distinct()->get();
		
		$citiesArr = [];
		foreach($data as $value){
			$citiesArr[] = $value->city;
		}
		
		return $citiesArr;
	}

	// Controller, Method, arguments array.
	public static function call($controller, $method, $arguments = [])
	{
		if (strpos($controller,'\\') === false) {
			$controller = 'App\Http\Controllers\\' . $controller;
		}
		
		$controller = app()->make($controller);
		app()->call([$controller, $method], $arguments);
	}
	
	public static function getTimeZoneArr($timeZone = false)
	{
		$timeZoneArr = [
		
			'Pacific/Midway' => '(UTC-11:00) Midway Island',
			'Pacific/Samoa' => '(UTC-11:00) Samoa',
			'Pacific/Honolulu' => '(UTC-10:00) Hawaii',
			'US/Alaska' => '(UTC-09:00) Alaska',
			'America/Los_Angeles' => '(UTC-08:00) Pacific Time (US & Canada)',
			'America/Tijuana' => '(UTC-08:00) Tijuana',
			'US/Arizona' => '(UTC-07:00) Arizona',
			'America/Chihuahua' => '(UTC-07:00) Chihuahua',
			'America/Chihuahua' => '(UTC-07:00) La Paz',
			'America/Mazatlan' => '(UTC-07:00) Mazatlan',
			'US/Mountain' => '(UTC-07:00) Mountain Time (US & Canada)',
			'America/Managua' => '(UTC-06:00) Central America',
			'US/Central' => '(UTC-06:00) Central Time (US & Canada)',
			'America/Mexico_City' => '(UTC-06:00) Guadalajara',
			'America/Mexico_City' => '(UTC-06:00) Mexico City',
			'America/Monterrey' => '(UTC-06:00) Monterrey',
			'Canada/Saskatchewan' => '(UTC-06:00) Saskatchewan',
			'America/Bogota' => '(UTC-05:00) Bogota',
			'US/Eastern' => '(UTC-05:00) Eastern Time (US & Canada)',
			'US/East-Indiana' => '(UTC-05:00) Indiana (East)',
			'America/Lima' => '(UTC-05:00) Lima',
			'America/Bogota' => '(UTC-05:00) Quito',
			'Canada/Atlantic' => '(UTC-04:00) Atlantic Time (Canada)',
			'America/Caracas' => '(UTC-04:30) Caracas',
			'America/La_Paz' => '(UTC-04:00) La Paz',
			'America/Santiago' => '(UTC-04:00) Santiago',
			'Canada/Newfoundland' => '(UTC-03:30) Newfoundland',
			'America/Sao_Paulo' => '(UTC-03:00) Brasilia',
			'America/Argentina/Buenos_Aires' => '(UTC-03:00) Buenos Aires',
			'America/Argentina/Buenos_Aires' => '(UTC-03:00) Georgetown',
			'America/Godthab' => '(UTC-03:00) Greenland',
			'America/Noronha' => '(UTC-02:00) Mid-Atlantic',
			'Atlantic/Azores' => '(UTC-01:00) Azores',
			'Atlantic/Cape_Verde' => '(UTC-01:00) Cape Verde Is.',
			'Africa/Casablanca' => '(UTC+00:00) Casablanca',
			'Europe/London' => '(UTC+00:00) Edinburgh',
			'Etc/Greenwich' => '(UTC+00:00) Greenwich Mean Time : Dublin',
			'Europe/Lisbon' => '(UTC+00:00) Lisbon',
			'Europe/London' => '(UTC+00:00) London',
			'Africa/Monrovia' => '(UTC+00:00) Monrovia',
			'UTC' => '(UTC+00:00) UTC',
			'Europe/Amsterdam' => '(UTC+01:00) Amsterdam',
			'Europe/Belgrade' => '(UTC+01:00) Belgrade',
			'Europe/Berlin' => '(UTC+01:00) Berlin',
			'Europe/Berlin' => '(UTC+01:00) Bern',
			'Europe/Bratislava' => '(UTC+01:00) Bratislava',
			'Europe/Brussels' => '(UTC+01:00) Brussels',
			'Europe/Budapest' => '(UTC+01:00) Budapest',
			'Europe/Copenhagen' => '(UTC+01:00) Copenhagen',
			'Europe/Ljubljana' => '(UTC+01:00) Ljubljana',
			'Europe/Madrid' => '(UTC+01:00) Madrid',
			'Europe/Paris' => '(UTC+01:00) Paris',
			'Europe/Prague' => '(UTC+01:00) Prague',
			'Europe/Rome' => '(UTC+01:00) Rome',
			'Europe/Sarajevo' => '(UTC+01:00) Sarajevo',
			'Europe/Skopje' => '(UTC+01:00) Skopje',
			'Europe/Stockholm' => '(UTC+01:00) Stockholm',
			'Europe/Vienna' => '(UTC+01:00) Vienna',
			'Europe/Warsaw' => '(UTC+01:00) Warsaw',
			'Africa/Lagos' => '(UTC+01:00) West Central Africa',
			'Europe/Zagreb' => '(UTC+01:00) Zagreb',
			'Europe/Athens' => '(UTC+02:00) Athens',
			'Europe/Bucharest' => '(UTC+02:00) Bucharest',
			'Africa/Cairo' => '(UTC+02:00) Cairo',
			'Africa/Harare' => '(UTC+02:00) Harare',
			'Europe/Helsinki' => '(UTC+02:00) Helsinki',
			'Europe/Istanbul' => '(UTC+02:00) Istanbul',
			'Asia/Jerusalem' => '(UTC+02:00) Jerusalem',
			'Europe/Helsinki' => '(UTC+02:00) Kyiv',
			'Africa/Johannesburg' => '(UTC+02:00) Pretoria',
			'Europe/Riga' => '(UTC+02:00) Riga',
			'Europe/Sofia' => '(UTC+02:00) Sofia',
			'Europe/Tallinn' => '(UTC+02:00) Tallinn',
			'Europe/Vilnius' => '(UTC+02:00) Vilnius',
			'Asia/Baghdad' => '(UTC+03:00) Baghdad',
			'Asia/Kuwait' => '(UTC+03:00) Kuwait',
			'Europe/Minsk' => '(UTC+03:00) Minsk',
			'Africa/Nairobi' => '(UTC+03:00) Nairobi',
			'Asia/Riyadh' => '(UTC+03:00) Riyadh',
			'Europe/Volgograd' => '(UTC+03:00) Volgograd',
			'Asia/Tehran' => '(UTC+03:30) Tehran',
			'Asia/Muscat' => '(UTC+04:00) Abu Dhabi',
			'Asia/Baku' => '(UTC+04:00) Baku',
			'Europe/Moscow' => '(UTC+04:00) Moscow',
			'Asia/Muscat' => '(UTC+04:00) Muscat',
			'Europe/Moscow' => '(UTC+04:00) St. Petersburg',
			'Asia/Tbilisi' => '(UTC+04:00) Tbilisi',
			'Asia/Yerevan' => '(UTC+04:00) Yerevan',
			'Asia/Kabul' => '(UTC+04:30) Kabul',
			'Asia/Karachi' => '(UTC+05:00) Islamabad',
			'Asia/Karachi' => '(UTC+05:00) Karachi',
			'Asia/Tashkent' => '(UTC+05:00) Tashkent',
			'Asia/Calcutta' => '(UTC+05:30) Chennai',
			'Asia/Kolkata' => '(UTC+05:30) Kolkata',
			'Asia/Calcutta' => '(UTC+05:30) Mumbai',
			'Asia/Calcutta' => '(UTC+05:30) New Delhi',
			'Asia/Calcutta' => '(UTC+05:30) Sri Jayawardenepura',
			'Asia/Katmandu' => '(UTC+05:45) Kathmandu',
			'Asia/Almaty' => '(UTC+06:00) Almaty',
			'Asia/Dhaka' => '(UTC+06:00) Astana',
			'Asia/Dhaka' => '(UTC+06:00) Dhaka',
			'Asia/Yekaterinburg' => '(UTC+06:00) Ekaterinburg',
			'Asia/Rangoon' => '(UTC+06:30) Rangoon',
			'Asia/Bangkok' => '(UTC+07:00) Bangkok',
			'Asia/Bangkok' => '(UTC+07:00) Hanoi',
			'Asia/Jakarta' => '(UTC+07:00) Jakarta',
			'Asia/Novosibirsk' => '(UTC+07:00) Novosibirsk',
			'Asia/Hong_Kong' => '(UTC+08:00) Beijing',
			'Asia/Chongqing' => '(UTC+08:00) Chongqing',
			'Asia/Hong_Kong' => '(UTC+08:00) Hong Kong',
			'Asia/Krasnoyarsk' => '(UTC+08:00) Krasnoyarsk',
			'Asia/Kuala_Lumpur' => '(UTC+08:00) Kuala Lumpur',
			'Australia/Perth' => '(UTC+08:00) Perth',
			'Asia/Singapore' => '(UTC+08:00) Singapore',
			'Asia/Taipei' => '(UTC+08:00) Taipei',
			'Asia/Ulan_Bator' => '(UTC+08:00) Ulaan Bataar',
			'Asia/Urumqi' => '(UTC+08:00) Urumqi',
			'Asia/Irkutsk' => '(UTC+09:00) Irkutsk',
			'Asia/Tokyo' => '(UTC+09:00) Osaka',
			'Asia/Tokyo' => '(UTC+09:00) Sapporo',
			'Asia/Seoul' => '(UTC+09:00) Seoul',
			'Asia/Tokyo' => '(UTC+09:00) Tokyo',
			'Australia/Adelaide' => '(UTC+09:30) Adelaide',
			'Australia/Darwin' => '(UTC+09:30) Darwin',
			'Australia/Brisbane' => '(UTC+10:00) Brisbane',
			'Australia/Canberra' => '(UTC+10:00) Canberra',
			'Pacific/Guam' => '(UTC+10:00) Guam',
			'Australia/Hobart' => '(UTC+10:00) Hobart',
			'Australia/Melbourne' => '(UTC+10:00) Melbourne',
			'Pacific/Port_Moresby' => '(UTC+10:00) Port Moresby',
			'Australia/Sydney' => '(UTC+10:00) Sydney',
			'Asia/Yakutsk' => '(UTC+10:00) Yakutsk',
			'Asia/Vladivostok' => '(UTC+11:00) Vladivostok',
			'Pacific/Auckland' => '(UTC+12:00) Auckland',
			'Pacific/Fiji' => '(UTC+12:00) Fiji',
			'Pacific/Kwajalein' => '(UTC+12:00) International Date Line West',
			'Asia/Kamchatka' => '(UTC+12:00) Kamchatka',
			'Asia/Magadan' => '(UTC+12:00) Magadan',
			'Pacific/Fiji' => '(UTC+12:00) Marshall Is.',
			'Asia/Magadan' => '(UTC+12:00) New Caledonia',
			'Asia/Magadan' => '(UTC+12:00) Solomon Is.',
			'Pacific/Auckland' => '(UTC+12:00) Wellington',
			'Pacific/Tongatapu' => '(UTC+13:00) Nuku\'alofa',
		];
		
		if($timeZone){
			return $timeZoneArr[$timeZone];
		}
		return $timeZoneArr;
	}
	
	public static function curlPost($url, $params)
	{
		// Generate the params string,
		$paramsStr = '';
		foreach($params as $key => $value) 
		{
			$paramsStr .= $key . '=' . urlencode($value) . '&'; 
		}
		rtrim($paramsStr, '&');
		
		// Open connection.
		$ch = curl_init();

		// Set the url, number of POST vars, POST data.
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($params));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $paramsStr);

		// Execute post.
		$result = curl_exec($ch);

		// Close connection.
		curl_close($ch);
		
		return $result;
	}
	
	public static function curlGetContents($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, 'My user agent string!');
		$data = curl_exec($ch);
		curl_close($ch);
		
		return $data;
	}
	
	public static function message($type, $messages)
	{
		$messageArr = [];
 		if(Session::has('message')){
			$messageArr = Session::get('message');
		}

		foreach($messages as $message){
			$messageArr[$type][] = $message;
		}
		
		Session::flash('message', $messageArr);
	}
	
	public static function log($data, $dump = false)
	{
		echo '<pre>';
		if($dump){
			var_dump($data);
		}else{
			print_r($data);	
		}
		echo '</pre>';
	}
	
	public static function arraySwitch($array)
	{
		$valuesToKeysArr = [];
		foreach($array as $key => $value){
			$valuesToKeysArr[$value] = $key;
		}
		
		return $valuesToKeysArr;
	}
	
	public static function block($identifier)
	{
		if(is_string($identifier)){
			$identifier = [$identifier];
		}
		
		$blocks = DB::table('block')
			->select('content', 'identifier')
			->whereIn('identifier', $identifier)
			->where('status', 1)
			->get();
			
		if(!$blocks) return false;
		
		if(count($blocks) == 1){
			return $blocks[0]->content; 
		}else{
			$blocksArr = [];
			foreach($blocks as $block)
			{
				$blocksArr[$block->identifier] = $block->content;
			}
			return $blocksArr;
		}
	}
	
	public static function email($identifier)
	{
		$email = DB::table('email')
			->select('content', 'subject')
			->where('identifier', $identifier)
			->where('status', 1)
			->first();
			
		return $email ? $email : false;
	}
	
	public static function getCoordinates($address)
	{
		// Replace all the white space with "+" sign to match with google search pattern.
		$address = str_replace(" ", "+", $address);
		$url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
		 
		$response = file_get_contents($url);
		$json = json_decode($response,TRUE);
		
		if(empty($json['results'][0])){
			return [
				'lat' => 0,
				'lng' => 0,
			];
		}
		
		return [
			'lat' => $json['results'][0]['geometry']['location']['lat'],
			'lng' => $json['results'][0]['geometry']['location']['lng']
		];
	}
	
	public static function calcNights($checkin, $checkout, $replaceSlashes = true)
	{
		if($replaceSlashes){
			$checkin = str_replace('/', '-', $checkin);
			$checkout = str_replace('/', '-', $checkout);
		}
		
		$checkInTimeStamp = strtotime($checkin);
		$checkOutTimeStamp = strtotime($checkout);
		
		// Calculate number of staying nights.
		$datediff = $checkOutTimeStamp - $checkInTimeStamp;
		return floor($datediff/(60*60*24));
	}
	
	public static function exception($e)
	{
		echo 'Message: ' . $e->getMessage();
		die();
	}
	
	public static function isNumber($i, $negetive = false, $float = false)
	{
		if(!is_numeric($i)){
			return false;
		}
		
		if(($negetive === false) && ($i < 1)){
			return false;
		}
		
		if(($float === false) && ($i != round($i))){
			return false;
		}
		
		return true;
	}
	
	public static function deductTax($price)
	{
		$tax = Helper::getVat();
		$divideBy = '1.' . $tax;
		
		return number_format($price/(float)$divideBy, 2, '.', '');
	}
	
	public static function dateToStr($checkInDate, $checkOutDate)
	{
		$checkInDate = str_replace('/', '-', $checkInDate);
		$checkOutDate = str_replace('/', '-', $checkOutDate);
		$checkInDateFormat = explode(':', date_format(new \DateTime($checkInDate), 'M:j:Y'));
		$checkOutDateFormat = explode(':', date_format(new \DateTime($checkOutDate), 'M:j:Y'));
		
		if(($checkInDateFormat[0] == $checkOutDateFormat[0]) && ($checkInDateFormat[2] == $checkOutDateFormat[2])){
			// Same month & year.
			$datesString = $checkInDateFormat[0] . ' ' . $checkInDateFormat[1] . 
			'-' . $checkOutDateFormat[1] . ', ' . $checkInDateFormat[2];
		}else{
			// Different month/year.
			$datesString = $checkInDateFormat[0] . ' ' . $checkInDateFormat[1] . ', ' . $checkInDateFormat[2] .
			' - ' . $checkOutDateFormat[0] . ' ' . $checkOutDateFormat[1] . ', ' . $checkOutDateFormat[2];
		}
		
		return $datesString;
	}
	
	public static function getVat()
	{
		return (float)Session::get('configs')['vat'];
	}
	
	public static function updateEvents()
	{
		require_once(app_path() . '/Tasks/Classes/UpdateEvents.php');
		$updateEventsObj = new \UpdateEvents;
		$updateEventsObj->update();
		
		Helper::message('success', ['Events data has been successfully updated.']);
	}
	
	public static function dateToTimestamp($date, $replaceSlashes = true)
	{
		if($replaceSlashes) $date = str_replace('/', '-', $date);
		
		return strtotime($date);
	}
	
	public static function isSecure() 
	{
		return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443;
	}
	
	public static function getPaypalCurrencies()
	{
		return [
			'currencies' => [
				"AUD" => ["name" => "Australian dollar", "symbol" => "$"],
				"BRL" => ["name" => "Brazilian real**", "symbol" => "R$"],
				"CAD" => ["name" => "Canadian dollar", "symbol" => "$"],
				"CZK" => ["name" => "Czech koruna", "symbol" => "Kč"],
				"DKK" => ["name" => "Danish krone", "symbol" => "kr"],
				"EUR" => ["name" => "Euro", "symbol" => "€"],
				"HKD" => ["name" => "Hong Kong dollar", "symbol" => "$"],
				"HUF" => ["name" => "Hungarian forint*", "symbol" => "Ft"],
				"ILS" => ["name" => "Israeli new shekel", "symbol" => "₪"],
				"JPY" => ["name" => "Japanese yen*", "symbol" => "¥"],
				"MYR" => ["name" => "Malaysian ringgit**", "symbol" => "RM"],
				"MXN" => ["name" => "Mexican peso", "symbol" => "$"],
				"TWD" => ["name" => "New Taiwan dollar*", "symbol" => "NT$"],
				"NZD" => ["name" => "New Zealand dollar", "symbol" => "$"],
				"NOK" => ["name" => "Norwegian krone", "symbol" => "kr"],
				"PHP" => ["name" => "Philippine peso", "symbol" => "₱"],
				"PLN" => ["name" => "Polish złoty", "symbol" => "zł"],
				"GBP" => ["name" => "Pound sterling", "symbol" => "£"],
				"SGD" => ["name" => "Singapore dollar", "symbol" => "$"],
				"SEK" => ["name" => "Swedish krona", "symbol" => "kr"],
				"CHF" => ["name" => "Swiss franc", "symbol" => "CHF"],
				"THB" => ["name" => "Thai baht", "symbol" => "฿"],
				"TRY" => ["name" => "Turkish lira**", "symbol" => "TRY"],
				"USD" => ["name" => "United States dollar", "symbol" => "$"],
			],
			'notices' => [
				'*'  => 'This currency does not support decimals. Passing a decimal amount will result in an error.',
				'**' => 'This currency is supported as a payment currency and a currency balance for in-country PayPal accounts only.',
			],
		];
	}
	
	public static function getCurrency()
	{
		return unserialize(Session::get('configs')['currency']);
	}
	
	public static function generateDownloadKey()
	{
		return 'dx_' . uniqid() . 'U3Z';
	}
	
	public static function getLocalIcalUrl($itemId, $downloadKey)
	{
		return URL::to('action/items/getIcs?p=' . $itemId . '&d=' . $downloadKey);
	}
	
	public static function urlToPath($url)
	{
		$path = parse_url($url, PHP_URL_PATH);
		return $_SERVER['DOCUMENT_ROOT'] . $path;
	}
	
	public static function embed($content, $message)
	{
		preg_match_all('/<img[^>]+>/i',$content, $result);
		
		$images = array();
		foreach($result[0] as $img_tag)
		{
			preg_match_all('/(alt|title|src)=("[^"]*")/i', $img_tag, $images[$img_tag]);
		}
		
		// Fetch the src key in the array.
		foreach($images as $image){
			foreach($image[1] as $key => $attr){
				if($attr == 'src'){
					$srcKey = $key;
					break;
				}
			}
			break;
		}

		foreach($images as $image)
		{	
			$src = Helper::urlToPath(str_replace('"', '', $image[2][$srcKey]));
			$content = str_replace($src, $message->embed((string)$src), $content);
		}
		
		return $content;
	}
	
	public static function sendCmsMail($data, $email)
	{
		if(isset($email['subject']) && isset($data['tokens']))
		{
			$email['subject'] = Helper::prepareMailSubject($email['subject'], $data['tokens']);
		}
		
		Mail::queue('emails.cms', $data, function($message) use ($email)
		{
			$message->from($email['from_address'], $email['from_name']);
			$message->to($email['to_address']);
			$message->subject($email['subject']);
		});
	}
	
	public static function prepareMailSubject($subject, $tokens)
	{
		if(is_object($tokens)){
			$tokens = json_decode(json_encode($tokens), true);
		}
		
		// Match tokens.
		$matches = [];
		preg_match_all('/{{([^}}]*)}/', $subject, $matches);

		// Replace tokens.
		foreach($matches[1] as $token)
		{
			if(isset($tokens[$token])){
				$subject = str_replace('{{' . $token . '}}', $tokens[$token], $subject);
			}
		}
		
		return $subject;
	}
	
	public static function prepareMail($content, $message, $tokens)
	{
		// Generate embeded images.
		$content = Helper::embed($content, $message);
		
		// Match tokens.
		$matches = [];
		preg_match_all('/{{([^}}]*)}/', $content, $matches);
		
		// Replace tokens.
		foreach($matches[1] as $token)
		{
			if(isset($tokens[$token])){
				$content = str_replace('{{' . $token . '}}', $tokens[$token], $content);
			}
		}
		
		return $content;
	}
	
	public static function timestampToDate($timestamp)
	{
		return date("d/m/Y", $timestamp);
	}
	
	public static function flog($data, $file = 'system.log')
	{
		$logDirectoryPath = base_path() . '/../log/';
		if (!file_exists($logDirectoryPath)) {
			mkdir($logDirectoryPath, 0777, true);
		}

		$file = $logDirectoryPath . $file;
		$data = $data . PHP_EOL;
		file_put_contents($file,$data,FILE_APPEND);
	}
	
	public static function getCurrentPage()
	{
		$pageNumber = Request::input('page');
		$pageNumber = $pageNumber ? $pageNumber : 1;
		
		$itemsPerPage = 10;
		$startFrom = ($pageNumber-1) * $itemsPerPage;
		
		$page = new \stdClass();
		$page->number 		= $pageNumber;
		$page->start	 	= $startFrom;
		$page->itemsPerPage = $itemsPerPage;
		$page->actionUrl 	= Request::url();
		
		return $page;
	}
	
	public static function getAvailablePages($table, $itemsPerPage, $colToCheck = false, $groupBy = false)
	{
		$itemsNumber = Helper::countTableRows($table, $colToCheck, $groupBy);
		return ceil($itemsNumber / $itemsPerPage);
	}
	
	public static function countTableRows($table, $colToCheck = false, $groupBy = false)
	{
		// Count number of matching rows.
		$selectString = "SELECT COUNT(*) FROM `" . $table . "`";
		
		if($colToCheck)
		{
			$selectString .= " WHERE " . $colToCheck['name'] . "=" . $colToCheck['value'];
		}
		
		if($groupBy)
		{
			$selectString .= "GROUP BY " . $groupBy;
		}

		$countResult = DB::select( DB::raw($selectString) );
		
		if($groupBy)
		{
			return count($countResult);
		}

		return reset($countResult[0]);
	}
	
	public static function calculateElapsedTime($datetime)
	{
		$time = strtotime($datetime);
		$time = time() - $time;
		
		$time = ($time < 1) ? 1 : $time;
		$tokens = [
			31536000 => 'yr',
			2592000 => 'month',
			604800 => 'week',
			86400 => 'day',
			3600 => 'hr',
			60 => 'min',
			1 => 'sec',
		];

		foreach($tokens as $unit => $text) 
		{
			if ($time < $unit) continue;
			
			$numberOfUnits = floor($time / $unit);
			return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '') . '.';
		}
	}
	
	public static function sortByValue($data, $keyOfValue)
	{
 		if(is_array($data[0]))
		{
			usort($data, function($a, $b) use ($keyOfValue)
			{
				return $a[$keyOfValue] - $b[$keyOfValue];
			});
			
			return $data;
		}
		
		usort($data, function($a, $b) use ($keyOfValue)
		{
			return $a->$keyOfValue - $b->$keyOfValue;
		});
		
		return $data;
	}
}
