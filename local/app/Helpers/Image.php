<?php

/* use DB;
use Input;
use Helper;
use Session;
use Validator; */
 
class Image{
 
    public static function upload($input, $destination = false, $inputName = 'image')
	{
	
		if(!$destination) $destination = base_path() . '/resources/assets/img/banners';
		
		// Setting up rules.
		$rules = array($inputName => 'image');
		
		// Getting the uploaded file.
		$filesObj = array_get($input,$inputName);
		if(!is_array($filesObj)){
			$files[0] = $filesObj;
		}else{
			$files = $filesObj;
		}
		
		$errors = [];
		$uploadedImages = [];
		foreach($files as $key => $file)
		{
			if($file){
				// Doing the validation, passing post data, rules and the messages.
				$validationArr = array($inputName => $file);
				
				$validator = Validator::make($validationArr, $rules);
				if ($validator->fails()) {
					
					$errors[$key] = [
						'name' 	 => $file->getClientOriginalName(),
						'error'  => 'validation failure.',
					];
				}
				else {
					// Checking if the file is valid.
					if ($file->isValid()){
						$fileName = $file->getClientOriginalName();
						$file->move($destination, $fileName); // uploading file to given path
						
						$uploadedImages[$key] = $fileName;
					}else{

						$errors[$key] = [
							'name' 	 => $file->getClientOriginalName(),
							'error'  => 'Uploaded file is not valid.',
						];
					}
				}
			}
		}
		
		if($uploadedImages){
			$successMessage = 'The following images:<br/>';
			foreach($uploadedImages as $image){
				$successMessage .= $image . ', ';
			}
			$successMessage = substr($successMessage, 0, -2) . '<br/> have been uploaded successfully.';
			
			Helper::message('success', [$successMessage]);
		}
		
		if($errors){
			$errorMessage = '';
			foreach($errors as $error){
				$errorMessage .= 'The file ' . $error['name'] . ' is not valid. <br/>';
			}
			
			Helper::message('danger', [$errorMessage]);
		}
		
		return ['errors' => $errors, 'images' => $uploadedImages];

    }
	
	public static function getBanners($identifier)
	{	
 		if(is_array($identifier))
		{
			$banners = DB::table('banner')
				->whereIn('identifier', $identifier)
				->orderBy('identifier', 'asc')
				->get();
			
			$bannersArr = [];
			foreach($banners as $banner)
			{
				$bannersArr[$banner->identifier][] = $banner;
			}
			
			$sortedBanners = [];
 			foreach($bannersArr as $identifier => $array)
			{
				$sortedBanners[$identifier] = Helper::sortByValue($array, 'position');
			}

			return $sortedBanners;
		}
		
		$banners = DB::table('banner')
			->where('identifier', $identifier)
			->orderBy('position', 'asc')
			->get();
		
		return $banners;
	}
	
	// The $fileName variable should be without the extension.
	public static function resize($filePath, $resizedDir, $widthToResize, $heightToResize = false, $fileName = false, $extension = false)
	{
		$resizedDir = rtrim($resizedDir, '/');

		if(!$fileName){
			$info = pathinfo($filePath);
			$fileName = $info['filename'];
		}

		if(($widthToResize == false) && ($heightToResize == false)){
			// No resize - return the original file.
			return $filePath;
		}

		try {
			$image = new imagick($filePath);
			$image->setBackgroundColor(new ImagickPixel('transparent'));		
			$imageprops = $image->getImageGeometry();
			$width = $imageprops['width'];
			$height = $imageprops['height'];
			$imageProportions = $height/$width;

			$bestfit = true;
			if($widthToResize && $heightToResize){
				$newWidth = $widthToResize;
				$newHeight = $heightToResize;
				$bestfit = false;
			}elseif($widthToResize){
				$newWidth = $widthToResize;
				$newHeight = $widthToResize * $imageProportions;
			}else{
				$newHeight = $heightToResize;
				$newWidth = $heightToResize/$imageProportions;
			}

			$image->resizeImage($newWidth,$newHeight, imagick::FILTER_LANCZOS, 0.9, $bestfit);
			// $image->cropImage (80,80,0,0);

			$extension = $extension ? $extension : strtolower($image->getImageFormat());
			$resizedFile = $resizedDir .  '/' . $fileName . '.' . $extension;

			$image->writeImage($resizedFile);
			
			return $resizedFile;
			
		}catch(\Exception $e){
			Helper::exception($e);
		}
	}
 
}