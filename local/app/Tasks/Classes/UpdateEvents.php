<?php
use \iCalUtilityFunctions;

class UpdateEvents{

	public $appPath = __DIR__  . '/../../';
	public $envPath;
 	public $icsDirPath;
	public $envDetails;
	public $listConf;
	public $icsUrls;
	public $timezone;
	
	public function __construct()
	{
		$this->envPath = $this->appPath . '../.env';
		$this->icsDirPath = $this->appPath . 'Libraries/ics-parser-master/ics/';

		// Fetch enviroment details.
		$this->envDetails = $this->getEnviromentDetails();
		
		// Fetch list configurations.
		$this->listConf = $this->getListConf();
		
		// Set default timezone.
		$this->timezone = $this->getTimeZone();
		date_default_timezone_set($this->timezone);
	}
	
	public function __destruct() 
	{
		exec ("find " . $this->icsDirPath . " -type d -exec chmod 0777 {} +");
		exec ("find " . $this->icsDirPath . " -type f -exec chmod 0777 {} +");
	}
	
	private function getTimeZone()
	{
		// Get DB instance.
		$conn = $this->dbConnect();
		
		$data = $conn->query('SELECT value FROM config WHERE name = "timezone"');

 		foreach($data as $row) {
			$timezone = $row['value'];
			break;
		}

		return $timezone;
	}
	
	public function updateFiles()
	{
		$this->icsUrls = $this->getIcsUrls();
		
		$this->updateAirbnbIcsFiles();
		$this->updateLocalFiles();
		$this->mergeIcsFiles();
	}
	
	public function updateLocalFiles()
	{
		
		// Get DB instance.
		$conn = $this->dbConnect();
		
		// Fetch & arrange all the existing apartments Ids.
		$apartments = $conn->query('SELECT id, name, country, city, street, house_number FROM `item`');
		
		$apartmentsData = [];
		$apartmentsIds = [];
		while ($row = $apartments->fetch(PDO::FETCH_ASSOC)){
		   $apartmentsIds[] = $row['id'];
		   $apartmentsData[$row['id']] = $row;
		} 

		$data = $conn->query('SELECT * FROM local_events');
		
		$icsDir = __DIR__  . '/../../Libraries/ics-parser-master/ics/';
		$icalDir = __DIR__  . '/../../Libraries/iCalcreator-2.22.1/';
		require_once( $icalDir . 'iCalcreator.php' );
			
		// Arrange the array.
		$eventsToUpdate = [];
		$rowIds = '';
		foreach($data as $row){
			$eventsToUpdate[$row['item_id']][] = $row;
			$rowIds .= $row['id'] . ',';
		}
		$rowIds = rtrim($rowIds, ',');
		
 		foreach($apartmentsIds as $id) 
		{	
			// If there are no local events for the current apartment, continue.
			if(!isset($eventsToUpdate[$id])){
				// If there is a local ics file for the current apartment, remove it.
				if(file_exists($icsDir . "local/local_" . $id . ".ics")){
					unlink($icsDir . "local/local_" . $id . ".ics");
				}
				continue;
			}
			
			$location = $apartmentsData[$id]['street'] . ' ' . $apartmentsData[$id]['house_number'] . ', ' . $apartmentsData[$id]['city']
			. ', ' . $apartmentsData[$id]['country'];
			
			$summary = $apartmentsData[$id]['name'];
			
			$tz     = $this->timezone;
			$config = array( "unique_id" => "kigkonsult.se" ,"TZID" => $tz );

			$v      = new \vcalendar( $config );
			$v->setProperty( "method", "PUBLISH" );
			$v->setProperty( "x-wr-calname", "Calendar Sample" );
			$v->setProperty( "X-WR-CALDESC", "Calendar Description" );
			$v->setProperty( "X-WR-TIMEZONE", $tz );
			$xprops = array( "X-LIC-LOCATION" => $tz );
			  
			iCalUtilityFunctions::createTimezone( $v, $tz, $xprops );
			
			foreach($eventsToUpdate[$id] as $event)
			{
				$checkInDateStamp = $event['checkin_timestamp'];
				$checkInDate = date('Ymd\THis', $checkInDateStamp);
				
				$checkOutDateStamp = $event['checkout_timestamp'];
				$checkOutDate = date('Ymd\THis', $checkOutDateStamp);
				
				// START VEVENT.  
				$vevent = & $v->newComponent( "vevent" );

				$vevent->setProperty( "dtstart", $checkInDate, array("VALUE" => "DATE"));
				$vevent->setProperty( "dtend", $checkOutDate, array("VALUE" => "DATE"));
				$vevent->setProperty("LOCATION", $location);
				$vevent->setProperty("summary", $summary);
				
				// $description = "CHECKIN:  27/09/2014\nCHECKOUT: 05/10/2014\nNIGHTS:   8\nPHONE:+7 916 922 01 93\nEMAIL:    (aucun alias d'e-mail disponible)\nPROPERTY: LARGE 2BR DESIGN apart next toBeach";
				// $vevent->setProperty( "description", $description );
				// END VEVENT
			}
			
			// Create the ics file in the local directory.
			$v->setConfig("directory", $icsDir . "local");
			$v->setConfig("filename", "local_" . $id . ".ics");
			$v->saveCalendar();
		}
	}
	
	public function mergeIcsFiles()
	{
		$icsArr = $this->icsUrls;
		$airbnbIcsLocation = $this->icsDirPath . 'airbnb/';
		$localIcsLocation = $this->icsDirPath . 'local/';
		$mergedIcsLocation = $this->icsDirPath . 'merged/';
		
		foreach($icsArr as $apartmentId => $icsUrl){
			
			if(file_exists($airbnbIcsLocation . 'airbnb_' . $apartmentId . '.ics')){
				// If exists in airbnbIcsLocation (airbnb folder) & local ics folder - merge it. 
				if(file_exists($localIcsLocation . 'local_' . $apartmentId . '.ics')){
					
					// Merge and place it in the merged folder with the required name.
					$filesToMerge = [
						$apartmentId => [
							[
								'directory' => $airbnbIcsLocation,
								'filename' 	=> 'airbnb_' . $apartmentId . '.ics',
							],
							[
								'directory' => $localIcsLocation,
								'filename' 	=> 'local_' . $apartmentId . '.ics',
							],
							
						]
					];
					
					$this->mergeFiles($filesToMerge);
					
				}else{
					// Copy the airbnb file to the merged folder with the required name.
					copy($airbnbIcsLocation . 'airbnb_' . $apartmentId . '.ics', $mergedIcsLocation . 'merged_' . $apartmentId . '.ics');
				}
			}else{
				if(file_exists($localIcsLocation . 'local_' . $apartmentId . '.ics')){
					// Copy the local file to the merged folder with the required name.
					copy($localIcsLocation . 'local_' . $apartmentId . '.ics', $mergedIcsLocation . 'merged_' . $apartmentId . '.ics');

				}else{
					// Remove the file from the merged folder if exists.
					if(file_exists($mergedIcsLocation . 'merged_' . $apartmentId . '.ics')){
						unlink($mergedIcsLocation . 'merged_' . $apartmentId . '.ics');
					}
				}
			}
		}
	}

	public function mergeFiles($filesToMerge, $dir = "merged")
	{
		$icalDir = __DIR__  . '/../../Libraries/iCalcreator-2.22.1/';
		require_once( $icalDir . 'iCalcreator.php' );
		
		$config = array( "unique_id" => "kigkonsult.se" );
		$v = new \vcalendar($config);
		
		foreach($filesToMerge as $apartmentId => $filesArr)
		{
			foreach($filesArr as $file)
			{
				$config = array( "directory" => $file['directory'], "filename" => $file['filename'] );
				$v->setConfig($config);
				$v->parse();	
			}
			
			// Create one merged file.
			$v->setConfig("directory", $this->icsDirPath . $dir);
			$v->setConfig("filename", $dir . '_' . $apartmentId . '.ics');
			$v->saveCalendar();
		}
	}
	
	public function updateAirbnbIcsFiles()
	{
		$airbnbIcsUrls = $this->icsUrls;
		$this->checkIfNecessaryDirsExist();
		$airbnbIcsLocation = $this->icsDirPath . 'airbnb/';
		
		foreach($airbnbIcsUrls as $apartmentId => $icsUrl){
			if($icsUrl){
				$icsFile = $this->curlGetContents($icsUrl);
				file_put_contents($airbnbIcsLocation . 'airbnb_' . $apartmentId . '.ics', $icsFile);
			}
		}
	}
	
	public function curlGetContents($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		$data = curl_exec($ch);
		curl_close($ch);
		
		return $data;
	}
	
	private function checkIfNecessaryDirsExist()
	{
		$directories = [
			$this->icsDirPath,
			$this->icsDirPath . 'airbnb',
			$this->icsDirPath . 'merged',
			$this->icsDirPath . 'local',
		];
		
		foreach($directories as $directoryPath){
			if (!file_exists($directoryPath)) {
				mkdir($directoryPath, 0777, true);
			}		
		}
	}

	public function getEnviromentDetails()
	{
		$lines = file($this->envPath);
		$variablesArr = [];
		foreach ($lines as $line) {
			if($line){
				$lineArr = explode("=", $line);
				if(isset($lineArr[1])){
					$variablesArr[$lineArr[0]] = $lineArr[1];	
				}
			}
		}

		return $variablesArr;
	}
	
	public function getListConf()
	{
		// Get DB instance.
		$conn = $this->dbConnect();
		
		$data = $conn->query('SELECT serialized_array FROM config WHERE name = "list_config"');
		foreach($data as $row) {
			$listConf = unserialize($row['serialized_array']);
			break;
		}

		return $listConf;
	}
	
	public function dbConnect()
	{
		$username = trim($this->envDetails['DB_USERNAME']);
		$password = trim($this->envDetails['DB_PASSWORD']);
		$host 	  = trim($this->envDetails['DB_HOST']); 	
		$dbname   = trim($this->envDetails['DB_DATABASE']);
		
		try {
			// Connection.
			$conn = new PDO('mysql:host=' . $host . ';dbname=' . $dbname, $username, $password);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			return $conn;
			
		} catch(PDOException $e) {
			echo 'ERROR: ' . $e->getMessage();
		}
	}
	
	public function getIcsUrls()
	{
		$conn = $this->dbConnect();
		$data = $conn->query('SELECT id, airbnb_ics FROM item WHERE status = 1');
		
		$airbnbUrls = [];
		foreach($data as $row) 
		{
			$airbnbUrls[$row['id']] = $row['airbnb_ics'];
		}
		
		return $airbnbUrls;
	}
	
	public function update()
	{
		$this->updateFiles();
		$events = $this->getIcalArr();
	
		// If check out date is in the future - insert it to the $futureEvents array.
		$allEvents = [];
		$futureEvents = [];
		foreach($events as $id => $apartment)
		{	
			foreach($apartment as $event)
			{
				$eventCheckOutStamp = @strtotime($event['DTEND']);
				$eventCheckInStamp = @strtotime($event['DTSTART']);
				
				$row = [
					'item_id' => $id,
					'checkin_timestamp' => $eventCheckInStamp,
					'checkout_timestamp' => $eventCheckOutStamp,
				];
				$allEvents[] = $row;
				
				if($eventCheckOutStamp >= time()){
					$futureEvents[] = $row;
				}
			}
		}
		
		$this->insertMultiple($futureEvents, 'future_events', true);
		$this->insertMultiple($allEvents, 'events', true);
	}
						
	public function insertMultiple($insertArr, $table, $truncate = false)
	{
		$cols = array_keys($insertArr[0]);

		// Prepare the multiple insert query.
		$queryStart = "INSERT INTO " . $table . " (";
		
		$tokensString = '';
		$queryFields = '';
		foreach($cols as $colName)
		{
			$queryFields .= $colName . ", ";
			$tokensString .= "?, ";
		}
		
		$tokensString = substr($tokensString, 0, -2);
		$tokensString = "(" . $tokensString . ")";
		
		$queryFields = substr($queryFields, 0, -2);
		$queryEnd = ") VALUES ";
		
		$query = $queryStart . $queryFields . $queryEnd;
		
		
		$qPart = array_fill(0, count($insertArr), $tokensString);
		$query .=  implode(",",$qPart);
		
		// Get the DB instance.
		$conn = $this->dbConnect();
		
		// Empty the table.
		if($truncate){
			$conn->exec("TRUNCATE TABLE " . $table);
		}
		
		
		$stmt = $conn->prepare($query); 
		$i = 1;
		
		foreach($insertArr as $row)
		{
			foreach($cols as $colName)
			{
				$stmt->bindValue($i++, $row[$colName]);
			}
		}
		
		// Insert row to DB with the apt ID, check in timestamp & check out timestamp.
		$stmt->execute();
		
	}		
					
	public function getIcalArr()
	{
		$airbnbIcsUrls = $this->icsUrls;
		$icsParserPath = $this->appPath . 'Libraries/ics-parser-master/class.iCalReader.php';
		require $icsParserPath;
		
		$events = [];
		foreach($airbnbIcsUrls as $apartmentId => $icsUrl)
		{
			$icsPath = $this->appPath . 'Libraries/ics-parser-master/ics/merged/merged_' . $apartmentId . '.ics';
			
			// If the merged file doesn't exist - continue to the next apartment.
			if(!file_exists($icsPath)) continue;
			
			$ical = new \ical($icsPath);
			$events[$apartmentId] = $ical->events();
		
		}
		
		return $events;
	}
	
}
