<?php
class Autoloader {
    static public function loader($className) {
        $filename = __dir__  . '/Classes/' . str_replace('\\', '/', $className) . ".php";

        if (file_exists($filename)) {
            include_once($filename);
            if (class_exists($className)) {
                return TRUE;
            }
        }
        return FALSE;
    }
}
spl_autoload_register('Autoloader::loader');