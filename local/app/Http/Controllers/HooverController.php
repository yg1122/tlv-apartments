<?php 

namespace App\Http\Controllers;

use Request;
// use Cookie;
// use Session;
use DB;
use Helper;
use App\Models\ItemModel;
use App\Http\Controllers\Controller;


class HooverController extends Controller {
	
	public $children = [];
	public $dataStateJson;
	private $itemModel;
	
	public function fetch()
	{
		$this->itemModel = new ItemModel();
		// $hooverTempDir = base_path() . '/app/Libraries/hoover/tmp/';
 		// $url = 'https://www.airbnb.com/rooms/7807461';
		$url = 'https://www.airbnb.com/rooms/7807460';
		
		$html = Helper::curlGetContents($url);
		// file_put_contents($hooverTempDir . 'airbnb_html.html', $html);
		// $html = file_get_contents($hooverTempDir . 'airbnb_html.html');


		// $classname = 'smart-banner';
		$dom = new \DOMDocument;
		libxml_use_internal_errors(true);
		$dom->loadHTML($html);
		libxml_use_internal_errors(false);
		// echo $dom->saveHTML($dom->getElementsByTagName('div')->item(0));
		
		
		
		// all links in #content
/*  		$container = $dom->getElementById("details-column");
		$arr = $container->getElementsByTagName("p"); */
/* 		
		$delArr = $container->getElementsByTagName("del");
		
		foreach($delArr as $item) {
			// $href =  $item->getAttribute("href");
			$text = trim(preg_replace("/[\r\n]+/", " ", $item->nodeValue));
			Helper::log($text);
			echo '----------------<br/>';
		}
		die(); */
		$xpath = new \DOMXpath($dom);
		$detailsElem = $xpath->query('//div[@class="___iso-state___p3about_this_listingbundlejs"]');
		
		$dataStateAttr = $detailsElem[0]->getAttribute("data-state");
		$this->dataStateJson = json_decode($dataStateAttr);
		// Helper::log($this->dataStateJson);

		$listingId = $this->dataStateJson->listingId;
		
 		$id = DB::table('item')
				->select('id')
				->where('id', $listingId)
				->first(); 
		if($id){
			die('The apartment ID: "' . $listingId . '" already exists.');
		}
			
		$spaceDetails = $this->getSpaceDetails();
		$priceDetails = $this->getPriceDetails();
		$amenities = $this->getAmenities();
		$amenitiesSerialized = $this->prepareAmenities($amenities);
		
		
		$description = $this->getDescription();
		$images = serialize($this->getImages());
		$houseRules = $this->dataStateJson->houseRules;
		$name = $this->dataStateJson->name;
		$summary = $this->dataStateJson->summary;
		$urlKey = strtolower(str_replace(' ', '-', $name));
		
		// Get price value (in dollars).
		$priceValue = $xpath->query('//div[@class="book-it__price-amount js-book-it-price-amount pull-left h3 text-special"]');
		$price = str_replace("$","",trim($priceValue[0]->nodeValue));

		// Get apartment location.
		$metaTags = $this->getMetaTags($dom);


		// TODO - arrange the main description part (by sections).
		$item = [
			'id'				=> $listingId,
			'status'			=> 1,
			'name'				=> $name,
			'bed_type'			=> false,
			'property_type'		=> $spaceDetails['Property type:'],
			'room_type'			=> $spaceDetails['Room type:'],
			'bedrooms'			=> $spaceDetails['Bedrooms:'],
			'beds' 				=> $spaceDetails['Beds:'],
			'bathrooms'			=> $spaceDetails['Bathrooms:'],
			'max_guests'		=> $spaceDetails['Accommodates:'],
			'cleaning_fee'		=> $priceDetails['cleaning_fee'],
			'amenities'			=> $amenitiesSerialized,
			'about'				=> $summary,
			'short_description' => $summary,
			'rules'				=> $houseRules,
			'url_key'			=> $urlKey,
			'cancellation'		=> 0,
			'min_nights'		=> 0,
			'images'			=> $images,
			'price'				=> $price,
			// Check if the next 3 exists in some cases.
			'apartment_number'  => false,
			'house_number'		=> false,
			'size'				=> 0,
			// TODO - strtolower ??
			'country'			=> $metaTags['airbedandbreakfast:country'],
			'city'				=> $metaTags['airbedandbreakfast:city'],
			// TODO - if lat/lng empty - fetch coordiantes from google.
			'coordinates'		=> $metaTags['airbedandbreakfast:location:latitude'] . ',' . $metaTags['airbedandbreakfast:location:longitude'],
			'd_key' 			=> Helper::generateDownloadKey(),
		];
		
		try {
			$id = DB::table('item')->insertGetId($item);
			if($id){
				Helper::message('success', ['The item: "' . $item['name'] . '" has been created successfully.']);
				return redirect('/apartments/' . $item['url_key']);
			}
					
		}catch(\Exception $e){
			Helper::exception($e);
		}
			
		die();
		
		
		$about = trim(preg_replace("/[\r\n]+/", " ", $arr[0]->nodeValue));
		
		// ABOUT !!! +++++++++++++++++++++++++++++++++++++++++++++++++++
		Helper::log($about);
		echo '----------------------------------------<br/><br/>';

		$xpath = new \DOMXpath($dom);
		$descriptionElems = $xpath->query('//div[@class="row description"]');

		$result = $this->xmlToArray($descriptionElems[0]);
		
		$this->prepareArrayData($result);
		
		$descriptionArr = $this->arrangeSections();
			
			
		// DESCRIPTION !!! ++++++++++++++++++++++++++++++++++++++++
		Helper::log($descriptionArr);
		echo '--------------------------------------------<br/><br/>';

		// }
		// var_dump($descriptionElems);
		// all links in .blogArticle
/*  		$links = array();
		foreach($descriptionElems as $container) {
			$arr = $container->getElementsByTagName("a");
			foreach($arr as $item) {
				$href =  $item->getAttribute("href");
				$text = trim(preg_replace("/[\r\n]+/", " ", $item->nodeValue));
				$links[] = array(
					'href' => $href,
					'text' => $text
				);
			}
		} */
		
			
		// $descriptionElems = $xpath->query('//div[@class="row description"]');

		// $result = $this->xmlToArray($descriptionElems[0]);

		
/*  		$xpath = new \DOMXPath($dom);
		$results = $xpath->query("//*[@class='" . $classname . "']");

		if ($results->length > 0) {
			echo $review = $results->item(0)->nodeValue;
		} */
	}
	
	private function getMetaTags($dom)
	{
		$metaTags = [];
		foreach ($dom->getElementsByTagName('meta') as $element)
		{
			if($element->getAttribute('property')){
				$metaTags[$element->getAttribute('property')] = $element->getAttribute('content');
			}
		}
		return $metaTags;
	}
	
	private function prepareAmenities($amenities)
	{
		$amenitiesArr = [];
		foreach($amenities as $key => $amenity)
		{
			$isPresent = $amenity['is_present'] ? 1 : 0;
			$amenitiesArr[$amenity['name']] = $isPresent;
		}
		
		return serialize($amenitiesArr);
	}
	
	private function getImages()
	{
		$imagesPath = base_path() . '/resources/assets/img/product/';
		
		$images = [];
		foreach($this->dataStateJson->photos as $image)
		{
			$randomNumber = uniqid();
			
/* 			$imageUrlArr = parse_url($image->picture);
			$extension = strtolower(pathinfo($imageUrlArr['path'], PATHINFO_EXTENSION));
			copy($image->picture, $imagesPath . 'image/' . $randomNumber . '.' . $extension); */
		
			// TODO - resize all xxl pics.
			$xxlImageUrl = str_replace("x_large","xx_large",$image->xl_picture);
			$xxlImageUrlArr = parse_url($xxlImageUrl);
			$xxlExtension = strtolower(pathinfo($xxlImageUrlArr['path'], PATHINFO_EXTENSION));
			
			copy($xxlImageUrl, $imagesPath . $randomNumber . '.' . $xxlExtension);
			
			$images[] = [
				'title'				=> $image->caption,
				'alt'				=> $image->caption,
				'url'				=> '',
				'target'			=> 0,
				'position'			=> $image->sort_order,
				'name' 				=> $randomNumber . '.' . $xxlExtension,
				// 'is_professional' 	=> $image->is_professional,
			];
			
		}
		return $images;
	}
	
	private function getDescription()
	{
		$description = [];
		$description['simpleDescription'] = $this->dataStateJson->description->simpleDescription;

		if($this->dataStateJson->description->sectionedDescription){
			foreach($this->dataStateJson->description->sectionedDescription as $key => $value)
			{
				$description['sectionedDescription'][$key] = $value;
			}
		}
		
		return $description;
	}
	
	private function getAmenities()
	{
		$amenitiesArr = $this->itemModel->getAmenities(true);
		$amenitiesArr['Free Parking on Premises'] = 'free_parking';
		$amenitiesArr['Internet'] = 'wifi';
		$amenitiesArrKeys = array_keys($amenitiesArr);
		$amenitiesArrFlip = array_flip($amenitiesArr);
		$amenitiesArrFlip['wifi'] = 'WiFi';
		$amenitiesArrFlip['free_parking'] = 'Free Parking';

		
		$amenities = [];
		foreach($this->dataStateJson->amenities as $amenity)
		{
			if(in_array($amenity->name, $amenitiesArrKeys)){
				$amenities[$amenitiesArr[$amenity->name]] = [
					'key' 		 => $amenitiesArr[$amenity->name],
					'name' 		 => $amenitiesArrFlip[$amenitiesArr[$amenity->name]],
					'tooltip' 	 => $amenity->tooltip,
					'is_present' => $amenity->is_present,
				];
			}
		}
		return $amenities;
	}
	
	private function getPriceDetails()
	{
		$priceDetails = [];
		foreach($this->dataStateJson->priceAttributes as $key => $item)
		{
			$value = isset($item->value) ? $item->value : false;
			$priceDetails[$key] = $value;
		}
		return $priceDetails;
	}
	
	private function getSpaceDetails()
	{
		$spaceDetails = [];
		foreach($this->dataStateJson->spaceAttributes as $item)
		{
			$spaceDetails[$item->label] = $item->value;
		}
		return $spaceDetails;
	}
	
	public function xmlToArray($root) {
		$result = array();

/*  		if ($root->hasAttributes()) {
			$attrs = $root->attributes;
			foreach ($attrs as $attr) {
				$result['@attributes'][$attr->name] = $attr->value;
			}
		} */

		if ($root->hasChildNodes()) {
			$children = $root->childNodes;
			if ($children->length == 1) {
				$child = $children->item(0);
				if ($child->nodeType == XML_TEXT_NODE) {
					$result['_value'] = $child->nodeValue;
					return count($result) == 1
						? $result['_value']
						: $result;
				}
			}
			$groups = array();
			foreach ($children as $child) {
				if (!isset($result[$child->nodeName])) {
					$result[$child->nodeName] = $this->xmlToArray($child);
				} else {
					if (!isset($groups[$child->nodeName])) {
						$result[$child->nodeName] = array($result[$child->nodeName]);
						$groups[$child->nodeName] = 1;
					}
					$result[$child->nodeName][] = $this->xmlToArray($child);
				}
			}
		}
		
		return $result;
	}
	
	public function arrangeSections()
	{
		$counter = 0;
		$sections = [];
		foreach($this->children as $section)
		{
			if(isset($section['strong'])){
				$sections[$counter]['title'] = $section['strong']['span'];
			}elseif(!isset($section['span'])){
				$content = [];
				foreach($section as $child){
					if(isset($child['span'])){
						$content[] = $child['span'];
					}
				}
				$sections[$counter]['content'] = $content;
				$counter++;
			}elseif(isset($section['span'])){
				$sections[$counter]['content'] = $section['span'];
				$counter++;
			}
		}
		
		return $sections;
	}
	
	public function prepareArrayData($result)
	{
		if(count($result) >= 1){
			foreach($result as $elemType => $child)
			{
				// if(is_string($child)){
				if($elemType === 'p'){
					$this->children[] = $child;
				}elseif(is_array($child)){
					$this->prepareArrayData($child);
				}
			}
		}
	}
	
	public function xml_to_array($root) {
		$result = array();

		if ($root->hasAttributes()) {
			$attrs = $root->attributes;
			foreach ($attrs as $attr) {
				$result['@attributes'][$attr->name] = $attr->value;
			}
		}

		if ($root->hasChildNodes()) {
			$children = $root->childNodes;
			if ($children->length == 1) {
				$child = $children->item(0);
				if ($child->nodeType == XML_TEXT_NODE) {
					$result['_value'] = $child->nodeValue;
					return count($result) == 1
						? $result['_value']
						: $result;
				}
			}
			$groups = array();
			foreach ($children as $child) {
				if (!isset($result[$child->nodeName])) {
					$result[$child->nodeName] = $this->xml_to_array($child);
				} else {
					if (!isset($groups[$child->nodeName])) {
						$result[$child->nodeName] = array($result[$child->nodeName]);
						$groups[$child->nodeName] = 1;
					}
					$result[$child->nodeName][] = $this->xml_to_array($child);
				}
			}
		}

		return $result;
	}

	public function getContentArr($node) 
	{ 
		$childrenArr = []; 
		$children = $node->childNodes; 
		foreach ($children as $child) { 
			// $innerHTML .= $child->ownerDocument->saveXML( $child ); 
			$childrenArr[] = $child->nodeValue;
		} 

		return $childrenArr;  
	} 
	
	public function get_inner_html($node) 
	{ 
		$innerHTML= ''; 
		$children = $node->childNodes; 
		foreach ($children as $child) { 
			$innerHTML .= $child->ownerDocument->saveXML( $child ); 
			var_dump($child->tagName);
		} 

		return $innerHTML;  
	} 
	
	public function innerXML($node) 
	{
		$doc = $node->ownerDocument; 
		$frag = $doc->createDocumentFragment(); 
		foreach ($node->childNodes as $child) 
		{
			$frag->appendChild($child->cloneNode(TRUE)); 
		} 
		
		return $doc->saveXML($frag); 
	}
}