<?php 

namespace App\Http\Controllers;

use Image;
use Cache;
use Helper;
use Cookie;
use Request;
use Session;
use App\Models\ListModel;
use App\Http\Controllers\Controller;

class HomeController extends Controller {

	public function build()
	{	
			
		$listConf = unserialize(Session::get('configs')['list_config']);
		$listModel = new ListModel();
		
		// Get user search cookie if exists.
		$userSearchData = Request::Cookie('user_search_data');
			
		$page = new \stdClass;
		$page->disabledDates = $listConf['list_disabled_dates'];
		$page->listUrlKey = $listConf['list_url_key'];
		$page->user_search_data = $userSearchData ? $userSearchData : false;
		$page->minDate = $listModel->getMinDate($listConf);
		
		// Fetch cities array for search field.
		$page->cities = Helper::getCities();
		
		// Fetch homepage banners.
		$page->banners = Image::getBanners(['homepage', 'homepage-secondary']);

		$blocks = ['homepage-content' => Helper::block('homepage-content')];
		$page->blocks = $blocks;
		
		return view('pages.home')->with('page', $page);

	}

}