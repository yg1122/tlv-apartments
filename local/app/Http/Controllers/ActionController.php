<?php 

namespace App\Http\Controllers;

use App;
use App\Http\Controllers\Controller;


class ActionController extends Controller {
	
	// In order to allow the action, the action's path should be added to the array below.
	private $allowedPaths = [
		'action/checkout/prepareReservation',
		'action/items/getIcs',
		'action/contact/handleInquiry',
		'action/checkout/requestBooking',
	];
	
	public function execute($controller, $method)
	{
		// If restricted redirect to 404.
		if($this->isRestricted($controller, $method)) abort(404);
		
 		$controller = studly_case($controller) . 'Controller';
		$controllerPath = "App\Http\Controllers\\" . $controller;
		
		if(class_exists($controllerPath)){
			$object = new $controllerPath;
			if(method_exists($object, $method)){
				// Call the method & return the response.
				return App::make($controllerPath)->$method();
				
			}else{
				// TODO - throw error - method doesn't exist.
				abort(404);
			}
		}else{
			// TODO - throw error - class doesn't exist.
			abort(404);
		}
	}
	
	private function isRestricted($controller, $method)
	{
		$path = 'action/' . $controller . '/' . $method;
		if(in_array($path, $this->allowedPaths)){
			return false;
		}
		return true;
	}
	
}