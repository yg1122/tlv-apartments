<?php 

namespace App\Http\Controllers;

use DB;
use Mail;
use Image;
use Helper;
use Request;
use Session;
use Validator;
use App\Http\Controllers\Controller;


class ContactController extends Controller {
	
	public function build($section = false, $content = false)
	{
		$data = Helper::getAdminSection($section, $content);
		$data['content_title'] = 'Manage Inquiries';
		$data['inquiries'] = $this->getInquiriesTable();
		
		return view('admin.admin')->with('data', $data);
		
	}
	
	private function getInquiriesTable()
	{
		$inquiries = DB::table('contact')
					->select('id', 'email', 'name', 'subject', 'date')
					->orderBy('date', 'desc')
					->get();
		
		return $inquiries;
	}
	
	public function loadInquiry()
	{
		$id = Request::input('id');
		$data = Helper::getAdminSection('contact', 'inquiry');
		
		$data['inquiry'] = $this->getInquiry($id);
		$data['content_title'] = 'Inquiry ID: ' . $data['inquiry']->id;
		
		return view('admin.admin')->with('data', $data);
	}
	
	private function getInquiry($id)
	{
		$inquiry = DB::table('contact')
					->where('id', $id)
					->first();

		return $inquiry;
	}
	
	public function removeInquiry()
	{
		$params = Request::all();

		DB::table('contact')->where('id', $params['id'])->delete();	
		
		Helper::message('success', ['The inquiry has been successfully removed.']);
		return redirect('/admin/contact/manage');
	}
	
	public function load()
	{
		$page 		 = new \stdClass();
		$page->title = 'Contact Us';
		
		// Fetch contact us banner.
		$banner = Image::getBanners('contact-us');
		$page->banner = ($banner && isset($banner[0])) ? $banner[0] : false;
		
		// Fetch contact us content.
		$page->blocks = Helper::block(['contact-us-content', 'contact-us-details']);
		
		return view('pages.contact')->with('page', $page);
	}
	
	public function handleInquiry()
	{
		if(!Request::isMethod('post')) return;
		$params = Request::all();
		// TODO - add captcha. OR the csrf is enough????
		
		// Set validation rules.
		$rules = [
			'name'		 	 => 'required|max:255',
			'email'  		 => 'required|email|max:255',
			'subject'	 	 => 'required|max:255',
			'message'  		 => 'required',
		];
		
	    $validator = Validator::make($params, $rules);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }
		
		$email = [
			'from_address' 	=> $params['email'],
			'from_name'		=> 'Contact Us - from: ' . $params['name'],
			'to_address'	=> Session::get('configs')['contact_email'],
			'subject'		=> $params['subject'],
		];
		
		$data = [
			'content' => nl2br($params['message']),
		];
		
		$inquiry = [
			'name' 		=> $params['name'],
			'email'		=> $params['email'],
			'subject' 	=> $params['subject'],
			'message' 	=> $params['message'],
			'notify'	=> 1,
			'date'		=> date('Y-m-d H:i:s'),
		];
		
		// Insert inquiry to the contact table.
		DB::table('contact')->insert($inquiry);
		
		// Send mail to the contact email address defined in the admin settings.
		Mail::queue('emails.contact', $data, function($message) use ($email)
		{
			$message->from($email['from_address'], $email['from_name']);
			$message->to($email['to_address']);
			$message->subject($email['subject']);
		});
		
		Helper::message('success', ['Your inquiry has been sent successfully.']);
		return redirect()->back();
	}
}