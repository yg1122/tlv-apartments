<?php 

namespace App\Http\Controllers;

use DB;
use URL;
use Input;
use Helper;
use Session;
use Request;
use Redirect;
use Validator;
use App\Http\Controllers\Controller;


class StructureController extends Controller {
	
	private $admin;
	private $page;
	
	public function build($section = false, $content = false)
	{
		$this->admin 	= Helper::getAdminSection($section, $content);
		$this->page 	= $this->admin['page'];

		$structureData = $this->getStructureData($content);
		$data = array_merge($this->admin, $structureData);
		
		return view('admin.admin')->with('data', $data);
	}
	
	private function getStructureData($content)
	{
		switch ($content) {
			case 'categories':
				$data['categories'] = $this->getCategories(); 
				break;
			case 'header':
				$data['header'] = $this->getHeader();
				break;
			case 'footer':
				$data['footer'] = $this->getFooter();
				break;
			default:
				$data = $this->getCategories(); 
		}
		
		return $data;
	}
	
	private function getHeader()
	{
 		$configs = Session::get('configs');

		return [
			'content' => $configs['header'],
		];
	}
	
	private function getFooter()
	{
		$configs = Session::get('configs');
		
		return [
			'credits' => $configs['credits'],
			'content' => $configs['footer'],
		];
	}
	
	public function saveHeader()
	{
		$params = Request::all();
		
		DB::table('config')
			->where('name', 'header')
			->update(['value' => $params['content']]);
			
		Helper::message('success',['Header content has been updated successfully.']);
		return redirect()->back();
	}
	
	public function saveFooter()
	{
		$params = Request::all();
		
		$footer = [
			['col' => 'value', 'name' => 'credits'],
			['col' => 'value', 'name' => 'footer'],
		];
		$params['footer'] = $params['content'];
		
		foreach($footer as $field)
		{
			DB::table('config')
				->where('name', $field['name'])
				->update([$field['col'] => $params[$field['name']]]);
		}
			
		Helper::message('success',['Footer content has been updated successfully.']);
		return redirect()->back();
	}
	
	private function getCategories()
	{
		$this->page->pagesNumber = Helper::getAvailablePages('category', $this->page->itemsPerPage);
		$data['page']			 = $this->page;
		
		$categories = DB::table('category')
						->orderBy('position')
						->skip($this->page->start)
						->take($this->page->itemsPerPage)
						->get();
						
		return $categories;
	}
	
	public function getCategory($id)
	{
		$category = DB::table('category')
					->where('id', $id)
					->first();
					
		return $category;
	}
	
	public function removeCategory()
	{
		$id = Request::input('id');
		DB::table('category')->where('id', $id)->delete();
		
		// Update categories in config table.
		$this->updateCategories();
					
		Helper::message('success', ['The category has been successfully removed.']);
		return redirect('/admin/structure/categories');
	}
	
	public function addCategory()
	{
		$data = Helper::getAdminSection('structure', 'edit-category');
		$data['content_title'] = 'New Category';
		
		$data['category'] = new \stdClass;
		
		return view('admin.admin')->with('data', $data);
	}
	
	public function saveCategory()
	{
		if(!Request::isMethod('post')) return;
		$params = Request::all();
		
		// Set validation rules.
		$idRules = $params['category_id'] ? 'numeric|required' : '';
		$urlKeyRules = ($params['url_key'] === '') ? '' : ['required', 'max:255', 'regex:/^[a-zA-Z0-9_.-]*$/'];
		$rules = [
			'url_key'		 => $urlKeyRules,
			'status'		 => 'numeric|required',
			'category_id'	 => $idRules,
			'update'		 => 'numeric|required',
			'name'  		 => 'required|max:255',
		];
		
		// If the given URL key is a new one, make sure it doesn't exist.
		if($params['current_url_key'] != $params['url_key']){
			$urlKey = DB::table('category')
						->select('id')
						->where('url_key', $params['url_key'])
						->first(); 
			
			// If already exists throw an error message.	
			if($urlKey){
				Helper::message('danger', ['The URL Key: "' . $params['url_key'] . '" already exists.', 'There cannot be two identical URL Keys.']);
				return redirect()->back();
			}
		}else{
			// If the URL key value is empty make sure there is no empty URL key already in the DB.
			if($params['url_key'] === ''){
				$urlKey = DB::table('category')
						->select('id')
						->where('url_key', '')
						->first(); 
			
				// If already exists throw an error message.	
				if($urlKey){
					Helper::message('danger', ['The URL Key: "' . $params['url_key'] . '" already exists.', 'There cannot be two identical URL Keys.']);
					return redirect()->back();
				}
			}
		}
		
	    $validator = Validator::make($params, $rules);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }
		
		$category = [
			'url_key'	 => $params['url_key'],
			'status'	 => $params['status'],
			'position'	 => $params['position'],
			'name'		 => $params['name'],
		];
		
		
		if($params['update']){
			try {
			
				DB::table('category')
						->where('id', $params['category_id'])
						->update($category);
				
				// Update categories in config table.
				$this->updateCategories();
				
				Helper::message('success', ['The category: "' . $category['name'] . '" has been updated successfully.']);
				return redirect()->back();
						
			}catch(\Exception $e){
				Helper::exception($e);
			}
			
		}else{
			try {
				$id = DB::table('category')->insertGetId($category);
				if($id){
					// Update categories in config table.
					$this->updateCategories();
				
					Helper::message('success', ['The category: "' . $category['name'] . '" has been created successfully.']);
					return redirect('/admin/structure/edit-category/editCategory?id=' . $id);
				}
						
			}catch(\Exception $e){
				Helper::exception($e);
			}
		}
	}
	
	public function editCategory()
	{
		$id = Request::input('id');
		$data = Helper::getAdminSection('structure', 'edit-category');
		
		$data['category'] = $this->getCategory($id);
		$data['content_title'] = 'Edit category: "' . $data['category']->name . '" (' . $data['category']->id . ')';
		
		return view('admin.admin')->with('data', $data);
	}
	
	public function updateCategories()
	{
		$categories = DB::table('category')
						->select('name','url_key')
						->where('status', 1)
						->orderBy('position')
						->get();
		
		$categoriesObj = array();
		foreach($categories as $ctg){
				$ctg->id = str_replace(' ', '-', strtolower($ctg->name));
				$categoriesObj[] = $ctg;
		}
		
		DB::table('config')
			->where('name', 'categories')
			->update(['serialized_array' => serialize($categoriesObj)]);
	}
	
	public function resetContent()
	{
		$contentName = Request::input('name');
		
		$content = DB::table('default')
						->select('value')
						->where('name', $contentName)
						->first();
		
		// If the content doesn't exist, throw an error message.
		if((empty($content->value)) || ($content->value == false)){
			Helper::message('danger', ['There is no default content called: "' . $contentName . '"']);
			return redirect()->back();
		}
		
		DB::table('config')
			->where('name', $contentName)
			->update(['value' => $content->value]);
			
		Helper::message('success', ['The content has been updated successfully.']);
		return redirect()->back();
	}
}