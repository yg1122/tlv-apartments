<?php 

namespace App\Http\Controllers;

use DB;
use URL;
use Helper;
use Request;
use Session;
use App\Http\Controllers\Controller;


class AdminController extends Controller {

	public function build($section = false, $content = false)
	{	

		$data = Helper::getAdminSection($section, $content);
		return view('admin.admin')->with('data', $data);
	}
	
	public function loadLoginPage()
	{
		return view('admin.login');
	}
	
	public function login()
	{
		if(!Request::isMethod('post')) return;
		$params = Request::all();
		
		// If user name value doesn't exist, throw an error message.
		if(empty($params['username']) || $params['username'] == false){
			Helper::message('danger', ['User name value is required.']);
			return redirect()->back();
		}
		
		$whereConditions = [
			'username' => $params['username'],
			'password' => md5($params['password']),
			'type' 	   => 'admin',
		];
		
		$user = DB::table('user')
				->where($whereConditions)
				->first(); 
		
		if($user){
			// Set admin's user session & redirect to admin's main page.
			Session::put('user', $user);
			return redirect('/admin');
		}
		
		// Throw an error message if the login details are not correct.
		Helper::message('danger', ['The password or user name are not correct.']);
		return redirect('/admin');
	}
	
	public function logout()
	{
		Session::forget('user');
		return redirect('/admin');
	}
	
	public function route($controller = false, $content = false, $method = false)
	{
		$loginAttemptUrl = URL::to('/') . '/admin/admin/login/login';
		$user = Session::get('user');
		$isAdmin = (isset($user->type) && ($user->type == 'admin')) ? true : false;
		
		if(($isAdmin == false) && ($loginAttemptUrl != Request::url())){
			
			$controllerPath = "App\Http\Controllers\AdminController";
			$method = 'loadLoginPage';
			
			$app = app();
			$controller = $app->make($controllerPath);
			$parameters = [];
			
			return $controller->callAction($method, $parameters);
		}
		
		if(!$method){
			if($content){
			
				$parameters = [
					'section' => $controller,
					'content' => $content,
				];

				$method = 'build';
				
			}else{
			
				$parameters = [
					'section' => $controller,
					'content' => false,
				];
				
				$controller = 'admin';
				$method = 'build';
			}
			
		}else{
			
			$parameters = [
				'controller' => $controller,
				'content'	 => $content,
				'method' 	 => $method,
			];
		}
		
		$controller = studly_case($controller) . 'Controller';
		$controllerPath = "App\Http\Controllers\\" . $controller;

		$app = app();
		$controller = $app->make($controllerPath);
		
		return $controller->callAction($method, $parameters);
	}
	
	public function updateNotifications()
	{
		$params = Request::all();
		
		if(empty($params['table']) || empty($params['notifications']))
		{
			return;
		}
		
		foreach($params['notifications'] as $id)
		{
			DB::table($params['table'])
				->where('id', $id)
				->update(['notify' => 0]);
		}
		
		return response()->json('success');
	}
}