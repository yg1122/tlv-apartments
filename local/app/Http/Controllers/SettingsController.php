<?php 

namespace App\Http\Controllers;

use DB;
use URL;
use Image;
use Input;
use Helper;
use Request;
use Session;
use Redirect;
use Validator;
use App\Http\Controllers\Controller;


class SettingsController extends Controller {
	
	private $section;
	private $content;
	
	public function build($section = false, $content = false)
	{
		$this->section = $section;
		$this->content = $content;
		
		$adminsData    = Helper::getAdminSection($section, $content);
		$settingsData  = $this->getSettingsData($content);
		
		$data = array_merge($adminsData, $settingsData);
		
		return view('admin.admin')->with('data', $data);
	}
	
	private function getSettingsData($content)
	{
		switch ($content) {
			case 'general-settings':
				$data = $this->getGeneralSettings(); 
				break;
			case 'paypal-settings':
				$data = $this->getPaypalSettings(); 
				break;
			case 'item-settings':
				$data = $this->getItemSettings();
				break;	
			case 'list-settings':
				$data = $this->getListSettings();
				break;
			case 'login':
				$data = $this->getLoginDetails();
				break;
			case 'seo':
				$data = $this->getSeo();
				break;
			default:
				$data = $this->getGeneralSettings(); 
		}
		
		return $data;
	}
	
	private function getSeo()
	{
		$data['seo'] = unserialize(Session::get('configs')['seo']);
		$data['seo']['robots_values'] = [
			'INDEX, FOLLOW',
			'INDEX, NOFOLLOW',
			'NOINDEX, NOFOLLOW',
			'NOINDEX, FOLLOW',
		];
		
		return $data;
	}
	
	private function getLoginDetails()
	{
		$data['username'] = Session::get('user')->username;
		return $data;
	}
	
	public function updateLoginDetails()
	{
		if(!Request::isMethod('post')) return;
		$params = Request::all();
		
		// Set validation rules.
		$rules = [
			'username'	=>	'required|max:255',
			'password'	=>	'max:255',
		];
		
	    $validator = Validator::make($params, $rules);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }
		
		$user = ['username' => $params['username']];
		if(isset($params['password']) && isset($params['confirm_password'])){
			if(($params['password'] === $params['confirm_password']) && $params['password']){
				$user['password'] = md5($params['password']);
			}
		}
		
		try {
		
			DB::table('user')
				->where('type', 'admin')
				->update($user);
			
			$userSession = Session::get('user');
			$userSession->username = $user['username']; 
			
			if(isset($user['password'])){
				$userSession->password = $user['password']; 
			}
			
			Session::put('user', $userSession);
			Helper::message('success', ['Login details have been updated successfully.']);
			return redirect()->back();
					
		}catch(\Exception $e){
			Helper::exception($e);
		}
		
	}
	
	private function getItemSettings()
	{
		return [
			'item_performance_mode' => (int)Session::get('configs')['item_performance_mode'],
			'request_booking'		=> (int)Session::get('configs')['request_booking'],
		];
	}
	
	private function getListSettings()
	{
		return [
			'list_conf' => unserialize(Session::get('configs')['list_config']),
			'pagination' => $this->getPagination(),
			
		];
	}

 	private function getPagination()
	{
		return [
			3  => 3,
			5  => 5,
			10 => 10,
			12 => 12,
			15 => 15,
			20 => 20,
			24 => 24,
		];
	}

	private function getPaypalSettings() 
	{
		$paypalSettings = unserialize(Session::get('configs')['paypal']);
		
		return $paypalSettings;
	}
	
	private function getGeneralSettings() 
	{
		$configs = Session::get('configs');
		$data = [
			'timezones'					=> Helper::getTimeZoneArr(),
			'timezone'					=> $configs['timezone'],
			'contact_phone'				=> $configs['contact_phone'],
			'email_address'				=> $configs['email_address'],
			'email_name'				=> $configs['email_name'],
			'contact_email' 			=> $configs['contact_email'],
			'booking_requests_email' 	=> $configs['booking_requests_email'],
			'vat'						=> Helper::getVat(),
			'currency'					=> Helper::getCurrency(),
			'currenciesArr' 			=> Helper::getPaypalCurrencies(),
			'seo'						=> unserialize($configs['seo']),
			'website_name'				=> $configs['website_name'],
			'logo'						=> $configs['logo'],
			'favicon'					=> $configs['favicon'],
		];
		
		return $data;
	}
	
	public function saveItem()
	{
		if(!Request::isMethod('post')) return;
		$params = Request::all();
		
		// Set validation rules.
		$rules = [
			'item_performance_mode'	=> 'required|numeric',
			'request_booking'		=> 'required|numeric',
		];
		
	    $validator = Validator::make($params, $rules);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }
		
		$update = [
			'item_performance_mode',
			'request_booking',
		];
		
		foreach($update as $field)
		{
			DB::table('config')
				->where('name', $field)
				->update(['value' => $params[$field]]);
		}
		
		Helper::message('success', ['Item settings has been successfully saved.']);
		return redirect('/admin/settings/item-settings');
	}
	
	public function saveList()
	{	
		if(!Request::isMethod('post')) return;
		$params = Request::all();
		
		// Set validation rules.
		$rules = [
			'list_url_key'		=> 'required|max:255',
			'items_per_page'	=> 'required|numeric',
			'update_ical'		=> 'required|numeric',
			'list_title'		=> 'max:255',
		];
		
	    $validator = Validator::make($params, $rules);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }
		
		// If the update Ical files button has been submitted execute only the following operation.
		if($params['update_ical'])
		{
			Helper::updateEvents();
			return redirect('/admin/settings/list-settings');
		}
		
		$listConf = unserialize(Session::get('configs')['list_config']);
		
		$listConf['list_url_key'] 		 = $params['list_url_key'];
		$listConf['list_min_date_hours'] = $params['hours_in_advance']; 
		$listConf['list_disabled_dates'] = $params['disabled_dates'];
		$listConf['max_checkin_hour'] 	 = $params['max_checkin_hour'];
		$listConf['list_title']			 = $params['list_title'];
		$listConf['items_per_page']		 = $params['items_per_page'];
		
		$listConf = serialize($listConf);
		DB::table('config')
            ->where('name', 'list_config')
            ->update(['serialized_array' => $listConf]);
		
		Helper::message('success', ['List settings has been successfully saved.']);
		
		return redirect('/admin/settings/list-settings');
	}
	
	public function savePaypal()
	{
		if(!Request::isMethod('post')) return;
		$params = Request::all();
		
		// TODO - all admin methods must validate request method (get or post)
		// Set validation rules.
 		$rules = [
			'username'		=> 'required|max:255',
			'password'		=> 'required|max:255',
			'signature'		=> 'required|max:255',
			'mode' 			=> 'required|in:live,sandbox',
			'auto_redirect' => 'required|in:0,1',
		];
		
	    $validator = Validator::make($params, $rules);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }
		
		$update = [
			'username' 		=> trim($params['username']),
			'password' 		=> trim($params['password']),
			'signature' 	=> trim($params['signature']),
			'mode'			=> $params['mode'],
			'auto_redirect' => $params['auto_redirect'],
		];
		
		DB::table('config')
			->where('name', 'paypal')
			->update(['serialized_array' => serialize($update)]);
		
		Helper::message('success', ['PayPal settings has been successfully saved.']);
		return redirect('/admin/settings/paypal-settings');
	}
	
	public function saveSeo()
	{
		if(!Request::isMethod('post')) return;
		$params = Request::all();
		
		$rules = [
			'robots' 			=> 'required',
			'meta_description' 	=> 'max:255',
			'meta_title'		=> 'max:255',
			'meta_keywords'		=> 'max:255',
		];
		
		$validator = Validator::make($params, $rules);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }
		
		$seo = [
			'meta_title' 		=> isset($params['meta_title']) ? $params['meta_title'] : false,
			'meta_keywords'		=> isset($params['meta_keywords']) ? $params['meta_keywords'] : false,
			'meta_description' 	=> isset($params['meta_description']) ? $params['meta_description'] : false,
			'robots'			=> $params['robots'],
		];
		
		DB::table('config')
			->where('name', 'seo')
			->update(['serialized_array' => serialize($seo)]);
				
		Helper::message('success', ['SEO settings has been successfully saved.']);
		return redirect('/admin/settings/seo');
	}
	
	public function saveGeneral()
	{
		if(!Request::isMethod('post')) return;
		$params = Request::all();
		
		// Set validation rules.
		$rules = [
			'timezone'					=> 'required',
			'vat'						=> 'required|numeric',
			'currency'					=> 'required',
			'email_address' 			=> 'required|email|max:255',
			'email_name'				=> 'required|max:255',
			'contact_email' 			=> 'required|email|max:255',
			'booking_requests_email' 	=> 'required|email|max:255',
			'website_name'				=> 'max:255',
			'contact_phone'				=> 'max:255',
		];
		
	    $validator = Validator::make($params, $rules);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }
		
		// Upload images if exist.
		$iconsDir = base_path() . '/resources/assets/img/icons';
		$images = Image::upload($params, $iconsDir);

		// Update images.
		$params['logo']   	= isset($images['images'][0]) ? $images['images'][0] : $params['current_logo'];
		
		// Resize the image in order to convert the uploaded image to ico file.
		if(isset($images['images'][1])){
			$icoPath = Image::resize($iconsDir . '/' . $images['images'][1], $iconsDir, 15, false, 'fav', 'ico');
			$info = pathinfo($icoPath);
			$fileName = $info['filename'];
			$params['favicon'] = $fileName . '.ico';
		}else{
			$params['favicon'] = $params['current_favicon'];
		}
		
		$currenciesArr = Helper::getPaypalCurrencies();
		$currency = [
			'code'   => $params['currency'],
			'name' 	 => $currenciesArr['currencies'][$params['currency']]['name'],
			'symbol' =>	$currenciesArr['currencies'][$params['currency']]['symbol'],
		];
		$params['currency'] = serialize($currency);

		$update = [
			['col' => 'serialized_array', 'name' => 'currency'],
			['col' => 'value', 'name' => 'email_address'],
			['col' => 'value', 'name' => 'email_name'],
			['col' => 'value', 'name' => 'timezone'],
			['col' => 'value', 'name' => 'vat'],
			['col' => 'value', 'name' => 'contact_email'],
			['col' => 'value', 'name' => 'booking_requests_email'],
			['col' => 'value', 'name' => 'website_name'],
			['col' => 'value', 'name' => 'logo'],
			['col' => 'value', 'name' => 'favicon'],
			['col' => 'value', 'name' => 'contact_phone'],
		];

		foreach($update as $field)
		{
			DB::table('config')
				->where('name', $field['name'])
				->update([$field['col'] => $params[$field['name']]]);
		}

		// Running events update after timezone change.
		if($params['current_timezone'] != $params['timezone']){
			Helper::updateEvents();
		}
		
		Helper::message('success', ['General settings has been successfully saved.']);
		return redirect('/admin/settings/general-settings');
	}

}