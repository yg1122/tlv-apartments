<?php 

namespace App\Http\Controllers;

use DB;
use URL;
use Input;
use Helper;
use Request;
use Session;
use Validator;
use App\Models\ListModel;
use \RecursiveIteratorIterator;
use \RecursiveDirectoryIterator;
use App\Http\Controllers\Controller;


class CmsController extends Controller {
	
	private $admin;
	private $page;
	public $listConf;
	
	public function build($section = false, $content = false)
	{
		$this->admin 	= Helper::getAdminSection($section, $content);
		$this->page 	= $this->admin['page'];
		
		$data['content_data'] = $this->getPageData($content);
		$data = array_merge($this->admin, $data);
		
		return view('admin.admin')->with('data', $data);
		
	}
	
	private function getPageData($content)
	{
 		switch($content){
			case 'cms-pages':
				$pageData = $this->getCmsPages();
				break;
			case 'cms-blocks':
				$pageData = $this->getCmsBlocks();
				break;
			case 'emails':
				$pageData = $this->getEmails();
				break;
			default:
				$pageData = $this->getCmsPages();
		}
		
		return $pageData;
			
	}
	
	public function getEmails()
	{
		$this->page->pagesNumber = Helper::getAvailablePages('email', $this->page->itemsPerPage);
		$this->admin['page']	 = $this->page;
		
		$emails = DB::table('email')
					->select('id', 'identifier', 'status', 'permanent')
					->orderBy('identifier', 'asc')
					->skip($this->page->start)
					->take($this->page->itemsPerPage)
					->get();
		
		return $emails;
	}
	
	public function getCmsBlocks()
	{
		$this->page->pagesNumber = Helper::getAvailablePages('block', $this->page->itemsPerPage);
		$this->admin['page']	 = $this->page;
		
		$blocks = DB::table('block')
					->orderBy('identifier', 'asc')
					->skip($this->page->start)
					->take($this->page->itemsPerPage)
					->get();
		
		return $blocks;
	}
	
	public function getCmsPages()
	{	
	 	$this->page->pagesNumber = Helper::getAvailablePages('page', $this->page->itemsPerPage);
		$this->admin['page']	 = $this->page;
		
		$pages = DB::table('page')
			->orderBy('title', 'asc')
			->skip($this->page->start)
			->take($this->page->itemsPerPage)
			->get();
		
		return $pages;
	}
	
	public function saveEmail()
	{
		if(!Request::isMethod('post')) return;
		$params = Request::all();
		
		$isPermanent = $params['permanent'];
		$statusRules = $isPermanent ? '' : 'numeric|required';
		
		// Set validation rules.
		$idRules = $params['email_id'] ? 'numeric|required' : '';
		$rules = [
			'identifier'	 => ['required', 'max:255', 'regex:/^[a-zA-Z0-9_.-]*$/'],
			'status'		 => $statusRules,
			'email_id'		 => $idRules,
			'update'		 => 'numeric|required',
			'subject'		 => 'max:255',
		];
		
		// If the given identifier is a new one, make sure it doesn't exist.
		if(($isPermanent == false) && ($params['current_identifier'] != $params['identifier'])){
			$identifier = DB::table('email')
						->select('identifier')
						->where('identifier', $params['identifier'])
						->first(); 
			
			// If already exists throw an error message.	
			if($identifier){
				Helper::message('danger', ['The identifier: "' . $params['identifier'] . '" already exists.', 'There cannot be two identical identifiers.']);
				return redirect()->back();
			}
		}
		
		// Set error messages.
		$messages = [
			// 'id.regex' => 'The Identifier value may contain only letters, numbers, underscores and dashes.',
		];
		
	    $validator = Validator::make($params, $rules, $messages);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }
		
		$email = [
			'subject'	 => $params['subject'],
			'content'	 => $params['content'],
		];
		
		if(!$isPermanent){
			$email['identifier'] = $params['identifier'];
			$email['status'] 	 = $params['status'];
		}
		
		if($params['update']){
			try {
			
				DB::table('email')
						->where('id', $params['email_id'])
						->update($email);
						
				Helper::message('success', ['The email: "' . $params['identifier'] . '" has been updated successfully.']);
				return redirect()->back();
						
			}catch(\Exception $e){
				Helper::exception($e);
			}
			
		}else{
			try {
				$id = DB::table('email')->insertGetId($email);
				if($id){
					Helper::message('success', ['The email: "' . $params['identifier'] . '" has been created successfully.']);
					return redirect('/admin/cms/emails/editEmail?id=' . $id);
				}
						
			}catch(\Exception $e){
				Helper::exception($e);
			}
		}
	}
	
	public function saveBlock()
	{
		if(!Request::isMethod('post')) return;
		$params = Request::all();
		
		// Set validation rules.
		$idRules = $params['block_id'] ? 'numeric|required' : '';
		$rules = [
			'identifier'	 => ['required', 'max:255', 'regex:/^[a-zA-Z0-9_.-]*$/'],
			'status'		 => 'numeric|required',
			'block_id'		 => $idRules,
			'update'		 => 'numeric|required',
		];
		
		// If the given identifier is a new one, make sure it doesn't exist.
		if($params['current_identifier'] != $params['identifier']){
			$identifier = DB::table('block')
						->select('identifier')
						->where('identifier', $params['identifier'])
						->first(); 
			
			// If already exists throw an error message.	
			if($identifier){
				Helper::message('danger', ['The identifier: "' . $params['identifier'] . '" already exists.', 'There cannot be two identical identifiers.']);
				return redirect()->back();
			}
		}
		
		// Set error messages.
		$messages = [
			// 'id.regex' => 'The Identifier value may contain only letters, numbers, underscores and dashes.',
		];
		
	    $validator = Validator::make($params, $rules, $messages);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }
		
		$block = [
			'identifier' => $params['identifier'],
			'status'	 => $params['status'],
			'content'	 => $params['content'],
		];
		
		
		if($params['update']){
			try {
			
				DB::table('block')
						->where('id', $params['block_id'])
						->update($block);
						
				Helper::message('success', ['The block: "' . $block['identifier'] . '" has been updated successfully.']);
				return redirect()->back();
						
			}catch(\Exception $e){
				Helper::exception($e);
			}
			
		}else{
			try {
				$id = DB::table('block')->insertGetId($block);
				if($id){
					Helper::message('success', ['The block: "' . $block['identifier'] . '" has been created successfully.']);
					return redirect('/admin/cms/cms-blocks/editBlock?id=' . $id);
				}
						
			}catch(\Exception $e){
				Helper::exception($e);
			}
		}
	}
	
	public function addBlock()
	{

		$data = Helper::getAdminSection('cms', 'edit-block');
		$data['content_title'] 	= 'New Block';
		$data['block'] 			= new \stdClass;
		
		return view('admin.admin')->with('data', $data);
	}
	
	public function addEmail()
	{
		$data = Helper::getAdminSection('cms', 'edit-email');
		$data['content_title'] 	= 'New Email';
		
		$data['email'] = new \stdClass;
		
		return view('admin.admin')->with('data', $data);
	}
	
	public function editEmail()
	{
		$id = Request::input('id');
		$data = Helper::getAdminSection('cms', 'edit-email');
		
		$data['email'] = $this->getEmail($id);
		$data['content_title'] = 'Edit email: "' . $data['email']->identifier . '" (' . $data['email']->id . ')';
		
		return view('admin.admin')->with('data', $data);
	}
	
	public function editBlock()
	{
		$id = Request::input('id');
		$data = Helper::getAdminSection('cms', 'edit-block');
		
		$data['block'] = $this->getBlock($id);
		$data['content_title'] = 'Edit block: "' . $data['block']->identifier . '" (' . $data['block']->id . ')';
		
		return view('admin.admin')->with('data', $data);
	}
	
	private function getTokens($tokens)
	{
		if(!$tokens) return false;
		
		$tokensStr = '';
		$tokens = unserialize($tokens);
		foreach($tokens as $token)
		{
			$tokensStr .= '{{' . $token . '}},';
		}
		
		return rtrim($tokensStr, ',');
	}
	
	public function getEmail($id)
	{
		$email = DB::table('email')
					->where('id', $id)
					->first();
		
		if(!$email) return false;
		
		$email->tokens = $this->getTokens($email->tokens);
		
		return $email;
	}
	
	public function getBlock($id)
	{
		$block = DB::table('block')
					->where('id', $id)
					->first();
			
		return $block;
	}
	
	public function removeEmail()
	{
		$id = Request::input('id');
		$deleted = DB::table('email')
					->whereNotIn('permanent', [1])
					->where('id', $id)
					->delete();
		
		if($deleted){
			Helper::message('success', ['The item has been successfully removed.']);
		}
		return redirect('/admin/cms/emails');
	}
	
	public function removeBlock()
	{
		$id = Request::input('id');
		DB::table('block')->where('id', $id)->delete();
		
		Helper::message('success', ['The item has been successfully removed.']);
		return redirect('/admin/cms/cms-blocks');
	}
	
	public function loadPage($slug, $urlKey = false)
	{
		
		$listConf = unserialize(Session::get('configs')['list_config']);

		if($slug == $listConf['list_url_key'])
		{
			$action = $this->getAction($urlKey);
			$actionObj = new $action->model();
			$method = $action->method;
			
			$page = $actionObj->$method($listConf);

			return view($action->view)->with('page', $page);
		}
		
		$page = $this->getPage($slug);
		
		if($page){
			return view('pages.cms')->with('page', $page);
		}else{
			abort(404);
		}
	}
	
	private function getAction($urlKey)
	{
		$action = new \stdClass;
		
		if($urlKey){
			$action->model = 'App\Models\ItemModel';
			$action->method = 'loadItem';
			$action->view = 'pages.item';
		}else{
			$action->model = 'App\Models\ListModel';
			$action->method = 'loadListPage';
			$action->view = 'pages.list';
		}
		
		return $action;
	}	
	
	public function fetchEvents()
	{
		$events = DB::table('future_events')
				->where('checkout_timestamp', '>=', time())
				->get();
				
		// Arrange the events array.
		$apartmentsEvents = [];
		foreach($events as $event)
		{
			$apartmentsEvents[$event->item_id][] = [
					'checkin_timestamp' => $event->checkin_timestamp,
					'checkout_timestamp' => $event->checkout_timestamp,
				];
		}
		
		$apartmentsIds = array_keys($apartmentsEvents);
		$apartmentsObj = DB::table('item')
				->whereIn('id', $apartmentsIds) 
				->where('status', 1)
				->get();

				
		$apartmentsObjArranged = [];
		foreach($apartmentsObj as $apartment)
		{
			$apartmentsObjArranged[$apartment->id] = [
					'name' => $apartment->name,
				];
		}
		

		foreach($apartmentsEvents as $apartmentId => $events)
		{
			echo '<style>
				table{border-collapse: collapse;margin-bottom:50px;border: 3px solid #555555;}th,td{padding: 5px;border: 1px dashed black;}th{background-color: #ccc;}
			</style>';
			
			echo '<h3>Apartment: ' . $apartmentsObjArranged[$apartmentId]['name'] . ' (' . $apartmentId . ')</h3>';
			echo '<table>';
			echo '<tr style="border-bottom:2px solid #555555;"><th>Check In</th><th>Check Out</th><th>Number of nights</th></tr>';
			foreach($events as $event)
			{
				// Calculate number of staying nights.
				$datediff = $event['checkout_timestamp'] - $event['checkin_timestamp'];
				$nightsNumber = floor($datediff/(60*60*24));
				
				echo '<tr>';
				echo '<td>' . date('d/m/Y', $event['checkin_timestamp']) . '</td>';
				echo '<td>' . date('d/m/Y', $event['checkout_timestamp']) . '</td>';
				echo '<td>' . $nightsNumber . '</td>';
				echo '</tr>';
			}
			echo '</table>';
		}
		die();
	}
	
	private function getPage($slug){
		
		$page = DB::table('page')->where('status', '1')->where('slug', $slug)->first();
		
		return $page;
	}
	
	public function removePage()
	{
		$pageId = Request::input('id');

		if($pageId){
			
			$removedItems = DB::table('page')
					->where('id', $pageId)
					->whereNotIn('slug',['404'])
					->delete();
					
			if($removedItems){
				Helper::message('success', ['The page has been successfully removed.']);
			}
		}

		return redirect('/admin/cms/cms-pages');
	}
	
	public function edit(){
		
		$pageId = Request::input('id');
		$page = DB::table('page')->where('id', $pageId)->first();
		
		$data = Helper::getAdminSection('cms', 'page');
		$data['content_title'] = 'Edit Page: ' . $page->title . ' (' . $page->id . ')';
		$data['content_data'] = $page;
		
		return view('admin.admin')->with('data', $data);
	}
	
	public function savePage(){
		
		if(!Request::isMethod('post')) return;

		$params = Request::all();

		// Set validation rules.
		$rules = [
			'slug'		=> ['required', 'max:255', 'regex:/^[a-zA-Z0-9_.-]*$/'],
			'status' 	=> 'required',
			'title'		=> 'max:255',
		];

		// Set error messages.
		$messages = [
			'slug.regex' => 'The URL Key value may contain only letters, numbers, underscores and dashes.',
		];
		
	    $validator = Validator::make($params, $rules, $messages);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					// ->withErrors($validator)
					->withInput();
        }

		// If the given slug (URL key) is a new one, make sure it doesn't exist.
		if($params['current_slug'] != $params['slug']){
			$slug = DB::table('page')
						->select('id')
						->where('slug', $params['slug'])
						->first(); 
			
			// If already exists throw an error message.	
			if($slug){
				Helper::message('danger', ['The URL Key: "' . $params['slug'] . '" already exists.', 'There cannot be two identical URL Keys.']);
				return redirect()->back();
			}
		}
		
		$page = [
			'slug' 			=> $params['slug'],
			'title' 		=> $params['title'],
			'page_content' 	=> $params['page_content'],
			'status' 		=> $params['status'],
		];
		
		if($params['update']){
			if($params['slug'] != '404'){
				// Remove current page & define the id value of the current page in the page array.
				$page['id'] = $params['page_id'];
				DB::table('page')
					->where('id', $params['page_id'])
					->whereNotIn('slug',['404'])
					->delete();
			}else{
				DB::table('page')
					->where('slug', '404')
					->update(['title' => $params['title'], 'page_content' => $params['page_content']]);
			}
			Helper::message('success', ['The page "' . $page['slug'] . '" has been successfully saved.']);
			
		}else{
			Helper::message('success', ['New page: "' . $page['slug'] . '" has been successfully created.']);
		}
		
		if($params['slug'] != '404'){
			DB::table('page')->insert($page);
		}
		
		return redirect('/admin/cms/cms-pages');
		
	}
	
	public function add()
	{	
		$page = new \stdClass();
		
		$data = Helper::getAdminSection('cms', 'page');
		$data['content_title'] = 'New Page:';
		$data['content_data'] = $page;
		
		return view('admin.admin')->with('data', $data);
	}
	
	public function browse()
	{
		$assetsDir = base_path() . '/resources/assets/';
		$imagesDir = $assetsDir . 'img/admin/editor';
		$editorImagesPath = URL::to('local/resources/assets/img/admin/editor');

		// Create recursive dir iterator which skips dot folders
		$dir = new RecursiveDirectoryIterator($imagesDir,
			RecursiveDirectoryIterator::SKIP_DOTS);

		// Flatten the recursive iterator, folders come before their files
		$it  = new RecursiveIteratorIterator($dir,
			RecursiveIteratorIterator::SELF_FIRST);

		// Maximum depth is 1 level deeper than the base folder
		$it->setMaxDepth(1);


		$imagesArr = [];
		$extensions = array("png", "jpg", "jpeg", "gif");
		foreach ($it as $fileinfo) {
			if (in_array($fileinfo->getExtension(), $extensions)) {
				$currentFolder = $it->getSubPath() ? $it->getSubPath() : 'Main';
				
				$subDir = ($currentFolder == 'Main') ? '/' : '/' . $currentFolder . '/';
				$imagesArr[] = [
					"image"	 => $editorImagesPath . $subDir . $fileinfo->getFilename(),
					"folder" => $currentFolder
				];
			}
		}
		
		return response()->json($imagesArr);
	}
	
	public function upload() 
	{
		$assetsDir = base_path() . '/resources/assets/';
		$imagesDir = $assetsDir . 'img/admin/editor';
		
		// Getting the uploaded file.
		$file = array('image' => Input::file('upload'));
		
		// Setting up rules.
		$rules = array('upload' => 'image');
		
		// Doing the validation, passing post data, rules and the messages.
		$validator = Validator::make($file, $rules);
		if ($validator->fails()) {
			$message = 'Error - validation failure.';
		}
		else {
			// Checking if the file is valid.
			if (Input::file('upload')->isValid()) {
				// $extension = Input::file('upload')->getClientOriginalExtension(); // getting image extension
				$fileName = Input::file('upload')->getClientOriginalName();
				Input::file('upload')->move($imagesDir, $fileName); // uploading file to given path
				
				$message = 'The image has been uploaded successfully';
			}
			else {
				$message = 'Uploaded file is not valid';
			}
		}
		
		$data = [
			'message' => $message,
		];
		
		return view('admin.content.cms.upload')->with('data', $data);
	}

}