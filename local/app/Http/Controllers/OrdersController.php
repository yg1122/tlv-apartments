<?php 

namespace App\Http\Controllers;

use DB;
use URL;
use Mail;
use Helper;
use Request;
use Session;
use Validator;
use App\Models\ItemModel;
use App\Models\ListModel;
use App\Models\CheckoutModel;
use App\Http\Controllers\Controller;


class OrdersController extends Controller {
	
	private $admin;
	private $page;
	
	public function build($section = false, $content = false)
	{
		$this->admin 	= Helper::getAdminSection($section, $content);
		$this->page 	= $this->admin['page'];
		
		$sectionData  	= $this->getSectionData($content);
		$data = array_merge($this->admin, $sectionData);
		
		return view('admin.admin')->with('data', $data);
		
	}
	
	private function getSectionData($content)
	{
		switch ($content) {
			case 'manage':
				$data = $this->getManageOrders(); 
				break;
			case 'manage-requests':
				$data = $this->getManageRequests(); 
				break;
			default:
				$data = $this->getManageOrders(); 
		}
		
		return $data;
	}
	
	private function getManageRequests()
	{
		$this->page->pagesNumber = Helper::getAvailablePages('request', $this->page->itemsPerPage);
		
		return [
			'content_title' => 'Manage Requests',
			'requests'		=> $this->getRequestsTable(),
			'page'			=> $this->page,
		];
	}
	
	private function getManageOrders()
	{
		$this->page->pagesNumber = Helper::getAvailablePages('order', $this->page->itemsPerPage);
		
		return [
			'content_title' => 'Manage Orders',
			'orders'		=> $this->getOrdersTable(),
			'page'			=> $this->page,
		];
	}
	
	private function getRequestsTable()
	{
		$select = [
			'request.id',
			'request.first_name',
			'request.last_name',
			'request.checkin',
			'request.checkout',
			'request.item_id',
			'request.date',
			'request.status',
			'item.name',
		];
		
		$requests = DB::table('request')
            ->join('item', 'item.id', '=', 'request.item_id')
            ->select($select)
			->orderBy('date', 'desc')
			->skip($this->page->start)
			->take($this->page->itemsPerPage)
            ->get();
		
		return $requests;
	}
	
	private function getOrdersTable()
	{
		$select = [
			'order.id',
			'order.first_name',
			'order.last_name',
			'order.checkin',
			'order.checkout',
			'order.guests_number',
			'order.item_id',
			'order.date',
			'item.name',
		];
		
		$orders = DB::table('order')
            ->join('item', 'item.id', '=', 'order.item_id')
            ->select($select)
			->orderBy('date', 'desc')
			->skip($this->page->start)
			->take($this->page->itemsPerPage)
            ->get();
		
		return $orders;
	}
	
	public function saveRequest()
	{
		if(!Request::isMethod('post')) return;
		$params = Request::all();

		
		// Set validation rules.
		$rules = [
			'first_name'	 	=> 'required|max:255',
			'last_name'	 	 	=> 'required|max:255',
			'checkin' 		 	=> 'required',
			'checkout'		 	=> 'required',
			'guests_number'  	=> 'required|numeric',
			'email'			 	=> 'required|email|max:255',
			'item_id'		 	=> 'required|numeric',
		];
		
	    $validator = Validator::make($params, $rules);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }
	
		// Check if the apartment is available at the requested dates.
		$itemModel = new ItemModel();
		$isAvailable = $itemModel->isAvailable($params['checkin'], $params['checkout'], true, $params['item_id'], false);
		if(!$isAvailable){
			Helper::message('danger', ['The apartment is not available at the following dates: ' . $params['checkin'] . ' - ' . $params['checkout']]);
			return redirect()->back();
		}
		
		$item = DB::table('item')
						->select('city')
						->where('id', $params['item_id'])
						->first();
						
		$request = [
			'first_name' 	=> $params['first_name'],
			'last_name'  	=> $params['last_name'],
			'email' 	 	=> $params['email'],
			'checkin' 		=> $params['checkin'],
			'checkout'		=> $params['checkout'],
			'guests_number' => $params['guests_number'],
			'city'			=> $item->city,
			'requests' 		=> $params['requests'],
			'item_id'		=> $params['item_id'],
			'uniq_id'		=> md5(uniqid(rand(), true)),
			'date'			=> date('Y-m-d H:i:s'),
			'status'		=> 'Pending',
		];
		
		try {
			// Insert the booking request to the request table.
			$id = DB::table('request')->insertGetId($request);
			
			if($id){
				Helper::message('success', ['Request number: "' . $id . '" has been created successfully.']);
				return redirect('/admin/orders/show-request/showRequest?id=' . $id);
			}
					
		}catch(\Exception $e){
			Helper::exception($e);
		}
	}
	
	public function saveOrder()
	{
		if(!Request::isMethod('post')) return;
		$params = Request::all();

		
		// Set validation rules.
		$idRules = $params['update'] ? ['required', 'numeric'] : [];
		$rules = [
			'order_id'		 	=> $idRules,
			'first_name'	 	=> 'required|max:255',
			'last_name'	 	 	=> 'required|max:255',
			'checkin' 		 	=> 'required',
			'checkout'		 	=> 'required',
			'guests_number'  	=> 'required|numeric',
			'email'			 	=> 'required|email|max:255',
			'item_id'		 	=> 'required|numeric',
			'phone'				=> 'max:255',
		];
		
	    $validator = Validator::make($params, $rules);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }
	
		// If the order dates have changed, check if the apartment is available. 
		if(($params['update'] == false) || ($params['item_id'] != $params['current_item_id']) || ($params['checkin'] != $params['current_checkin']) || ($params['checkout'] != $params['current_checkout'])){
			$itemModel = new ItemModel();
			$isAvailable = $itemModel->isAvailable($params['checkin'], $params['checkout'], true, $params['item_id'], false);
			if(!$isAvailable){
				Helper::message('danger', ['The apartment is not available at the following dates: ' . $params['checkin'] . ' - ' . $params['checkout']]);
				return redirect()->back();
			}
		}
		
		// If the booking dates or item_id has changed, remove the old order event & create a new one.
		if($params['update']){
			if(($params['item_id'] != $params['current_item_id']) || ($params['checkin'] != $params['current_checkin']) || ($params['checkout'] != $params['current_checkout'])){
			
				$eventsWhere = [
					"checkin_timestamp" 	=> Helper::dateToTimestamp($params['current_checkin']),
					"checkout_timestamp" 	=> Helper::dateToTimestamp($params['current_checkout']),
					"item_id" 				=> $params["current_item_id"],
				];
				
				$eventsTables = ["events", "local_events", "future_events"];
				foreach($eventsTables as $table)
				{
					DB::table($table)->where($eventsWhere)->delete();
					DB::table($table)->insert($eventsWhere);
				}
			}
		}
		
		$order = [
			'first_name' 	=> $params['first_name'],
			'last_name'  	=> $params['last_name'],
			'phone' 	 	=> $params['phone'],
			'email' 	 	=> $params['email'],
			'checkin' 		=> $params['checkin'],
			'checkout'		=> $params['checkout'],
			'guests_number' => $params['guests_number'],
			'requests' 		=> $params['requests'],
			'item_id'		=> $params['item_id'],
		];
		
		if($params['update']){
			try {
			
				DB::table('order')
						->where('id', $params['order_id'])
						->update($order);
						
				Helper::message('success', ['The order: "' . $params['order_id'] . '" has been updated successfully.']);
				return redirect()->back();
						
			}catch(\Exception $e){
				Helper::exception($e);
			}
			
		}else{
			$order['date'] = date('Y-m-d H:i:s');
			
			try {
				$id = DB::table('order')->insertGetId($order);
				if($id){
					
					$event = [
						'checkin_timestamp' 	=> Helper::dateToTimestamp($params['checkin']),
						'checkout_timestamp'	=> Helper::dateToTimestamp($params['checkout']),
						'item_id'				=> $params['item_id'],
					];

					$eventTables = ['local_events', 'events'];
					if($event['checkout_timestamp'] >= time()){
						$eventTables[] = 'future_events';
					}
		
					// Create the order events.
					foreach($eventTables as $table){
						DB::table($table)->insert($event);
					}
					
					Helper::message('success', ['The order: "' . $id . '" has been created successfully.']);
					return redirect('/admin/orders/edit/editOrder?id=' . $id);
				}
						
			}catch(\Exception $e){
				Helper::exception($e);
			}
		}
		
	}
	
	public function createOrder()
	{
		$listConf = unserialize(Session::get('configs')['list_config']);
		$listModel = new ListModel();
		
		$data = Helper::getAdminSection('orders', 'edit');
		$data['content_title'] = 'New Order:';
		
		$data['order'] = new \stdClass;
		$data['order']->items = $this->getItems();
		$data['order']->item_id = false;
		$data['order']->minDate = $listModel->getMinDate($listConf);
		$data['order']->disabledDates = $listConf['list_disabled_dates'];
		
		return view('admin.admin')->with('data', $data);
	}
	
	public function editOrder()
	{
		$id = Request::input('id');
		$data = Helper::getAdminSection('orders', 'edit');
		
		$data['order'] = $this->getOrder($id);
		$data['content_title'] = 'Edit order: "' . $data['order']->id . '"';
		
		return view('admin.admin')->with('data', $data);
	}
	
	public function createRequest()
	{
		$listConf = unserialize(Session::get('configs')['list_config']);
		$listModel = new ListModel();
		
		$data = Helper::getAdminSection('orders', 'create-request');
		$data['content_title'] = 'New Request:';
		
		$data['request'] = new \stdClass;
		$data['request']->items = $this->getItems();
		$data['request']->minDate = $listModel->getMinDate($listConf);
		$data['request']->disabledDates = $listConf['list_disabled_dates'];
		
		return view('admin.admin')->with('data', $data);
	}
	
	public function showRequest()
	{
		$id = Request::input('id');
		$data = Helper::getAdminSection('orders', 'request');
		
		$data['request'] = $this->getRequest($id);
		$data['content_title'] = 'Edit request: "' . $data['request']->id . '"';
		
		return view('admin.admin')->with('data', $data);
	}
	
	private function getRequest($id)
	{
		$request = DB::table('request')
					->where('id', $id)
					->first();
		
		$request->property = $this->getRequestItem($request->item_id);
		$request->approval_url = $this->generateApprovalLink($request->id, $request->uniq_id, $request->item_id);
		
		return $request;
	}
	
	private function getRequestItem($itemId)
	{
		$item = DB::table('item')
					->select('name')
					->where('id', $itemId)
					->first();
						
		return $item->name;
	}
	
	private function getOrder($id)
	{
		$listConf = unserialize(Session::get('configs')['list_config']);
		$listModel = new ListModel();
		
		$order = DB::table('order')
			->where('id', $id)
			->first();
		
		$order->items = $this->getItems();
		$order->minDate = $listModel->getMinDate($listConf);
		$order->disabledDates = $listConf['list_disabled_dates'];
		
		return $order;
	}
	
	private function getItems()
	{
		$items = DB::table("item")
					->select('id', 'name', 'status')
					->orderBy('name', 'asc')
					->get();
		
		return $items;
	}
	
	public function removeOrder()
	{
		$params = Request::all();
		
		// Fetch the necessary order data before removing it.
		$order = DB::table('order')
						->select('checkin', 'checkout', 'item_id')
						->where('id', $params['order_id'])
						->first();
		
		// Remove the order.
		DB::table('order')->where('id', $params['order_id'])->delete();
		
		$whereConditions = [
			"checkin_timestamp" 	=> Helper::dateToTimestamp($order->checkin),
			"checkout_timestamp" 	=> Helper::dateToTimestamp($order->checkout),
			"item_id" 				=> $order->item_id,
		];
		
		$eventTables = ['local_events', 'events', 'future_events'];
		
		// Remove the order event data.
		foreach($eventTables as $table){
			DB::table($table)->where($whereConditions)->delete();	
		}
		
		Helper::message('success', ['The order has been successfully removed.']);
		return redirect('/admin/orders/manage');
	}
	
	public function removeRequest()
	{
		$params = Request::all();
		DB::table('request')->where('id', $params['request_id'])->delete();
		
		Helper::message('success', ['The request has been successfully removed.']);
		return redirect('/admin/orders/manage-requests');
	}
	
	public function sendOrderMail()
	{
		// TODO - turn this to post instead of get request.
		$id = Request::input('id');
		
		$orderObj = DB::table('order')
					->where('id', $id)
					->first();
		
		$orderItem = DB::table('item')
						->select('name', 'country', 'city', 'street', 'house_number', 'apartment_number', 'cleaning_fee')
						->where('id', $orderObj->item_id)
						->first();
						
		$tokens = [
			'order_id'			=> $id,
			'first_name' 		=> $orderObj->first_name,
			'last_name' 		=> $orderObj->last_name,
			'phone'				=> $orderObj->phone,
			'email'				=> $orderObj->email,
			'requests' 			=> nl2br($orderObj->requests),
			'checkin' 			=> $orderObj->checkin,
			'checkout' 			=> $orderObj->checkout,
			'nights_number' 	=> Helper::calcNights($orderObj->checkin, $orderObj->checkout),
			'guests_number' 	=> $orderObj->guests_number,
			'price_per_night'	=> $orderObj->price_per_night,
			'total_price'		=> $orderObj->total_price,
			'subtotal'			=> $orderObj->subtotal,
			'vat'				=> Helper::getVat(),
			'vat_value'			=> $orderObj->total_price - $orderObj->subtotal,
			
			// Item properties.
			'property_name' 	=> $orderItem->name,
			'country'			=> $orderItem->country,
			'city'				=> $orderItem->city,
			'street'			=> $orderItem->street,
			'house_number'		=> $orderItem->house_number,
			'apartment_number'	=> $orderItem->apartment_number,
			'cleaning_fee'		=> $orderItem->cleaning_fee,
		];		
		

		$email 	   = Helper::email('order');
		$emailData = [
			'from_address' 	=> Session::get('configs')['email_address'],
			'from_name'		=> Session::get('configs')['email_name'],
			'to_address'	=> $orderObj->email,
			'subject'		=> $email->subject,
		];
		
		$data = [
			'content' => $email->content,
			'tokens'  => $tokens,
		];
		
		// Send the order email.
		Helper::sendCmsMail($data, $emailData);
		
		Helper::message('success', ['An email for the order: "' . $orderObj->id . '" has been sent successfully.']);
		return redirect()->back();
	}
	
	public function updateRequest()
	{
		if(!Request::isMethod('post')) return;
		
		$params = Request::all();
		
		// Set validation rules.
		$rules = [
			'id'			=> 'required|numeric',
			'update_status' => 'required|in:1,2,3,4',
		];
		
	    $validator = Validator::make($params, $rules);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }

		$autoRedirect = false;
		if(isset($params['paypal_auto_redirect']) && ($params['paypal_auto_redirect'] == 'on')){
			$autoRedirect = true;
		}

		switch ($params['update_status']) 
		{
			// Approve & Send approval email.
			case '1':
				$this->updateRequestStatus('Approved', $params['id']);
				$this->sendRequestMail($params['id'], true, $autoRedirect);
				$successMessages = ['The request\'s status has been updated successfully.', 'An approval email has been sent to the customer.'];
				break;
				
			// Approve & Don't send approval email.
			case '2':
				$this->updateRequestStatus('Approved', $params['id']);
				$successMessages = ['The request\'s status has been updated successfully.'];
				break;
				
			// Reject & Send rejection email.
			case '3':
				$this->updateRequestStatus('Rejected', $params['id']);
				$this->sendRequestMail($params['id'], false);
				$successMessages = ['The request\'s status has been updated successfully.', 'A rejection email has been sent to the customer.'];
				break;
				
			// Reject & Don't send rejection email.
			case '4':
				$this->updateRequestStatus('Rejected' ,$params['id']);
				$successMessages = ['The request\'s status has been updated successfully.'];
				break;
		}
		
		Helper::message('success', $successMessages);
		return redirect()->back();
	}
	
	private function updateRequestStatus($status, $requestId)
	{
		DB::table('request')
			->where('id', $requestId)
			->update(['status' => $status]);
	}
	
	public function sendRequestMail($requestId, $approved, $autoRedirect = false)
	{

		$request = DB::table('request')
					->where('id', $requestId)
					->first();
		
		$item = DB::table('item')
					->select('name')
					->where('id', $request->item_id)
					->first();
		
		// Add a property_name property to the request object.
		$request->property_name = $item->name;
		
		$emailToUser = [
			'from_address' 	=> Session::get('configs')['booking_requests_email'],
			'from_name'		=> Session::get('configs')['email_name'],
			'to_address'	=> $request->email,
		];
		
		if($approved)
		{
			$emailIdentifier = 'request-approval';
			
			// Generate the approval URL.
			$request->approval_url = $this->generateApprovalLink($request->id, $request->uniq_id, $request->item_id, $autoRedirect);
		
		}else
		{
			$emailIdentifier = 'request-rejection';
		}

		// Apply nl2br on the 'requests' property before sending the email.
		$request->requests = nl2br($request->requests);
		
		$email = Helper::email($emailIdentifier);
		$mailData = [
			'content' => $email->content,
			'tokens'  => $request,	
		];
		
		$emailToUser['subject'] = $email->subject;
		
		// Send success email to the customer.
		Helper::sendCmsMail($mailData, $emailToUser);

	}
	
	private function generateApprovalLink($requestId, $requestUniqId, $requestItemId, $autoRedirect = false)
	{
		$q = $requestId . ',' . $requestUniqId . ',' . $requestItemId;
		$approvalUrl = URL::to("checkout?q=" . urlencode($q));
		
		if($autoRedirect){
			$approvalUrl .= '&auto_redirect=1';
		}
		
		return $approvalUrl;
	}

}