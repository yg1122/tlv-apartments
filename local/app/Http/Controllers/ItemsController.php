<?php 

namespace App\Http\Controllers;

use DB;
use Input;
use Image;
use Helper;
use Request;
use Validator;
use App\Models\ItemModel;
use App\Http\Controllers\Controller;


class ItemsController extends Controller {
	
	private $admin;
	private $page;
	
	public function build($section = false, $content = false)
	{
		$this->admin 	= Helper::getAdminSection($section, $content);
		$this->page 	= $this->admin['page'];
		
		$this->page->pagesNumber = Helper::getAvailablePages('item', $this->page->itemsPerPage);
		$data['page']			 = $this->page;
		
		$data['items'] = $this->getItemsTable();
		$data = array_merge($this->admin, $data);
		
		return view('admin.admin')->with('data', $data);
		
	}
	
	private function getItemsTable()
	{
		$items = DB::table('item')
					->select('id', 'name', 'status', 'url_key')
					->orderBy('name', 'asc')
					->skip($this->page->start)
					->take($this->page->itemsPerPage)
					->get();
		
		return $items;
	}
	
	private function getListImage($params, $destination, $resizedDir)
	{
		$listImageName = Image::upload($params, $destination, 'list_image'); 
		$imageName     = isset($listImageName['images'][0]) ? $listImageName['images'][0] : $params['current_list_image'][0];
		
		// Resize the image if needed.
		$resizeWidth  = isset($params['resize_width'][0]) ? $params['resize_width'][0] : false;
		$resizeHeight = isset($params['resize_height'][0]) ? $params['resize_height'][0] : false;
		
		// Resize if needed.
		if($resizeWidth || $resizeHeight){
			// Validate the resize values, resize values should contain only positive integers.
			if((($resizeHeight === '') && (Helper::isNumber($resizeWidth))) || (($resizeWidth === '') && (Helper::isNumber($resizeHeight))) || ((Helper::isNumber($resizeWidth)) && (Helper::isNumber($resizeHeight)))){
				$resizedImage = str_replace($destination . '/', "", Image::resize($destination . '/' . $imageName, $resizedDir, $resizeWidth, $resizeHeight));
			}else{
				return ['error' => ['Resize values should contain only positive integers.']];
			}
			
		}else{
			$resizedImage = '';
		}
				
		$listImage = [
			'title' 	 	=> $params['title'][0],
			'alt' 		 	=> $params['alt'][0],
			'url' 	 	 	=> $params['url'][0],
			'target' 	 	=> isset($params['target'][0]) ? $params['target'][0] : 0,
			'position' 	 	=> $params['position'][0],
			'name' 		 	=> $imageName,
			'resized_image' => $resizedImage,
			'resize_width' 	=> $resizeWidth,
			'resize_height' => $resizeHeight,
			'input_name'	=> 'list_image',
			'field_name'	=> 'List Image',
		];
		
		return $listImage;
	}
	
	public function saveItem()
	{
		if(!Request::isMethod('post')) return;
		$params = Request::all();

		
		// Set validation rules.
		$idRules = $params['update'] ? ['required', 'max:255', 'regex:/^[a-zA-Z0-9_.-]*$/'] : [];
		$rules = [
			'id'			 => $idRules,
			'url_key'		 => ['required', 'max:255', 'regex:/^[a-zA-Z0-9_.-]*$/'],
			'name'	 		 => ['required', 'max:255'],
			'ical_url' 		 => 'url',
			'price'			 => 'numeric|required',
			'max_guests' 	 => 'numeric|required',
			'bedrooms'		 => 'numeric|required',
			'beds'			 => 'numeric|required',
			'bathrooms'		 => 'numeric|required',
			'cleaning_fee'	 => 'numeric|required',
			'size'			 => 'numeric|required',
			'min_nights'	 => 'numeric',
			'cancellation'	 => 'required',
			'bed_type'		 => 'required',
			'property_type'	 => 'required',
			'checkin_time'	 => 'required|max:255',
			'checkout_time'	 => 'required|max:255',
			'country' 		 => 'required|max:255',
			'city'			 => 'required|max:255',
			'street'		 => 'required|max:255',
			'house_number' 	 => 'required',
		];
		
		// Set error messages.
		$messages = [
			// 'id.regex' => 'The Identifier value may contain only letters, numbers, underscores and dashes.',
		];
		
	    $validator = Validator::make($params, $rules, $messages);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }
		
		
		$destination = base_path() . '/resources/assets/img/product';
		$resizedDir = $destination . '/resized';
		
		// Upload list image
		$listImage = $this->getListImage($params, $destination, $resizedDir);	
		if(isset($listImage['error']))
		{
			Helper::message('danger', $listImage['error']);
			return redirect()->back();
		}
		
		// Upload images.
		$images = Image::upload($params, $destination);

		$banners = [];
		$keys = array_keys($params['title']);
		
		// Remove the list_image position from the array.
		array_shift($keys);
		foreach($keys as $keyValue)
		{
			if(isset($images['images'][$keyValue]) || $params['current_image'][$keyValue]){
				// The original image (not the resized).
				$imageName = isset($images['images'][$keyValue]) ? $images['images'][$keyValue] : $params['current_image'][$keyValue];
				
				$resizeWidth  = isset($params['resize_width'][$keyValue]) ? $params['resize_width'][$keyValue] : false;
				$resizeHeight = isset($params['resize_height'][$keyValue]) ? $params['resize_height'][$keyValue] : false;
				
				// Resize if needed.
				if($resizeWidth || $resizeHeight){
					// Validate the resize values, resize values should contain only positive integers.
					if((($resizeHeight === '') && (Helper::isNumber($resizeWidth))) || (($resizeWidth === '') && (Helper::isNumber($resizeHeight))) || ((Helper::isNumber($resizeWidth)) && (Helper::isNumber($resizeHeight)))){
						$resizedImage = str_replace($destination . '/', "", Image::resize($destination . '/' . $imageName, $resizedDir, $resizeWidth, $resizeHeight));
					}else{
						Helper::message('danger', ['Resize values should contain only positive integers.']);
						return redirect()->back();
					}
					
				}else{
					$resizedImage = '';
				}
				
				$banners[$keyValue] = [
					'title' 	 	=> $params['title'][$keyValue],
					'alt' 		 	=> $params['alt'][$keyValue],
					'url' 	 	 	=> $params['url'][$keyValue],
					'target' 	 	=> isset($params['target'][$keyValue]) ? $params['target'][$keyValue] : 0,
					'position' 	 	=> $params['position'][$keyValue],
					'name' 		 	=> $imageName,
					'resized_image' => $resizedImage,
					'resize_width' 	=> $resizeWidth,
					'resize_height' => $resizeHeight,
				];
			}
		}
		
		$banners = serialize($banners);
		$amenities = $this->prepareAmenities($params);
		
		// Get Google map coordinates.
		$coordinates = Helper::getCoordinates(trim($params['street']) . ' ' . trim($params['house_number']) . ' ' . trim($params['city']) . ' ' . trim($params['country']));

		$item = [
			'url_key' 				=> $params['url_key'],
			'status'				=> (isset($params['status']) && $params['status']) ? 1 : 0,
			'name' 					=> $params['name'],
			'price'					=> $params['price'],
			'short_description' 	=> $params['short_description'],
			'description' 			=> $params['description'],
			'max_guests' 			=> $params['max_guests'],
			'airbnb_ics' 			=> $params['ical_url'],
			'country' 				=> $params['country'],
			'city' 					=> $params['city'],
			'street' 				=> $params['street'],
			'house_number' 			=> $params['house_number'],
			'apartment_number' 		=> $params['apartment_number'],
			'list_image'			=> serialize($listImage),
			'images'				=> $banners,
			'about'					=> $params['about'],
			'bed_type'				=> $params['bed_type'],
			'property_type'			=> $params['property_type'],
			'bedrooms'				=> $params['bedrooms'],
			'beds'					=> $params['beds'],
			'bathrooms'				=> $params['bathrooms'],
			'cleaning_fee'			=> $params['cleaning_fee'],
			'cancellation'			=> $params['cancellation'],
			'checkin_time'			=> $params['checkin_time'],
			'checkout_time'			=> $params['checkout_time'],
			'rules'					=> $params['rules'],
			'min_nights'			=> $params['min_nights'],
			'size'					=> $params['size'],
			'amenities'				=> $amenities,
			'coordinates'			=> $coordinates['lat'] . ',' . $coordinates['lng'],
		];
	
		if($params['update']){
			try {
			
				DB::table('item')
						->where('id', $params['id'])
						->update($item);
						
				Helper::message('success', ['The item: "' . $item['name'] . '" has been updated successfully.']);
				return redirect()->back();
						
			}catch(\Exception $e){
				Helper::exception($e);
			}
			
		}else{
			try {
				// Generate d_key for new products.
				$item['d_key'] = Helper::generateDownloadKey();
				
				$id = DB::table('item')->insertGetId($item);
				if($id){
					Helper::message('success', ['The item: "' . $item['name'] . '" has been created successfully.']);
					return redirect('/admin/items/manage/editItem?id=' . $id);
				}
						
			}catch(\Exception $e){
				Helper::exception($e);
			}
		}
		
	}
	
	private function prepareAmenities($params)
	{
		$amenities = isset($params['amenities']) ? $params['amenities'] : []; 
		
		$itemModel = new ItemModel();
		$allAmenities = $itemModel->getAmenities();
		
		// Arrange & serialize amenities data.
		$amenitiesArr = [];
		foreach($allAmenities as $key => $value)
		{
			if(isset($amenities[$key])){
				$amenitiesArr[$value] = 1;
			}else{
				$amenitiesArr[$value] = 0; 
			}
		}

		return serialize($amenitiesArr);
	}
	
	public function addItem()
	{
		$data = Helper::getAdminSection('items', 'edit-item');
		$data['content_title'] = 'New item';
		
		$data['item'] = new \stdClass;
		$images[] = [];
		$data['item']->images = $images;
		
		$listImage = [
			'title' 	 	=> '',
			'alt' 		 	=> '',
			'url' 	 	 	=> '',
			'target' 	 	=> 0,
			'position' 	 	=> '',
			'name' 		 	=> '',
			'resized_image' => '',
			'resize_width' 	=> '',
			'resize_height' => '',
			'input_name'	=> 'list_image',
			'field_name'	=> 'List Image',
		];
		
		// Add the list image to the images array.
		array_unshift($data['item']->images, $listImage);
		
		$itemModel = new ItemModel();
		$data['item']->amenitiesArr = $itemModel->getAmenities();
		$data['item']->update = false;
		
		return view('admin.admin')->with('data', $data);
	}
	
	public function editItem()
	{
		$id = Request::input('id');
		
		$data = Helper::getAdminSection('items', 'edit-item');
		$data['item'] = $this->getItem($id);
		$data['content_title'] = 'Edit item: "' . $data['item']->name . '" (' . $data['item']->id . ')';
		
		
		$itemModel = new ItemModel();
		$images = $itemModel->getItemImages($data['item']);
		$images[] = [];
		$data['item']->images = $images;

		// Add the list image to the images array.
		array_unshift($data['item']->images, unserialize($data['item']->list_image));

		$data['item']->amenitiesArr = $itemModel->getAmenities();
		$data['item']->amenities = unserialize($data['item']->amenities);
		$data['item']->coordinates = explode(",", $data['item']->coordinates);
		$data['item']->update = true;
		
		return view('admin.admin')->with('data', $data);
	}
	
	private function getItem($id)
	{
		$item = DB::table('item')
			->where('id', $id)
			->first();
			
		return $item;
	}
	
	public function removeItem()
	{
		$id = Request::input('id');
		DB::table('item')->where('id', $id)->delete();
		
		Helper::message('success', ['The item has been successfully removed.']);
		return redirect('/admin/items/manage');
	}
	
	public function getIcs()
	{
		if(!Request::isMethod('get')) return;
		
		$itemId 	 = Request::input('p');
		$downloadKey = Request::input('d');
		
		// Check if item exists in DB & fetch the download key.
		$item = DB::table('item')
				->select('id')
				->where(['id' => $itemId, 'd_key' => $downloadKey])
				->first();
		
		if(!$item) return;
		
		$localDir = base_path() . '/app/Libraries/ics-parser-master/ics/local/';
		$pathToFile = $localDir . 'local_' . $item->id . '.ics';
		
		if(file_exists($pathToFile)){
			return response()->download($pathToFile);
		}
	}

}