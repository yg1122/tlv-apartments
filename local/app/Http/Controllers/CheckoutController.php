<?php 

namespace App\Http\Controllers;

use DB;
use Mail;
use Cookie;
use Helper;
use Session;
use Request;
use Validator;
use App\Models\ItemModel;
use App\Http\Controllers\Controller;

class CheckoutController extends Controller {
	
	public function build()
	{	
		$params = Request::all();
		$requestBookingMode = (int)Session::get('configs')['request_booking'];
		$paypalAutoRedirect = false;
		
		// In case of booking request mode, check if approval request exists.
		if($requestBookingMode){
			$approved = $this->handleRequestApproval();
			
 			if($approved === false)
			{
				return redirect()->back();
			}
			
			// If success message has been thrown.
			if($approved !== true)
			{
				// If the request is approved & Paypal auto redirect mode is enabled & a 'q' parameter exists, auto redirect to Paypal.
				$paypalAutoRedirectMode = (int)unserialize(Session::get('configs')['paypal'])['auto_redirect'];
				$autoRedirectParam = isset($params['auto_redirect']) ? $params['auto_redirect'] : false;
				if($paypalAutoRedirectMode || ($autoRedirectParam == '1'))
				{
					$paypalAutoRedirect = true;
				}
				
				if(!$paypalAutoRedirect)
				{
					return redirect('checkout');	
				}
			}
		}

		$reservation = Session::get('reservation');
		$itemModel = new ItemModel();
		
		if(!$reservation){
			Helper::message('danger', ['The checkout page is not available without reserving an apartment.', 'Please reserve an apartment in order to continue.']);
			return redirect()->back();
		}
		
		$selectFields = [
			'images',
			'price',
			'name',
			'country',
			'city',
			'street',
			'house_number',
			'apartment_number',
			'property_type',
			'cleaning_fee',
			'checkin_time',
			'checkout_time',
		];
		
		$item = DB::table('item')
					->select($selectFields)
					->where('status', 1)
					->where('id', $reservation['item_id'])
					->first();
		
		if(!$item){
			Helper::message('danger', ['Item ID: "' . $reservation['item_id'] . '" doesn\'t exist.']);
			return redirect()->back();
		}
		
		$page = new \stdClass();
		$page->title		 	= 'Checkout';
		$page->item			 	= $item;
		
		$images					= $itemModel->getItemImages($item);
		$page->item->image	 	= ($images && isset($images[0])) ? $images[0] : false;
		$page->checkin 		 	= $reservation['checkin'];
		$page->checkout 	 	= $reservation['checkout'];
		$page->nightsNumber  	= Helper::calcNights($reservation['checkin'], $reservation['checkout']);
		// $page->dates  		 	= Helper::dateToStr($page->checkin, $page->checkout);
		$page->totalPrice	 	= ($page->item->price * $page->nightsNumber) + $page->item->cleaning_fee;
		$page->priceBeforeTax	= Helper::deductTax($page->totalPrice);
		$page->vat		 		= Helper::getVat();
		$page->vatValue		 	= $page->totalPrice - $page->priceBeforeTax;
		$page->guestsNumber		= $reservation['guests_number'];
		
		// Set customer details array.
		$customerDetails = isset($reservation['customer_details']) ? $reservation['customer_details'] : false;
		if($customerDetails){
			$page->customer_details = [
				'first_name' 	=> isset($customerDetails['first_name']) ? $customerDetails['first_name'] : '',
				'last_name' 	=> isset($customerDetails['last_name']) ? $customerDetails['last_name'] : '',
				'phone' 		=> isset($customerDetails['phone']) ? $customerDetails['phone'] : '',
				'email' 		=> isset($customerDetails['email']) ? $customerDetails['email'] : '',
				'requests' 		=> isset($customerDetails['requests']) ? $customerDetails['requests'] : '',
			];
		}else{
			$page->customer_details = [
				'first_name' 	=> '',
				'last_name' 	=> '',
				'phone' 		=> '',
				'email' 		=> '',
				'requests' 		=> '',
			];
		}

		// Update reservation session.
		$reservation['total_price'] 	= $page->totalPrice;
		$reservation['subtotal'] 		= $page->priceBeforeTax;
		$reservation['cleaning_fee'] 	= $page->item->cleaning_fee;
		$reservation['vat'] 			= $page->vat;
		$reservation['vat_value'] 		= $page->vatValue;
		$reservation['nights_number']	= $page->nightsNumber;
		$reservation['price_per_night']	= $page->item->price;
		$reservation['checkin_time']	= $page->item->checkin_time;
		$reservation['checkout_time']	= $page->item->checkout_time;
		$reservation['property_name']	= $page->item->name;
		$reservation['property_type']	= $page->item->property_type;
		$reservation['image']			= $page->item->image;
		$reservation['location'] 		= [
			'country' 			=> $item->country,
			'city' 				=> $item->city,
			'street' 			=> $item->street,
			'house_number' 		=> $item->house_number,
			'apartment_number' 	=> $item->apartment_number,
		];
		
		Session::put('reservation', $reservation);
		
		// If request booking mode & Paypal auto redirect mode are enabled, load the paypal auto_redirect view.
		if($requestBookingMode && $paypalAutoRedirect){
			return view('pages.paypal.auto_redirect')->with('page', $page);
		}
		return view('pages.checkout')->with('page', $page);
	}
	
	public function loadSuccess()
	{
		$reservation = Session::get('reservation');
 		if(empty($reservation['success_page']) || $reservation['success_page'] !== true)
		{
			Helper::message('danger', ['The checkout page is not available.']);
			return redirect('/');
		}
		
		// Set the customer_details cookie & remove the reservation session.
		$customerDetails = $reservation['customer_details'];
		Session::forget('reservation');

		$page 		 		 = new \stdClass();
		$page->title 		 = 'Thank You';
		$page->order 		 = $reservation;
		$page->general_email = Session::get('configs')['email_address'];
		$page->contact_phone = Session::get('configs')['contact_phone'];
		
		return view('pages.success')->with('page', $page);
	}
	
	public function prepareReservation()
	{
		$requestBookingMode = (int)Session::get('configs')['request_booking'];
		if((Request::isMethod('post') == false) || $requestBookingMode){
			return;
		}
		
		$params = Request::all();

		// Set validation rules.
		$rules = [
			'item_id'		 => 'required|numeric',
			'guests_number'  => 'required|numeric',
			'checkin_date'	 => 'required',
			'checkout_date'  => 'required',
			'city'			 => 'required|max:255',
		];
		
	    $validator = Validator::make($params, $rules);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }

		$performanceMode = (int)Session::get('configs')['item_performance_mode'];

		$airbnbIcs = false;
		if(!$performanceMode){
			$item = DB::table('item')
						->select('airbnb_ics')
						->where('id', $params['item_id'])
						->first();
			
			$airbnbIcs = $item->airbnb_ics;
		}
		
		// Check if the apartment is available. 
		$itemModel = new ItemModel();
		$isAvailable = $itemModel->isAvailable($params['checkin_date'], $params['checkout_date'], $performanceMode, $params['item_id'], $airbnbIcs);
		if(!$isAvailable){
			Helper::message('danger', ['The apartment is not available at the following dates: ' . $params['checkin_date'] . ' - ' . $params['checkout_date']]);
			return redirect()->back();
		}
		
		$reservation = [
			'item_id'		 => $params['item_id'],
			'checkin'		 => $params['checkin_date'],
			'checkout' 		 => $params['checkout_date'],
			'guests_number'  => $params['guests_number'],
		];
		
		// Set reservation session.
		Session::put('reservation', $reservation);
		
		// Update user search details cookie.
		$userSearchData = Request::Cookie('user_search_data');
		$userSearchData['checkin_date']  = $params['checkin_date'];
		$userSearchData['checkout_date'] = $params['checkout_date'];
		$userSearchData['guests_number'] = $params['guests_number'];
		$userSearchData['location'] 	 = $params['city'];
		
		// Set user search cookie for 16 weeks (about 3.5 months).
		Cookie::queue('user_search_data', $userSearchData, 60*24*7*4*4);
		
		// Load checkout page.
		return redirect('checkout');
	}
	
	public function requestBooking()
	{
		
		$requestBookingMode = (int)Session::get('configs')['request_booking'];
		if((Request::isMethod('post') == false) || ($requestBookingMode == false)){
			return;
		}
		
		$reservation = Session::get('reservation');
		$params = Request::all();
		
		// Set validation rules.
		$rules = [
			'item_id'		 => 'required|numeric',
			'guests_number'  => 'required|numeric',
			'checkin_date'	 => 'required',
			'checkout_date'  => 'required',
			'city'			 => 'required|max:255',
			'first_name'	 => 'required|max:255',
			'last_name'		 => 'required|max:255',
			'email'		 	 => 'required|email|max:255',
		];
		
	    $validator = Validator::make($params, $rules);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }

		$performanceMode = (int)Session::get('configs')['item_performance_mode'];

		$airbnbIcs = false;
		if(!$performanceMode){
			$item = DB::table('item')
						->select('airbnb_ics')
						->where('id', $params['item_id'])
						->first();
			
			$airbnbIcs = $item->airbnb_ics;
		}
		
		// TODO - add the is availability performance mode to save order, save request, save event????
		// Check if the apartment is available. 
		$itemModel = new ItemModel();
		$isAvailable = $itemModel->isAvailable($params['checkin_date'], $params['checkout_date'], $performanceMode, $params['item_id'], $airbnbIcs);
		if(!$isAvailable){
			Helper::message('danger', ['The apartment is not available at the following dates: ' . $params['checkin_date'] . ' - ' . $params['checkout_date']]);
			return redirect()->back();
		}
		
		// Update user search details cookie.
		$userSearchData = Request::Cookie('user_search_data');
		$userSearchData['checkin_date']  = $params['checkin_date'];
		$userSearchData['checkout_date'] = $params['checkout_date'];
		$userSearchData['guests_number'] = $params['guests_number'];
		$userSearchData['location'] 	 = $params['city'];
		
		// Set user search cookie for 16 weeks (about 3.5 months).
		Cookie::queue('user_search_data', $userSearchData, 60*24*7*4*4);
		
		$request = [
			'checkin' 		=> $params['checkin_date'],
			'checkout' 		=> $params['checkout_date'],
			'guests_number' => $params['guests_number'],
			'city'			=> $params['city'],
			'item_id' 		=> $params['item_id'],
			'first_name' 	=> $params['first_name'],
			'last_name' 	=> $params['last_name'],
			'email' 		=> $params['email'],
			'requests' 		=> $params['requests'],
			'uniq_id'		=> md5(uniqid(rand(), true)),
			'notify'		=> 1,
			'date'			=> date('Y-m-d H:i:s'),
			'status'		=> 'Pending',
		];
		
		// Insert the booking request to the request table.
		$requestId = DB::table('request')->insertGetId($request);
		
		// Add the request ID to the request array.
		$request['request_id'] = $requestId;
		
		// Apply nl2br on the 'requests' property before sending the email.
		$request['requests'] = nl2br($request['requests']);
		
		$email = Helper::email('request-customer-received');
		$emailToUser = [
			'from_address' 	=> Session::get('configs')['booking_requests_email'],
			'from_name'		=> Session::get('configs')['email_name'],
			'to_address'	=> $request['email'],
			'subject'		=> $email->subject,
		];
		
		$mailData = [
			'content' => $email->content,
			'tokens'  => $request,	
		];
		
		// Send success email to the customer.
		Helper::sendCmsMail($mailData, $emailToUser);

		
		$email = Helper::email('request-admin-received');
		$emailToAdministrator = [
			'from_address' 	=> $request['email'],
			'from_name'		=> $request['first_name'] . ' '. $request['last_name'],
			'to_address'	=> Session::get('configs')['booking_requests_email'],
			'subject'		=> $email->subject,
		];
		
		$mailData = [
			'content' => $email->content,
			'tokens'  => $request,	
		];
		
		// Send an email concerning the request to the administrator.
		Helper::sendCmsMail($mailData, $emailToAdministrator);

		// Throw a success message.
		Helper::message('success', ['Your booking request for the following dates: ' . $params['checkin_date'] . ' - ' . $params['checkout_date'] . ' has been successfully sent.', 'Request number: ' . $requestId]);
		return redirect()->back();
	}
	
	public function handleRequestApproval()
	{
		$params = Request::all();
		$reservation = Session::get('reservation');
		if(empty($params['q']) && empty($reservation['q']))
		{
			Helper::message('danger', ['The checkout page is not available without reserving an apartment.', 'Please reserve an apartment in order to continue.']);
			return false;
		}
		
		// If already approved, fetch q from the reservation session.
		if(isset($reservation['q']))
		{
			$q = $reservation['q'];
			$isApproved = true;
		}else{
			$q = $params['q'];
			$isApproved = false;
		}
		
		if(!$q)
		{
			Helper::message('danger', ['Invalid approval link.', 'Please contact the website\'s administrator.']);
			return false;
		}
		
		// Validate number of params.
		$bookingRequestArr = explode(',', urldecode($q));
		if(count($bookingRequestArr) !== 3)
		{
			Helper::message('danger', ['Invalid approval link.', 'Please contact the website\'s administrator.']);
			return false;
		}
		
		$requestId 		= $bookingRequestArr[0];
		$requestUniqId	= $bookingRequestArr[1];
		$requestItemId	= $bookingRequestArr[2];
		
		$whereConditions = [
			'id' 	  => $requestId,
			'uniq_id' => $requestUniqId,
			'item_id' => $requestItemId,
		];
		
		$request = DB::table('request')
					->where($whereConditions)
					->first();
					
		if(!$request)
		{
			Helper::message('danger', ['Invalid approval link.', 'Please contact the website\'s administrator.']);
			return false;
		}
		
		$reservation = [
			'item_id'		 	=> $request->item_id,
			'checkin'		 	=> $request->checkin,
			'checkout' 		 	=> $request->checkout,
			'guests_number'  	=> $request->guests_number,
			'q'		 		 	=> $q,
			'customer_details' 	=> [
				'first_name'	=>	$request->first_name,
				'last_name'		=>	$request->last_name,
				'email'			=>	$request->email,
				'requests'		=> 	$request->requests,
			],
		];
		
		// Set reservation session.
		Session::put('reservation', $reservation);
		
		// Update user search details cookie.
		$userSearchData = Request::Cookie('user_search_data');
		$userSearchData['checkin_date']  = $request->checkin;
		$userSearchData['checkout_date'] = $request->checkout;
		$userSearchData['guests_number'] = $request->guests_number;
		$userSearchData['location'] 	 = $request->city;
		
		// Set user search cookie for 16 weeks (about 3.5 months).
		Cookie::queue('user_search_data', $userSearchData, 60*24*7*4*4);
		
		if(!$isApproved)
		{
			// Throw a success message.
			Helper::message('success', ['Your booking request has been approved.', 'Please fill the checkout form below & click the "Continue to Payment" button in order to complete the booking process.']);
			return 'redirect';
		}
		
		return true;
	}
	
}