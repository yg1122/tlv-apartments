<?php 

namespace App\Http\Controllers;

use DB;
use URL;
use Input;
use Image;
use Helper;
use Request;
use Session;
use Validator;
use App\Http\Controllers\Controller;


class ContentController extends Controller {
	
	private $admin;
	private $page;
	
	public function build($section = false, $content = false)
	{
	
		$this->admin 	= Helper::getAdminSection($section, $content);
		$this->page 	= $this->admin['page'];
		
		$this->page->pagesNumber = Helper::getAvailablePages('banner', $this->page->itemsPerPage, false, 'identifier');
		$data['page']			 = $this->page;
		
		$data['banners'] = $this->getAllBanners();
		$data = array_merge($this->admin, $data);
		
		return view('admin.admin')->with('data', $data);
		
	}

	private function getAllBanners()
	{
		$banners = DB::table('banner')
			->select('id', 'identifier')
			->groupBy('identifier') 
			->orderBy('identifier', 'asc')
			->skip($this->page->start)
			->take($this->page->itemsPerPage)
			->get();
		
		return $banners;
	}
	
	public function saveBanner()
	{
		if(!Request::isMethod('post')) return;
		$params = Request::all();

		// Set validation rules.
		$rules = [
			'identifier'	=> ['required', 'max:255', 'regex:/^[a-zA-Z0-9_.-]*$/'],
		];
		
		// Set error messages.
		$messages = [
			'identifier.regex' => 'The Identifier value may contain only letters, numbers, underscores and dashes.',
		];
		
	    $validator = Validator::make($params, $rules, $messages);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }
		
		// If the given identifier is a new one, make sure it doesn't exist.
		if($params['current_identifier'] != $params['identifier']){
			$identifier = DB::table('banner')
						->select('identifier')
						->where('identifier', $params['identifier'])
						->first();
			
			// If already exists throw an error message.	
			if($identifier){
				Helper::message('danger', ['The identifier: "' . $params['identifier'] . '" already exists.', 'There cannot be two identical identifiers.']);
				return redirect()->back();
			}
		}
		
		// Upload images.
		$images = Image::upload($params);
		$bannersDir = base_path() . '/resources/assets/img/banners/';
		$resizedDir = $bannersDir . 'resized';
		
		$banners = [];
		$keys = array_keys($params['title']);
		foreach($keys as $keyValue)
		{
			if(isset($images['images'][$keyValue]) || $params['current_image'][$keyValue]){
				// The original image (not the resized).
				$imageName = isset($images['images'][$keyValue]) ? $images['images'][$keyValue] : $params['current_image'][$keyValue];
				
				$resizeWidth  = isset($params['resize_width'][$keyValue]) ? $params['resize_width'][$keyValue] : false;
				$resizeHeight = isset($params['resize_height'][$keyValue]) ? $params['resize_height'][$keyValue] : false;
				
				// Resize if needed.
				if($resizeWidth || $resizeHeight){
					// Validate the resize values, resize values should contain only positive integers.
					if((($resizeHeight === '') && (Helper::isNumber($resizeWidth))) || (($resizeWidth === '') && (Helper::isNumber($resizeHeight))) || ((Helper::isNumber($resizeWidth)) && (Helper::isNumber($resizeHeight)))){
						$resizedImage = str_replace($bannersDir, "", Image::resize($bannersDir . $imageName, $resizedDir, $resizeWidth, $resizeHeight));
					}else{
						Helper::message('danger', ['Resize values should contain only positive integers.']);
						return redirect()->back();
					}
					
				}else{
					$resizedImage = '';
				}
				
				$banners[$keyValue] = [
					'identifier' 	=> $params['identifier'],
					'title' 	 	=> $params['title'][$keyValue],
					'alt' 		 	=> $params['alt'][$keyValue],
					'url' 	 	 	=> $params['url'][$keyValue],
					'target' 	 	=> isset($params['target'][$keyValue]) ? $params['target'][$keyValue] : 0,
					'position' 		=> $params['position'][$keyValue],
					'image_name' 	=> $imageName,
					'resized_image' => $resizedImage,
					'resize_width' 	=> $resizeWidth,
					'resize_height' => $resizeHeight,
				];
			}
		}
		
		if(empty($banners)){
			
			$identifier = DB::table('banner')
						->select('identifier')
						->where('identifier', $params['identifier'])
						->first();
							
			$errorMessages = ['The item: "' . $params['identifier'] . '" doesn\'t have any uploaded banners.'];
			
			if($identifier){
				$removeBannerUrl = URL::to('admin/content/banners/removeBanner?identifier=' . $params['identifier']);
				$errorMessages[] = '<a href="' . $removeBannerUrl . '">Click here in order to remove it.</a>';
			}
			
			Helper::message('danger', $errorMessages);
			return redirect()->back();
		}
		
		// Remove existing banners.
		DB::table('banner')->where('identifier', $params['current_identifier'])->delete();
		
		// Insert banners data.
		DB::table('banner')->insert($banners);
		
		Helper::message('success', ['The banner "' . $params['identifier'] . '" has been successfully saved.']);
		return redirect('/admin/content/banners/editBanner?identifier=' . $params['identifier']);
	}
	
	public function editBanner()
	{
		$identifier = Request::input('identifier');
		$data = Helper::getAdminSection('content', 'banner-edit');
		$data['content_title'] = 'Edit Banner';
		
		$data['banners'] = $this->getBanners($identifier);
		
		return view('admin.admin')->with('data', $data);
	}
	
	public function addBanner()
	{
		$data = Helper::getAdminSection('content', 'banner-edit');
		$data['content_title'] = 'New Banner';
		
		$data['banners'][] = new \stdClass;
		
		return view('admin.admin')->with('data', $data);
	}
	
	private function getBanners($identifier)
	{
		$banners = DB::table('banner')
			->where('identifier', $identifier)
			->orderBy('position', 'asc')
			->get();
		
		$banners[] = new \stdClass;
		
		return $banners;
	}
	
	public function removeBanner()
	{
		$identifier = Request::input('identifier');
		DB::table('banner')->where('identifier', $identifier)->delete();
		
		Helper::message('success', ['The banner has been successfully removed.']);
		return redirect('/admin/content/banners');
	}
}