<?php
namespace App\Http\Controllers;

use DB;
use URL;
use Input;
use Helper;
use Session;
use Validator;
use App\Models\ItemModel;
use App\Models\CheckoutModel;
use App\Http\Controllers\Controller;


class PaypalController extends Controller
{
	
	public function postPayment()
	{
		// Checking is a reservation session exists.
		$reservation = Session::get('reservation');
		if(!$reservation){
			Helper::message('danger', ['Placing an order is impossible without reserving an apartment.', 'Please reserve an apartment in order to continue.']);
			return redirect()->back();
		}
		
		$params = Input::all();
		
		// Set validation rules.
		$rules = [
			'first_name'	=>	'required|max:255',
			'last_name'		=>	'required|max:255',
			'email'			=>	'required|email|max:255',
			'total_price'	=>	'required|numeric',
			'phone'			=>  'max:255',
		];
		
	    $validator = Validator::make($params, $rules);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }
		
		$reservation['customer_details'] = [
			'first_name'	=>	$params['first_name'],
			'last_name'		=>	$params['last_name'],
			'phone'			=>	$params['phone'],
			'email'			=>	$params['email'],
			'requests'		=> 	isset($params['requests']) ? $params['requests'] : '',
		];
		
		// Add the customer details to the reservation session.
		Session::put('reservation', $reservation);
		
		$item = DB::table('item')
				->select('price', 'cleaning_fee', 'name', 'airbnb_ics', 'short_description')
				->where('status', 1)
				->where('id', $reservation['item_id'])
				->first();
		
		
		$nightsNumber 	  = Helper::calcNights($reservation['checkin'], $reservation['checkout']);	
		$dbTotalPrice 	  = trim((float)$item->price * $nightsNumber) + $item->cleaning_fee;;		
		$clientTotalPrice = trim((float)Input::get('total_price'));
		

		// If the given total price from the client doesn't equal to the total price defined in the DB, throw an error message.
		if($clientTotalPrice != $dbTotalPrice){
			Helper::message('danger', ['The total price of your reservation is: ' . $dbTotalPrice . ', please try again.']);
			return redirect()->back();
		}
		
		
		$performanceMode = (int)Session::get('configs')['item_performance_mode'];
	
		$airbnbIcs = false;
		if(!$performanceMode){
			$airbnbIcs = $item->airbnb_ics;
		}
		
		// Check if the apartment is available. 
		$itemModel = new ItemModel();
		$isAvailable = $itemModel->isAvailable($reservation['checkin'], $reservation['checkout'], $performanceMode, $reservation['item_id'], $airbnbIcs);
		if(!$isAvailable){
			Helper::message('danger', ['The apartment is not available at the following dates: ' . $reservation['checkin'] . ' - ' . $reservation['checkout']]);
			return redirect()->back();
		}

		
		$paypalConf = $this->getPaypalConf();
		$postFields = [
			'USER' 								=> $paypalConf['username'],
			'PWD' 								=> $paypalConf['password'],
			'SIGNATURE' 						=> $paypalConf['signature'],
			'METHOD' 							=> 'SetExpressCheckout',
			'VERSION' 							=> $paypalConf['api_version'],
			'PAYMENTREQUEST_0_AMT' 				=> $dbTotalPrice,
			'PAYMENTREQUEST_0_CURRENCYCODE' 	=> $paypalConf['currency'],
			'PAYMENTREQUEST_0_PAYMENTACTION' 	=> 'Sale',
			'PAYMENTREQUEST_0_ITEMAMT' 			=> $dbTotalPrice,
			'L_PAYMENTREQUEST_0_NAME0' 			=> $item->name,
			'L_PAYMENTREQUEST_0_DESC0' 			=> $item->short_description,
			'L_PAYMENTREQUEST_0_QTY0' 			=> 1,
			'L_PAYMENTREQUEST_0_AMT0' 			=> $dbTotalPrice,
			'L_PAYMENTREQUEST_0_ITEMCATEGORY0' 	=> 'Digital',
			'CANCELURL' 						=> $paypalConf['cancel_url'],
			'RETURNURL' 						=> $paypalConf['return_url'],
			// 'L_BILLINGTYPE0' 					=> 'RecurringPayments',
			// 'L_BILLINGAGREEMENTDESCRIPTION0' 	=> $item->name,
			// 'LOCALECODE' 					=> 'en_US',
		];
		
		$response = $this->paypalCurlExecute($paypalConf['url'], $postFields);
		$nvp	  = $this->fetchResponseData($response);
		
		if(isset($nvp['ACK']) && $nvp['ACK'] == 'Success'){
			$query = array(
				'cmd' => '_express-checkout',
				'token' => $nvp['TOKEN']
			);
			$redirectURL = sprintf($paypalConf['redirect_url'], http_build_query($query));
			
			return redirect($redirectURL);

		}else{
			// Failure, TODO - Log to file/DB.
			
			Helper::message('danger', ['We could\'nt charge your credit card for some reason.','Error code: ' . $nvp['L_ERRORCODE0']]);
			return redirect('checkout');
		} 
	}

	private function fetchResponseData($response)
	{
		$nvp = [];
		if(preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)){
			foreach ($matches['name'] as $offset => $name) {
				$nvp[$name] = urldecode($matches['value'][$offset]);
			}
		}
		
		return $nvp;
	}
	
	private function paypalCurlExecute($apiUrl, $postFields)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_URL, $apiUrl);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postFields));
		
		$response = curl_exec($curl);
		curl_close($curl);
		
		return $response;
	}
	
	private function getPaypalConf()
	{
		$paypalSettings = unserialize(Session::get('configs')['paypal']);

		if($paypalSettings['mode'] == 'live'){
			$paypalConf = [
				'username' 		=> $paypalSettings['username'],
				'password' 		=> $paypalSettings['password'],
				'signature' 	=> $paypalSettings['signature'],
				'url' 			=> 'https://api-3t.paypal.com/nvp',
				'redirect_url' 	=> 'https://www.paypal.com/cgi-bin/webscr?%s',
			];
		
		}else{
			$paypalConf = [
				'username' 		=> 'sdk-three_api1.sdk.com',
				'password' 		=> 'QFZCWN5HZM8VBG7Q',
				'signature' 	=> 'A-IzJhZZjhg29XQ2qnhapuwxIDzyAZQ92FRP5dqBzVesOkzbdUONzmOU',
				'url' 			=> 'https://api-3t.sandbox.paypal.com/nvp',
				'redirect_url' 	=> 'https://www.sandbox.paypal.com/cgi-bin/webscr?%s',
			];
		}
		
		$paypalConf['api_version'] = '109.0';
		$paypalConf['currency']    = Helper::getCurrency();
		$paypalConf['return_url']  = URL::to('payment/status');
		$paypalConf['cancel_url']  = URL::to('payment/status');

		return $paypalConf;
	}
	
	public function getPaymentStatus()
	{
		$params = Input::all();
		
		if(empty($params['token']) || empty($params['PayerID']) || ($params['token'] == false) || ($params['PayerID'] == false)){
			Helper::message('danger', ['Payment failed for some reason.', 'Please try again.']);
			return redirect('checkout');
		}
		
		$paypalConf = $this->getPaypalConf();
		$postFields = [
			'USER' 		=> $paypalConf['username'],
			'PWD' 		=> $paypalConf['password'],
			'SIGNATURE' => $paypalConf['signature'],
			'METHOD' 	=> 'GetExpressCheckoutDetails',
			'VERSION' 	=> $paypalConf['api_version'],
			'TOKEN' 	=> $params['token'],
		];
		
		$response = $this->paypalCurlExecute($paypalConf['url'], $postFields);
		$nvp	  = $this->fetchResponseData($response);
		// TODO - save the recieved data (special log???).
		
		if(empty($nvp['PAYMENTREQUEST_0_AMT'])){
			Helper::message('danger', ['Payment failed for some reason.', 'Please try again.']);
			return redirect('checkout');
		}
		
		$postFields = [
			'USER' 								=> $paypalConf['username'],
			'PWD' 								=> $paypalConf['password'],
			'SIGNATURE'	 						=> $paypalConf['signature'],
			'METHOD' 							=> 'DoExpressCheckoutPayment',
			'VERSION' 							=> $paypalConf['api_version'],
			'TOKEN' 							=> $params['token'],
			'PAYERID' 							=> $params['PayerID'],
			'PAYMENTREQUEST_0_PAYMENTACTION' 	=> 'Sale',
			'PAYMENTREQUEST_0_CURRENCYCODE' 	=> $paypalConf['currency'],
			'PAYMENTREQUEST_0_AMT' 				=> $nvp['PAYMENTREQUEST_0_AMT'], // The total price
		];
		
		$response = $this->paypalCurlExecute($paypalConf['url'], $postFields);
		$nvp	  = $this->fetchResponseData($response);
		
		if(isset($nvp['PAYMENTINFO_0_PAYMENTSTATUS']) && ($nvp['PAYMENTINFO_0_PAYMENTSTATUS'] == 'Completed') && isset($nvp['PAYMENTINFO_0_ACK']) && ($nvp['PAYMENTINFO_0_ACK'] == 'Success')){
			$transactionDetails = [
				'payment_id' => $nvp['PAYMENTINFO_0_TRANSACTIONID'],
				'payer_id' 	 => $params['PayerID'],
				'token'		 => $params['token'],
			];
			
			$checkoutModel = new CheckoutModel;
			$checkoutModel->createOrder($transactionDetails);
			$checkoutModel->sendOrderMail();
			
			// Update reservation session - set success_page to true.
			$reservation = Session::get('reservation');
			$reservation['success_page'] = true;
			Session::put('reservation', $reservation);	
			
			Helper::message('success', ['Payment success.']);
			return redirect('checkout/success');
		}
		
		Helper::message('danger', ['Payment failed.', 'Please contact the website owners to inform them of this problem.']);
		return redirect('checkout');
	}
}