<?php 

namespace App\Http\Controllers;

use DB;
use Helper;
use Request;
use Session;
use Validator;
use App\Models\ItemModel;
use App\Models\ListModel;
use App\Http\Controllers\Controller;


class EventsController extends Controller {
	
	private $admin;
	private $page;
	
	public function build($section = false, $content = false)
	{
		$this->admin 	= Helper::getAdminSection($section, $content);
		$this->page 	= $this->admin['page'];
		
		$data['content_title'] 	= 'Manage Events';
		$data['events'] 		= $this->getEventsTable();
		
		$this->page->pagesNumber = Helper::getAvailablePages('local_events', $this->page->itemsPerPage);
		$data['page']			 = $this->page;
		
		$data = array_merge($this->admin, $data);
		
		return view('admin.admin')->with('data', $data);
		
	}
	
	private function getEventsTable()
	{
		$select = [
			'local_events.id',
			'local_events.item_id',
			'local_events.checkin_timestamp',
			'local_events.checkout_timestamp',
			'item.name',
		];
		
		$events = DB::table('local_events')
            ->join('item', 'item.id', '=', 'local_events.item_id')
            ->select($select)
			->orderBy('checkin_timestamp', 'desc')
			->skip($this->page->start)
			->take($this->page->itemsPerPage)
            ->get();
		
		// Arrange the events array, convert timestamp to date.
		foreach($events as $event)
		{
			$event->checkin = Helper::timestampToDate($event->checkin_timestamp);
			$event->checkout = Helper::timestampToDate($event->checkout_timestamp);
			
			// Remove the unnecessary properties.
			unset($event->checkin_timestamp);
			unset($event->checkout_timestamp);
		}
		
		return $events;
	}

	public function saveEvent()
	{
		if(!Request::isMethod('post')) return;
		$params = Request::all();

		if(($params['checkin'] == $params['current_checkin']) && ($params['checkout'] == $params['current_checkout']) && ($params['item_id'] == $params['current_item_id'])){
			Helper::message('info', ['Notice: the event details haven\'t changed.']);
            return redirect()->back();
		}
		
		// Set validation rules.
		$idRules = $params['update'] ? ['required', 'numeric'] : [];
		$rules = [
			'event_id'		 	=> $idRules,
			'checkin' 		 	=> 'required',
			'checkout'		 	=> 'required',
			'item_id'		 	=> 'required|numeric',
		];
		
	    $validator = Validator::make($params, $rules);
		
        if($validator->fails()){
			
			Helper::message('danger', $validator->errors()->all());
            return redirect()
					->back()
					->withInput();
        }
		
		// Check if the apartment is available. 
		$itemModel = new ItemModel();
		$isAvailable = $itemModel->isAvailable($params['checkin'], $params['checkout'], true, $params['item_id'], false);
		if(!$isAvailable){
			Helper::message('danger', ['The apartment is not available at the following dates: ' . $params['checkin'] . ' - ' . $params['checkout']]);
			return redirect()->back();
		}
		
		$event = [
			'checkin_timestamp' 	=> Helper::dateToTimestamp($params['checkin']),
			'checkout_timestamp'	=> Helper::dateToTimestamp($params['checkout']),
			'item_id'				=> $params['item_id'],
		];

		$eventTables = ['local_events', 'events'];
		if($event['checkout_timestamp'] >= time()){
			$eventTables[] = 'future_events';
		}
				
		if($params['update']){
			try {
				
				$whereConditions = [
					"checkin_timestamp" 	=> Helper::dateToTimestamp($params['current_checkin']),
					"checkout_timestamp" 	=> Helper::dateToTimestamp($params['current_checkout']),
					"item_id" 				=> $params["current_item_id"],
				];
				
				foreach($eventTables as $table)
				{
					DB::table($table)
						->where($whereConditions)
						->update($event);	
				}
						
				Helper::message('success', ['An event at the following dates: ' . $params['checkin'] . ' - ' . $params['checkout'] . ' has been updated successfully.']);
				return redirect()->back();
						
			}catch(\Exception $e){
				Helper::exception($e);
			}
			
		}else{
			try {
				
				foreach($eventTables as $table){
					DB::table($table)->insert($event);
				}

				Helper::message('success', ['An event at the following dates: ' . $params['checkin'] . ' - ' . $params['checkout'] . ' has been created successfully.']);
				return redirect('/admin/events/manage');
					
			}catch(\Exception $e){
				Helper::exception($e);
			}
		}
		
	}
	
	public function createEvent()
	{
		$listConf = unserialize(Session::get('configs')['list_config']);
		$listModel = new ListModel();
		
		$data = Helper::getAdminSection('events', 'edit');
		$data['content_title'] = 'New Event:';
		
		$data['event'] = new \stdClass;
		$data['event']->items = $this->getItems();
		$data['event']->item_id = false;
		$data['event']->items = $this->getItems();
		$data['event']->minDate = $listModel->getMinDate($listConf);
		$data['event']->disabledDates = $listConf['list_disabled_dates'];
		
		return view('admin.admin')->with('data', $data);
	}
	
	public function editEvent()
	{
		$id = Request::input('id');
		$data = Helper::getAdminSection('events', 'edit');
		
		$data['event'] = $this->getEvent($id);
		$data['content_title'] = 'Edit event: "' . $data['event']->id . '"';
		
		return view('admin.admin')->with('data', $data);
	}
	
	private function getEvent($id)
	{
		$listConf = unserialize(Session::get('configs')['list_config']);
		$listModel = new ListModel();
		
		$event = DB::table('local_events')
					->where('id', $id)
					->first();
		
		
		$event->checkin = Helper::timestampToDate($event->checkin_timestamp);
		$event->checkout = Helper::timestampToDate($event->checkout_timestamp);
			
		// Remove the unnecessary properties.
		unset($event->checkin_timestamp);
		unset($event->checkout_timestamp);
			
		$event->items = $this->getItems();
		$event->minDate = $listModel->getMinDate($listConf);
		$event->disabledDates = $listConf['list_disabled_dates'];
		
		return $event;
	}
	
	private function getItems()
	{
		$items = DB::table("item")
					->select('id', 'name', 'status')
					->orderBy('name', 'asc')
					->get();
		
		return $items;
	}
	
	public function removeEvent()
	{
		// TODO - Change the get to post (csrf attacks).
		$params = Request::all();

		if(isset($params['id']) && $params['id'] && empty($params['current_item_id'])){
			$event = DB::table('local_events')
						->where('id', $params['id'])
						->first();
			
			$currentCheckin 	= $event->checkin_timestamp;
			$currentCheckout	= $event->checkout_timestamp;
			$params['current_item_id']	= $event->item_id;
		}else{
			$currentCheckin   = Helper::dateToTimestamp($params['current_checkin']);
			$currentCheckout  = Helper::dateToTimestamp($params['current_checkout']);
		}
		
		$eventTables = ['local_events', 'events', 'future_events'];
		$whereConditions = [
			"checkin_timestamp" 	=> $currentCheckin,
			"checkout_timestamp" 	=> $currentCheckout,
			"item_id" 				=> $params["current_item_id"],
		];
				
		foreach($eventTables as $table){
			DB::table($table)->where($whereConditions)->delete();	
		}
		
		Helper::message('success', ['The event has been successfully removed.']);
		return redirect('/admin/events/manage');
	}

}