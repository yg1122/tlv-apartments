<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Site - HomeController.
Route::get('/', 'HomeController@build');

// Site - events Route.
Route::get('events', 'CmsController@fetchEvents');

// Site - hoover Route.
Route::get('hoover', 'HooverController@fetch');

// Site - checkout Route. 
Route::get('checkout', 'CheckoutController@build');

// Site - checkout Route. 
Route::get('checkout/success', 'CheckoutController@loadSuccess');

// Site - actions Route. 
Route::any('action/{controller}/{method}', 'ActionController@execute');

Route::post('payment', array(
    'as' => 'payment',
    'uses' => 'PaypalController@postPayment',
));

// PayPal response handler.
Route::get('payment/status', array(
    'as' => 'payment.status',
    'uses' => 'PaypalController@getPaymentStatus',
));

// Admin - Main routing logic.
Route::any('admin/{controller?}/{content?}/{method?}', 'AdminController@route');

// Site - ContactController.
Route::get('contact-us', 'ContactController@load');

// Site - CmsController.
Route::get('{slug}/{urlKey?}', 'CmsController@loadPage');

