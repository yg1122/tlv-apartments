<?php namespace App\Http\Middleware;

use DB;
use Config;
use Closure;
use Session;
use Illuminate\Contracts\Routing\Middleware;

class SetConfig implements Middleware {

    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle($request, Closure $next)
    {
			
		$configObj = DB::table('config')->select('name','value','serialized_array')->get();
		$configs = [];
		
		foreach($configObj as $config){
			$configVal = $config->value ? $config->value : $config->serialized_array;
			$configs[$config->name] = $configVal;
		}
		
		// Set configs session.
		Session::put('configs', $configs);
		
		// Set default timezone.
		date_default_timezone_set(Session::get('configs')['timezone']);
		
		// Share default data.
		$this->shareDefaultData();
		
        return $next($request);
    }
	
	private function shareDefaultData()
	{
		$configs = Session::get('configs');
		$data = [
			'categories' => unserialize($configs['categories']),
			'seo' 		 => unserialize($configs['seo']),
			'favicon' 	 => $configs['favicon'],
			'footer' => [
				'credits' => $configs['credits'],
				'content' => $configs['footer'],
			],
			'header' => [
				'content' => $configs['header'],
			],
		];
		
        view()->share('data', $data);
	}
	
}