<?php

$twitterPath = '/var/www/test';
require_once($twitterPath . '/twitter-api-php-master/TwitterAPIExchange.php');

$q 		= $_GET['q'];
$count 	= $_GET['count'] ? $_GET['count'] : 5;

if(!$q) die("Please define a Search Key value.");

// App settings.
$settings = array(
    'oauth_access_token' 		=> "4266578266-cqof633gGSgwy5isRLEH8Z8NpbvllIhygCoIrwE",
    'oauth_access_token_secret' => "e2K4xq81C2UomDruZhbQ9zCqteyevP7tykQNTRpq9iSy3",
    'consumer_key' 				=> "83kh9Mu3Ra40sakxDEKrDPxEt",
    'consumer_secret' 			=> "UB90lQdRfoEByzDmvMMFu7EoI8rm8CssRXSewG4ADpALqJQyCi"
);

$url = 'https://api.twitter.com/1.1/search/tweets.json';
$params = '?q=' . $q . '&count=' . $count;
$requestMethod = 'GET';
$twitter = new TwitterAPIExchange($settings);

// Execute the request.
$json = $twitter->setGetfield($params)
             ->buildOauth($url, $requestMethod)
             ->performRequest();     
			 

$twitterData = json_decode($json);

$tweetsArr = [];
foreach($twitterData->statuses as $tweet)
{
	$tweetArr = [
		'Id' => $tweet->id,
		'DateTime' => $tweet->created_at,
		'Text'	=> $tweet->text,
		'UserName' => $tweet->user->name,
		'Followers_Count' => $tweet->user->followers_count,
		'Friends_Count' => $tweet->user->friends_count,
		'Retweet_Count' => $tweet->retweet_count,
		'Favorite_Count' => $tweet->favorite_count,
	];

	$tweetsArr[] = $tweetArr;
}

$xml = new SimpleXMLElement('<tweets/>');

foreach($tweetsArr as $tweet)
{
	$child = $xml->addChild('tweet');
	foreach($tweet as $key => $value)
	{
		$child->addChild($key, $value);
	}
}

print $xml->asXML();